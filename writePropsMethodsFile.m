d = dir('./@*');
classes = {d.name};
fid = fopen('PropsAndMethods.md','wt');
fprintf(fid,'# arfiClass');
for i = 1:length(classes)
    classname = classes{i}(2:end);
    fprintf(fid,'\r\n## %s:',classname);
    metadata = mc.fromName(classname);
    p = properties(classname);
    m = methods(classname);
    list = {'Hidden','Sealed','Abstract'};
    for j = 1:length(list)
        if metadata.(list{j})
            fprintf(fid,'\r\n%s',list{j});
        end
    end
    if ~isempty(m)
    fprintf(fid,'\r\n\tMethods');
        for j = 1:length(m)
            fprintf(fid,'\r\t\t* %s',m{j});
        end
    end
    if ~isempty(p)
        fprintf(fid,'\r\n\tProperties');
        for j = 1:length(p)
            fprintf(fid,'\r\t\t* %s',p{j});
        end
    end
end
fclose(fid);
