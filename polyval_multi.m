function Y = polyval_multi(x,P)
x = x(:);
outsz = size(P);
P = P(:,:);
outsz(1) = length(x);
ny = size(P,2);
order = size(P,1)-1;
Y = zeros(length(x),ny);
for i = 1:(order+1);
    Y = Y + x.^(i-1)*P(i,:);
end
Y = reshape(Y,outsz);

