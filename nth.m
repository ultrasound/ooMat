function sfx = nth(n)
    switch n
        case 1
            sfx = '1st';
        case 2
            sfx = '2nd';
        case 3
            sfx = '3rd';
        otherwise
            sfx = sprintf('%gth',n);
    end
end