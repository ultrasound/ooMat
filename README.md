# ooMat

Object-oriented data processing for MATLAB  

peterhollender@gmail.com

This project began as modularizing our labs processing code to make it
scanner-agnostic, but has been expanded into a generalized tool for processing
multi-dimensional data. Reading through the datacontainer source code is a
decent way to see what methods have been implemented, and example.m covers some
simple examples on dummy data.

MEX / CUDA-specific files make need to be compiled on individual Windows /
Linux systems, depending on available libraries.  Linux binaries can be
compiled using the associated ```Makefile``` in source directories.

the top level class is ```datacontainer```, which holds the matrix of data, 
information about the dimensions, and any optional parameters.

```example.m``` works through some of basic functionality of creating and 
working with a datacontainer.