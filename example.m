%% Read in data from MATLAB example
trafficVid = VideoReader('traffic.mj2');
clear im
nFrames = trafficVid.Duration*trafficVid.FrameRate;
for i = 1:nFrames
    im(:,:,i) = double(rgb2gray(read(trafficVid,i)));
end
x = 0:trafficVid.Width-1;
y = 0:trafficVid.Height-1;
t = (0:nFrames-1)*(1/trafficVid.FrameRate);
%% Create New Datacontainer
o = datacontainer(im,datacontainer.datadims({y,x,t},{'y','x','t'},{'px','px','s'}))
%% Clear previous data
clear trafficVid im nFrames x y t i
%% List some information about the datacontainer
methods(o)
properties(o)
o.dims
%% Animate
%We can slice and 3D data with imageslider. Click the > or < buttons to
%play forward or backward
o.imageslider('t',[0 255])
%% Slice
% slice(obj,dim,idx) is a method of the object. It returns a second object
% which is a slice of the first in the 'dim',dimension. imagesc(obj,...)
% overloads the built-in imagesc. The overloaded imagesc includes axes
% labels
o1 = o.slice('t',71)
figure(1);clf;
o1.imagesc([0 255])
colormap gray
%% Slicex
% slicex(obj,dim,range) retrieves a range of values from the object 
figure(1);clf;
o1 = o.slicex('t',5).imagesc([0 255])
colormap gray
%% Calling Methods
%Note how methods be both called as out = method(obj,var1,var2,...) or out
%= obj.method(var1,var2,...)
o1 = o.slice('t',71);
%Dimensions can be specified by their index or name
o1 = slice(o,3,71);
%% Multiple methods
% using the method(obj...) syntax, though, you can chain multiple methods
% together on one line the filter command takes its pass band in units of
% hz (1/s), because the filtered dimension 't', has units of 's'. In this
% case, we've high-pass filtered the movie in the 't' dimension to suppress
% the background
o.filter('t',[0.5],2,'high').imageslider('t',[-50 50]);
%% A few simple methods
% There are many methods that can be applied to a data object
clf
subplot(221)
o.max('t').imagesc
subplot(222)
o.min('t').imagesc
subplot(223)
o.median('t').imagesc
subplot(224)
o.imageslider('t')
%% Overloaded methods
% Some basic MATLAB functions are overloaded when used in the context of
% datacontainers. Basic math functions work, but only as long as the first
% argument is a datacontainer...
clf
subplot(221)
imagesc(slicex(o,'t',5),[0 255])
title('original')
subplot(222)
imagesc(slicex(o,'t',5)+100,[0 255])
title('added 100')
subplot(223)
imagesc((slicex(o,'t',5)+slicex(o,'t',1))/2,[0 255])
title('averaged two frames')
subplot(224)
imagesc(slicex(o,'t',5)\1)
title('inverted')
%%
% note that when we wanted to invert the data, we had to use the left
% divide symbol '\', obj\1, rather than doing 1/obj. This is because
% 1/obj will use the built-in matlab divide function because
% matlab internally executes rdivide(1,obj), while ldivide(obj,1) will
% trigger the use of the overloaded ldivide method of the datacontainer.
% Similarly, obj+50 is read as plus(obj,50) (the overloaded method),
% whereas 50+obj is read as plus(50,obj), which will error.

%% Slicing in different dimensions 
%the overloaded imagesc only requires that you have 2 non-singlteon
%dimensions
clf
subplot(221)
imagesc(slice(o,'t',71))
subplot(222)
imagesc(slice(o,'x',50))
subplot(223)
imagesc(slice(o,'y',50))
%% FFT Example
%methods can be chained into complicated processing chains, handling units
%on the dimensions and data automatically. Here, we will create an
%imageslider, taking the Fourier transform in the x and y dimensions, and
%showing it on a normalized, log-compressed scale.
clf
o.fft({'x','y'}).abs.normalize.db.imageslider('t',[-60 0]);