function clim = autoclim(data,prct)
%AUTOCLIM get nice c-axis limits clim = AUTOCLIM(data,prct)if
%~exist('prct','var')
%
%AUTOCLIM returns a set of "nice" c-axis limits from the input data that
%reject outliers. The default limits are the 10th and 90th percentile of
%the data, but can be specified otherwise. If the magnitude of either
%initially calculated limit is more than 10 times greater in magnitude than
%the lower, the opposite limit is set to 0, and if the initial limits are
%opposite in sign and 10 times greater than the magnitude of their average,
%they are adjusted to be centered at 0.
%
%INPUTS
%   data: Input data of any size prct: optional percentile specification.
%   Can be set as a two element vector as percentage points i.e. [10 90] or
%   fractions [0.1 0.9] if both elements are < 1. If specified as a single
%   element, the opposite is set to 100-prct / 1-prct.
%
%OUTPUT
%   clim: computed c-axis limits


if ~exist('prct','var')
    prct = [10 90];
end
if all(prct<=1)
    prct = 100*prct;
end
if numel(prct) == 1
    prct = sort([prct 100-prct]);
end
msk = ~isnan(data) & isfinite(data);
if ~any(msk(:))
    error('no valid data found!')
end
if ~license('test','statistics_toolbox')
    warning('Need to install the Statistics Toolbox')
end
clim = prctile(data(msk),prct);
if clim(2)>10*abs(clim(1))
    clim(1) = 0;
end
if clim(1)<-10*abs(clim(2))
    clim(2) = 0;
end
if abs(clim(1)+clim(2))<(abs(clim(1))+abs(clim(2)))/10
    clim = [-1 1]*max(abs(clim));
end
if diff(clim)==0
    clim(2) = clim(2)+max(1e-6,abs(0.1*clim(1)));
end
if clim(2)<clim(1)
    clim = clim([2 1]);
end
