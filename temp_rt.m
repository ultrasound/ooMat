if ~exist('idx','var')
    idx = 0;
end
filepath = 'V:\Program Files\Siemens\syngo\Bedrock\Startup';
iq = sc2000.loadSWIFData(filepath,idx);
arfi = progressive(loupas('kernel_lambda',5),iq,...
    'refidx',iq.params.sc2000params.nref,...
    'skipidx',iq.params.sc2000params.nref+[1:3],...
    'interp',true);
arfi = interp(arfi,'t','uniform');
arfi = motionfilter(arfi,find(arfi.dims.t<0 | arfi.dims.t>15),2);
if length(arfi.dims.th)>1
    thdim = 'th-';
else
    thdim = 'thp-';
end;
v = filter(differentiate(arfi-median(arfi,{thdim(1:end-1),'r'}),'t'),'t',[50 1000]*1e-3);
%v = differentiate(arfi,'t');
[vL vR] = directionalfilter(v,thdim,'t');
v = vR;
[pk tpk] = subsamplepeak(v,'t');
[gx gy] = gradient(tpk,{thdim,'r'});
c = scanconvert(abs(gx\1));
vsc = scanconvert(v,'latinc',0.2,'axialinc',0.2);
[pksc tpksc] = subsamplepeak(vsc,'t');
[gx gy] = gradient(tpksc,{'x','z'});
c = sqrt(gx^2+gy^2)\1;
%%
figure(1);
colormap hollender
sp = subplots([2 1],1,[0.1 0.15],[0.05 0.1 0.1 0.05]);
subplot(sp(2));
imagesc(permute(median(slice(v,'r',find(abs(v.dims.r-c.params.sc2000params.pushFocalDepth)<1)),'r'),[1 3 2 4]),[-1 1])
ylim([-1 6])
subplot(sp(1));
animate(vsc,'t','fps',40,'clim',[-1 1],'ylim',v.params.sc2000params.pushFocalDepth+[-10 10],'xlim',[-5 5]);
%%
figure(2);
sp = subplots(1,[2 1],[0.05 0.15],[0.05 0.15 0.1 0.05]);
subplot(sp(1));
imagesc(pk);
subplot(sp(2));
plot(permute(slice(pk,'r',find(abs(pk.dims.r-pk.params.sc2000params.pushFocalDepth)<2)),[2 4 3 1]))
legend off
axis tight
figure(3);
sp = subplots(1,[2 1],[0.05 0.15],[0.05 0.15 0.1 0.05]);
subplot(sp(1));
imagesc(tpk);
subplot(sp(2));
plot(permute(slice(tpk,'r',find(abs(pk.dims.r-pk.params.sc2000params.pushFocalDepth)<2)),[2 4 3 1]));
legend off
axis tight
ylim([0 5])
colormap(genColorMap([[0 0 0];getTeamColors('Cavaliers')],255))
figure(4);
sp = subplots(1,[2 1],[0.05 0.15],[0.05 0.15 0.1 0.05]);
subplot(sp(1));
imagesc(c,[0.5 2.5])
ylim(v.params.sc2000params.pushFocalDepth+[-10 10]);
subplot(sp(2));
plot(permute(slice(c,'z',find(abs(c.dims.z-c.params.sc2000params.pushFocalDepth)<2)),[2 4 3 1]))
legend off
axis tight
ylim([0.5 3])
