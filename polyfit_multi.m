function P = polyfit_multi(x, Y, n)
sz = size(Y);
if sz(1) ~= length(x)
  error('number of elements of x must match number of rows in Y');
end
x = x(:);
outsz = sz; outsz(1) = n+1;
P = zeros(outsz);
Xmat = zeros(sz(1),n+1);
for p = 1:n+1;
    Xmat(:,p) = x.^(p-1);
end
XtXiXt = (Xmat.'*Xmat)\(Xmat.');
P(:,:) = XtXiXt*Y(:,:);