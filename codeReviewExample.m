cd F:\git\arfiClass\;
%% Load Data (S2000)
filepath = 'D:\S2000\data\Synecor\Week00\G433\URI\';
iq = s2000.loadURIData(filepath,8);
refidx = iq.params.s2000params.num_pretracks*iq.params.s2000params.m_factor;
iq = slice(iq,'acq',1);
iq = s2000.interpMultiTime(iq,'t');
%% Load Data (SC2000)
%filepath = 'D:\SC2000\data\20140514_yd17_4C1\';
filepath = 'D:\SC2000\data\20151112_Phantoms\E04p5kPa\acq01\MTLSWEI';
iq = sc2000.loadSWIFData(filepath,1);
refidx = iq.params.sc2000params.nref;

%% Create RF Data
iqup = resample(iq,48e6);
rf = remodulate(iqup);
%% Simple anchored kasai
estobj = kasai('kernel_lambda',5);
tic
[arfi] = estobj.anchored(iq,'refidx',refidx);
toc
%% Display results
animate(arfi,'t','fps',30,'clim',[0 5])
%% Motion Filter, Scan Convert
arfim = motionfilter(arfi,find(arfi.dims.t<-0.2 | arfi.dims.t>10),1);
arfisc = scanconvert(arfim);
animate(arfisc,'t','fps',30);r
%% Loupas
estobj = loupas('kernel_lambda',5);
tic
[arfi] = estobj.progressive(iq,'refidx',refidx,'skipidx',refidx+1);
toc
%% Normxcorr - No Mex, no Parfor
estobj = normxcorr(...
    'kernel_lambda',1.5,...
    'search_lambda',0.5,...
    'fs_calc',[],...
    'fs_out',12e6);
%%
fprintf('Checking Parpool...\n');
poolID = gcp;
%%
estobj.disableParallel = true;
estobj.disableMex = true;
estobj.GPU = false;
fprintf('Using MATLAB:\n  ')
tic
[arfi cc] = estobj.anchored(rf,'refidx',refidx);
toc
%% Motion Filter, Display
arfim = motionfilter(arfi,find(arfi.dims.t<-0.2 | arfi.dims.t>10),2);
arfisc = scanconvert(arfim,'latinc',0.1,'axialinc',0.1);
animate(arfisc,'t','fps',30);
%% Enable MEX
fprintf('Using MEX in for loop:\n  ')
estobj.disableMex = false;
estobj.disableParallel = true;
estobj.GPU = false;
tic
[arfi cc] = estobj.anchored(rf,'refidx',refidx);
toc
%% Enable Parallel
fprintf('Using Parfor MEX:\n  ')
estobj.disableMex = false;
estobj.disableParallel = false;
estobj.GPU = false;
tic
[arfi cc] = estobj.anchored(rf,'refidx',refidx);
toc
%% Enable CUDA
fprintf('Using CUDA-Accelerated MEX:\n  ')
estobj.disableMex = false;
estobj.disableParallel = false;
estobj.GPU = true;
tic
[arfi cc] = estobj.anchored(rf,'refidx',refidx);
toc
%% Slicing and Rescaling
arfi1 = slice(arfisc,'x',find(abs(arfisc.dims.x)<10));
arfi1 = slice(arfi1,'t',find(arfi1.dims.t<10));
arfi1 = rescale(arfi1,'m');
arfi1 = rescale(arfi1,'x','m');
arfi1 = rescale(arfi1,'z','m');
animate(arfi1,'t','fps',30);
%% Filtering
[F f] = fft1(rf.data,rf.params.fs);
rf1 = filter(rescale(rf,'r','meter'),1,[3.25e6 6e6]/(rf.params.c/2),4);
[F1 f1] = fft1(rf1.data,rf1.params.fs);
plot(f,mean(abs(F(:,:)),2),f1,mean(abs(F1(:,:)),2));
xlim([1e6 6e6]);

%% Filtering (2)
estobj = kasai('kernel_lambda',5);
arfi = estobj.progressive(iq,'skipidx',refidx+1,'refidx',refidx);
v = differentiate(arfi,3);
v_filt = filter(v,3,[50 500]*1e-3,4);
v_sc = scanconvert(v_filt,'latinc',0.1,'axialinc',0.1);
v_sc = filter(v_sc,1,[0.2],3);
v_sc = filter(v_sc,2,[0.2],3);
%animate(v,'fps',30)
animate(v_sc,'t','fps',30)
