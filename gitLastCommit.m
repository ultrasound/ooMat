function commitSHA = gitLastCommit(repoPath)
if ~exist('repoPath','var')
    repoPath = pwd;
end
HEADlog = fullfile(repoPath,'.git','logs','HEAD');
if ~exist(HEADlog,'file')
    if strcmp(fileparts(repoPath),repoPath)
        warning('log:Missing','No .git\\logs\\HEAD file found. Unable to retrive git commit information.');
        commitSHA = '';
        return
    else
        commitSHA = gitLastCommit(fileparts(repoPath));
    end
else
    fid = fopen(HEADlog,'r');
    nextLine = 1;
    while nextLine~=(-1)
        currentLine = nextLine;
        nextLine = fgets(fid);
    end
    fclose(fid);
    firstTwoWords = textscan(currentLine,'%s',2,'delimiter',' ');
    commitSHA = firstTwoWords{1}{2};
end