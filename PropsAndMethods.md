# arfiClass
## bmodedata:
	Methods		* bmodedata		* cat		* differentiate		* disp		* filter		* interp		* logcompress		* normalize		* permute		* resample		* rescale		* scanconvert		* slice
	Properties		* data		* units		* dims		* params		* log
## datacontainer:
	Methods		* cat		* datacontainer		* differentiate		* disp		* filter		* interp		* permute		* rescale		* scanconvert		* slice
	Properties		* data		* units		* dims		* params		* log
## displacementdata:
	Methods		* animate		* cat		* differentiate		* disp		* displacementdata		* filter		* interp		* linearmotionfilter		* motionfilter		* permute		* resample		* rescale		* scanconvert		* slice		* struct
	Properties		* data		* units		* dims		* params		* log
## estimator:
Abstract
	Methods		* anchored		* estimate		* estimator		* getprops		* multiresolution		* progressive
## iqdata:
	Methods		* cat		* detect		* differentiate		* disp		* filter		* interp		* iqdata		* permute		* remodulate		* resample		* rescale		* scanconvert		* slice
	Properties		* data		* units		* dims		* params		* log
## kasai:
	Methods		* anchored		* estimate		* getprops		* multiresolution		* progressive
	Properties		* kernel_lambda		* fs_out
## loupas:
	Methods		* anchored		* estimate		* getprops		* multiresolution		* progressive
	Properties		* kernel_lambda		* fs_out
## normxcorr:
	Methods		* anchored		* estimate		* estimatedisp		* getprops		* log		* multiresolution		* normxcorr		* progressive
	Properties		* kernel_lambda		* search_lambda		* fs_calc		* fs_out		* GPU
## pesavento:
	Methods		* anchored		* calc_pesavento		* estimate		* estimatedisp		* getprops		* log		* multiresolution		* pesavento		* progressive
	Properties		* kernel_lambda		* iterations
## piharmoniciqdata:
	Methods		* cat		* detect		* differentiate		* disp		* filter		* interp		* permute		* piharmoniciqdata		* remodulate		* resample		* rescale		* scanconvert		* slice		* sumPulseInversionHarmonic
	Properties		* data		* units		* dims		* params		* log
## pjh7simdata:
	Methods		* cat		* demodulate		* detect		* differentiate		* disp		* filter		* interp		* load		* loadAnalyticSimData		* permute		* pjh7simdata		* resample		* rescale		* save		* scanconvert		* slice
	Properties		* data		* units		* dims		* params		* log
## rfdata:
	Methods		* cat		* demodulate		* detect		* differentiate		* disp		* filter		* interp		* permute		* resample		* rescale		* rfdata		* scanconvert		* slice
	Properties		* data		* units		* dims		* params		* log
## s2000:
	Methods		* Compute_Displacements		* info		* interpMultiTime		* listfileinfo		* load		* s2000
	Properties		* description
## sc2000:
	Methods		* info		* load		* sc2000
	Properties		* description
## scanner:
Abstract
	Methods		* info		* load		* scanner
	Properties		* description
## ultrasounddata:
Abstract
	Methods		* cat		* differentiate		* disp		* filter		* interp		* permute		* resample		* rescale		* scanconvert		* slice		* ultrasounddata
	Properties		* data		* units		* dims		* params		* log