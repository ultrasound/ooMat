function sz = datasize(self,dim)
if ~exist('dim','var')
    dim = 1:ndims(self);
end
if ischar(dim)
    dim = {dim};
end
if iscell(dim)
    for i = 1:length(dim)
        dim1(i) = getdim(self,dim{i});
    end
    dim = dim1;
end
sz = ones(1,length(dim));
for i = 1:length(dim)
    [~,dimName] = getdim(self,dim(i));
    sz(i) = length(self.dims.(dimName));
end