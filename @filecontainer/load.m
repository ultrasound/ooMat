function dataobj = load(self)
    dataobj = datacontainer(self.src_mat.data(self.indices{:}),self.dims);
    dataobj.params = self.params;
    dataobj.units = self.units;
    dataobj.info = self.info;
    dataobj.log = self.log;
end