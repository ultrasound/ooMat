        function [dimidx, dimname, dimunit] = getdim(self,dim_name_or_index)
            %GETDIM Retrieve Filecontainer Dimension Info
            %
            %[dimidx, dimname, dimunit] = getdim(self,dim_name_or_index)
            %GETDIM returns the index, name and unit for the requested
            %dimension, specified by either index or name
            %
            %Inputs
            %  self: <a href="matlab:helpPopup('filecontainer')">filecontainer</a> object
            %      dim: dimension along which to concatenate. May be the index
            %       of the dimension, or the string matching the dimension name 
            %       in self.dims.names
            %
            %Ouputs
            %   dimidx: index of the requested dimension
            %   dimname: string name of the requested dimension
            %   dimunit: string unit of the reqested dimension
            %
            if ischar(dim_name_or_index)
                dimidx = find(strcmp(self.dims.names,dim_name_or_index));
                if isempty(dimidx)
                    if strcmpi(dim_name_or_index,'data')
                        dimidx = 0;
                    else
                        error('No dimension ''%s'' found',dim_name_or_index);
                    end
                end
            else
                dimidx = dim_name_or_index;
            end
            if dimidx == 0
                dimname = 'data';
                dimunit = self.units;
            else
                dimname = self.dims.names{dimidx};
                dimunit = self.dims.units{dimidx};
            end
        end
