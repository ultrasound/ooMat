function self = slicex(self,dim,range,varargin);
%SLICEX slices filecontainer using bounds

[dim,dimName] = self.getdim(dim);
switch class(range)
    case 'char'
        index = find(strcmp(range,self.dims.(dimName)));
        self = slice(self,dim,index);
    case 'cell'
       switch class(range{1})
           case 'char'
               index = zeros(1,numel(range));
               for i = 1:numel(range)
                    index(i) = find(strcmp(range{i},self.dims.(dimName))); 
               end
           otherwise
               index = [];
               for i = 1:numel(range)
                   rangei = range{i};
                   if numel(rangei)==1
                       switch rangei
                           case inf
                               [~,indexi] = max((self.dims.(dimName)));
                           case -inf
                               [~,indexi] = min((self.dims.(dimName)));
                           otherwise
                               [~, indexi] = min(abs(self.dims.(dimName)-rangei));
                       end
                   elseif numel(rangei) == 2
                       indexi = find(self.dims.(dimName)>=rangei(1) & self.dims.(dimName)<=rangei(2));
                   else
                        error('range must be a 1- or 2-element vector');
                   end
               index = [index;indexi(:)];
               end    
       end
       self = slice(self,dim,index);
    otherwise
    if numel(range)==1
        switch range
           case inf
               [~,index] = max((self.dims.(dimName)));
           case -inf
               [~,index] = min((self.dims.(dimName)));
           otherwise
               [~, index] = min(abs(self.dims.(dimName)-range));
       end
        self = slice(self,dim,index);
    elseif numel(range) == 2
        self = slice(self,dim,find(self.dims.(dimName)>=range(1) & self.dims.(dimName)<=range(2)));
    elseif mod(numel(range),2)==0
        idx = [];
        for i = 1:2:numel(range)
            idx0 = find(self.dims.(dimName)>=range(i) & self.dims.(dimName)<=range(i+1));
            idx = [idx;idx0(:)];
        end
            self = slice(self,dim,idx);
    else
        error('range cannot have an odd number of elements ~= 1');
    end
end
if nargin>3
    if length(varargin)>2
        self = slicex(self,varargin{1},varargin{2},varargin{3:end});
    else
        self = slicex(self,varargin{1},varargin{2});
    end
end