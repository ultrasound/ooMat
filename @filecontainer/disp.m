function disp(self)
%DISP overloaded standard display for filecontainer
            left = @(x,n)x(1:(mod(n-1,length(x))+1));
            if numel(self)>1
                builtin('disp',self);
                return
            end
            s = superclasses(self);
            fprintf('\t<a href="matlab:helpPopup %s">%s</a>',class(self),class(self));
            if ~isempty(s)
                for i = 1:length(s)
                    fprintf(' < <a href="matlab:helpPopup %s">%s</a>',s{i},s{i});
                end
            end
            fprintf(':\n\n')
            if ~isempty(self.info)
                fprintf('\tinfo: %s\n\n',self.info);
            end
            fprintf('\t%s:\n','data');
            fprintf('\t\t%s: [%s %s]','size',left(sprintf('%gx',datasize(self)),-1),self.src_details.class)
            if isempty(self.units)
                fprintf(' with no units\n')
            else
                fprintf(' with units of %s\n',self.units)
            end
            fprintf('\t\tsize on disk: [%s %s] (%g bytes)\n',left(sprintf('%gx',self.src_size),-1),self.src_details.class,self.src_details.bytes)
            fprintf('\t\tstored in %s\n',self.src_mat.Properties.Source);
            
            fprintf('\n\t%s:','dims')
            for i = 1:ndims(self)
                fprintf('\n\t\t%s: %s',...
                    sprintf('%s',self.dims.names{i}),...
                    sprintf('[%s %s]',left(sprintf('%gx',size(self.dims.(self.dims.names{i}))),-1),class(self.dims.(self.dims.names{i}))));
                if isempty(self.dims.units{i})
                    fprintf(' with no units')
                else
                    fprintf(' with units of %s',self.dims.units{i})
                end
            end
            fn = fieldnames(self.dims);
            for i = 1:length(fn)
                if ~any(strcmpi(fn{i},[self.dims.names 'units','names']))
                    if ischar(self.dims.(fn{i}))
                        fprintf('\n\t\t%s: ''%s''',fn{i},self.dims.(fn{i}));
                    elseif isnumeric(self.dims.(fn{i})) && numel(self.dims.(fn{i}))==1
                        fprintf('\n\t\t%s: %g',fn{i},self.dims.(fn{i}));
                    else
                        fprintf('\n\t\t%s: [%s %s]',fn{i},left(sprintf('%gx',size(self.dims.(fn{i}))),-1),class(self.dims.(fn{i})));
                    end
                end
            end
            fprintf('\n');
            fprintf('\n\t%s:\n','params')
            fn = fieldnames(self.params);
            for i = 1:length(fn)
                    if ischar(self.params.(fn{i}))
                        fprintf('\t\t%s: ''%s''',fn{i},self.params.(fn{i}));
                    elseif isnumeric(self.params.(fn{i})) && numel(self.params.(fn{i}))==1
                        fprintf('\t\t%s: %g',fn{i},self.params.(fn{i}));
                    else
                        fprintf('\t\t%s: [%s %s]',fn{i},left(sprintf('%gx',size(self.params.(fn{i}))),-1),class(self.params.(fn{i})));
                    end
                    fprintf('\n');
            end
            fprintf('\n\t%s:\n','log')
            if size(self.log,1)>4
                fulllog = '\n';
                for i = 1:size(self.log,1)
                    fulllog = [fulllog '\t\t' strrep(deblank(self.log(i,:)),'''',''''''),'\n'];
                end
                
                fprintf('\t\t<a href="matlab:fprintf(''%s'')">[%g Hidden Lines]</a>\n',fulllog,size(self.log,1)-4);
            end
            for i = max(1,size(self.log,1)-3):size(self.log,1)
                fprintf('\t\t%s\n',self.log(i,:));
            end
            fprintf('\n\t')
            fprintf('<a href="matlab:methods(''%s'')">Methods</a>, ',class(self))
            fprintf('<a href="matlab:properties(''%s'')">Properties</a>, ',class(self))
            fprintf('<a href="matlab:superclasses(''%s'')">Superclasses</a>\n',class(self))
            
        end