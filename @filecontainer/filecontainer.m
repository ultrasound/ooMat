classdef filecontainer
    properties
        units = '';
        info = '';
        dims = struct();
        params = struct();
        log = '';
    end
    properties (Hidden)
        src_mat = [];
        src_size = [];
        src_ndims = 0;
        src_details = [];
        indices = {};
    end
    methods 
        function self = filecontainer(filename);
            if ~exist(filename,'file')
                error('%s not found!',filename);
            end
            m = matfile(filename);
            self.info = m.info;
            self.dims = m.dims;
            self.params = m.params;
            self.log = m.log;
            self.src_ndims = length(self.dims.names);
            self.src_size = ones(1,self.src_ndims);
            tmpsz = size(m,'data');
            self.src_size(1:length(tmpsz)) = tmpsz;
            for i = 1:self.src_ndims
                self.indices{i} = 1:self.src_size(i);
            end
            self.src_mat = m;
            self.src_details = whos(m,'data');
        end
        
        self = load(self);
        self = slice(self,dim,index,varargin)
        self = slicex(self,dim,range,varargin);
        [dimidx, dimname, dimunit] = getdim(self,dim_name_or_index);
        sz = datasize(self,dim)
        isadim = isdim(self,dimname)
        n = ndims(self)
        disp(self);
        
    end

end