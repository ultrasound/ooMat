function scl = getunitconversion(unit0,unit1,unitratio,constant)
%GETUNITCONVERSION get SI unit conversion factors
%scl = getunitconversion(unit0,unit1);
%scl =
%getunitconversion(unit0,unit1,unitratio,constant);
%
%GETUNITCONVERSION returns a scaling factor that can be multiplied by
%values with units unit0 to get values in units of unit1. With two inputs,
%unit0 and unit1 must be the same type. With 4 inputs, a conversion ratio
%and value specify the constant that links the two unlike types.
%
%INPUTS:
%   unit0: String or char array containing source SI-prefixed unit
%   unit1: String or char array containing destination SI-prefixed unit
%   [unitratio]: string containing coversion ratio units i.e. 'm/s'
%   [constant]: numeric conversion i.e. 1540
%
%OUTPUT:
%   scl: scaling value to multiply by values in units of unit0 to get units
%   of unit1.
%
% Currently supported units include:
%   -Linear distance (m,meter,micron)
%   -Angles (deg,rad,degree,radian,� (sprintf('\272')) )
%   -Time (sec,second,min,minute,hr,hour,day)
% Also supported are
%   -plurals (meters,seconds,degrees)
%   -SI letter prefixes (mm,ms,km)
%   -SI text prefixes (microseconds,kilometer,Terameters)
%   -multiple representations of greek mu (\mu,u,� (sprintf('\265') )
%   -ratios separated by '/' (m/s,km/hour,rad/s)

if ~(nargin==2 || nargin==4)
    error('incorrect number of input arguments')
end
if isempty(unit0)
    scl = 1;
    return
end
if nargin==4
    slashr = strfind(unitratio,'/');
    unit0 = char(unit0); %convert string to char array
    unit1 = char(unit1); %convert string to char array
    if (contains(unit0,'/') || contains(unit1,'/'))
        error('ratios of ratios not supported.')
    end
    if isempty(slashr)
        error('conversion unit ratio must have a ''/'' symbol')
    end
    unitn = unitratio(1:slashr-1);
    unitd = unitratio(slashr+1:end);
    type0 = getunittype(unit0);
    type1 = getunittype(unit1);
    typen = getunittype(unitn);
    typed = getunittype(unitd);
    if strcmp(type0,typed) && strcmp(type1,typen)
        scl = getunitconversion(unit0,unitd) * constant * getunitconversion(unitn,unit1);
    elseif strcmp(type0,typen) && strcmp(type1,typed)
        scl = getunitconversion(unit0,unitn) * 1/constant * getunitconversion(unitd,unit1);
    elseif strcmp(type0,type1)
        scl = getunitconversion(unit0,unit1);
    else
        error('unit type mismatch %s -> (%s/%s) -> %s',type0,typen,typed,type1);
    end
else
    unit0 = char(unit0); %convert string to char array
    unit1 = char(unit1); %convert string to char array
    slash0 = strfind(unit0,'/');
    slash1 = strfind(unit1,'/');
    if length(slash0)==1 && length(slash1)==1
        num0 = unit0(1:slash0-1);
        denom0 = unit0(slash0+1:end);
        num1 = unit1(1:slash1-1);
        denom1 = unit1(slash1+1:end);
        scl = getunitconversion(num0,num1)/getunitconversion(denom0,denom1);
    elseif isempty(slash0) && isempty(slash1)
        type0 = getunittype(unit0);
        type1 = getunittype(unit1);
        if ~strcmpi(type0,type1)
            error('unit type mismatch (%s vs %s).',type0,type1);
        end
        if strcmp(type0,'other')
            if ~strcmp(unit0(end),unit1(end))
                error('cannot convert %s to %s',unit0,unit1);
            end
            i = 0;
            while (i< min(length(unit0),length(unit1))) && strcmp(unit0(end-i:end),unit1(end-i:end)) 
                type = unit0(end-i:end);
                i = i+1;
            end
            scl0 = getsiscale(unit0,type);
            scl1 = getsiscale(unit1,type);
            scl = scl0/scl1;
        else    
            scl0 = getsiscale(unit0,type0);
            scl1 = getsiscale(unit1,type0);
            scl = scl0/scl1;
        end
    else
        error('unit ratio mismatch (%s vs %s).',unit0,unit1);
    end
end
end

function type = getunittype(unit)
    switch lower(unit)
        case {'micron','microns'}
            type = 'distance';
        case {'minute','minutes','min','mins','hour','hours','hr','hrs','day','days','d'}
            type = 'time';
        case {'rad','deg','radian','radians','degree','degrees',sprintf('\272')}
            type = 'angle';
        otherwise
            if contains(unit,'sec')
                type = 'time';
            elseif contains(unit,'meter')
                type = 'distance';
            elseif contains(unit,'micron')
                type = 'distance';    
            elseif strcmpi(unit(end),'s')
                type = 'time';
            elseif strcmpi(unit(end),'m')
                type = 'distance';
            else
                type = 'other';
                %error('unkown unit %s',unit);
            end
    end

end

function scl = getsiscale(unit,type)
switch type
    case 'distance'
        idx = strfind(unit,'meters');
        if isempty(idx)
            idx = strfind(unit,'meter');
            if isempty(idx)
                if strcmpi(unit,'micron') || strcmpi(unit,'microns')
                    idx = 6;
                else
                        idx = strfind(unit,'m');
                        if isempty(idx)
                            idx = length(unit)+1;
                        end

                end
            end
        end
    case 'time'
        idx = strfind(unit,'seconds');
        if isempty(idx)
            idx = strfind(unit,'second');
            if isempty(idx)
                idx = strfind(unit,'sec');
                if isempty(idx)
                    idx = strfind(unit,'s');
                    if isempty(idx)
                        idx = length(unit)+1;
                    end
                end
            end
        end
    case 'angle'
        idx = length(unit)+1;
    otherwise
        idx = length(unit)-length(type)+1;
end
idx = idx(end);
prefix = unit(1:idx-1);
if isempty(prefix)
    scl = 1;
else
    switch prefix
        case {'pico','p'}
            scl = 1e-12;
        case {'nano','n'}
            scl = 1e-9;
        case {'micro','u','\mu',sprintf('\265')}
            scl = 1e-6;
        case {'milli','m'}
            scl = 1e-3;
        case {'centi','c'}
            scl = 1e-2;
        case {'',[]}
            scl = 1;
        case {'kilo','k'}
            scl = 1e3;
        case {'mega','M'}
            scl = 1e6;
        case {'giga','G'}
            scl = 1e9;
        case {'tera','T'}
            scl = 1e12;
        case {'min','minute'}
            scl = 60;
        case {'hour','hr'}
            scl = 60*60;
        case {'day','d'}
            scl = 60*60*24;
        case {'rad','radian','radians'}
            scl = 1;
        case {'deg','degree','degrees',sprintf('\272')}
            scl = 2*pi/360;
        otherwise
            if isempty(prefix)
                scl = 1;
            else
                error('unkown prefix %s',prefix);
            end
    end
end
end
