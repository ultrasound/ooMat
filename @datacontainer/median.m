        function self = median(self,dim)
            if iscell(dim)
                for i = 1:length(dim)
                    [dimidx(i), dimname{i}, dimunit{i}] = getdim(self,dim{i});
                    msz(i) = size(self.data,dimidx(i));
                end
                    pOrder = [1:ndims(self)];
                    pOrder(dimidx) = 0;
                    pOrder = sort(pOrder);
                    pOrder = [dimidx pOrder(length(dim)+1:end)];
                    [~,rOrder] = sort(pOrder);
                    self = permute(self,pOrder);
                    data = nanmedian(reshape(self.data,prod(msz),[]));
                for i = 1:length(dim)
                    self = median(self,i);
                end
                    self.data(:) = data(:);
                    self = permute(self,rOrder);
                return
            end
            [dim,dimname] = self.getdim(dim);
            self.data = nanmedian(self.data,dim);
            self.dims.(dimname) = nan;
            self.log = char(self.log,sprintf('median value taken in the %s dimension',dimname));
        end