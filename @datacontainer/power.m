 function self = power(self,B)
            if isnumeric(self)
                A = self;
                self = B;
                self.data = A.^self.data;
                self.units = '';
                self.log = char(self.log,sprintf('%g raised to the data-th power',A));
            elseif isnumeric(B)
                self.data = self.data.^B;
                if ~isempty(self.units)
                self.units = sprintf('%s^%g',self.units,B);
                end
                self.log = char(self.log,sprintf('data raised to the %s power',nth(B)));
            elseif isa(B, 'datacontainer')
                self.data = self.data.^B.data;
                if ~isempty(self.units) && ~isempty(B.units)
                    self.units = sprintf('%s^%s',self.units,B.units);
                end
            else
                error('Type Mismatch');
            end
 end
        
 function sfx = nth(n)
    switch n
        case 1
            sfx = '1st';
        case 2
            sfx = '2nd';
        case 3
            sfx = '3rd';
        otherwise
            sfx = sprintf('%gth',n);
    end
end