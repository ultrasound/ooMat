function self = concatenate(self,dim,varargin)
        %CONCATENATE Concatenate in specified dimension
        %self = CONCATENATE(self,dim,self1,self2,...)
        %
        %CONCATENATE operates similarly to the built-in method CONCATENATE, but manages the
        %concatenation of the data dimensions and connected parameters.
        %
        %Inputs:
        %  self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %      dim: dimension along which to concatenate. May be the index
        %       of the dimension, or the string matching the dimension name 
        %       in self.dims.names. self can be an empty
        %       datacontainer, in which case it is assigned to self1
        %       (and concatenated with self2, etc.)
        %  self1, self2, etc: datacontainers to be concatenated
        %
        %Outputs:
        %  self: concatenated object
        %
        %CONCATENATE searches the parameters in self.params for parameters that
        %match the size of self.data in the dim dimension, with matching
        %sizes or singleton sizes in the rest of the dimensions. It then
        %attempts to concatenate each of these parameter matrices in the
        %dim dimension. If size(self.data,dim) == 1 and the parameter (x) fits the
        %size of the data (or singleton) in the other dimensions,
        %concatenation will occur only if the parameters (x) are not identical.
        %If the size(self.param.(x),dim) == 1, and
        %size(self.data,dim) > 1 and x differs between self and
        %selfi, repmat() will be used to make the output parameter x
        %have the same size in the dim dimension as the output data. This
        %ought to handle issues with using slice and cat to split and
        %re-form data.
        %
        % See Also: datacontainer/slice
        %
            if nargin<3
                if nargin == 2 && length(self)>1
                    for i = 1:length(self);
                    v{i} = builtin('subsref',self,struct('type','()','subs',{{i}}));
                    end
                    self0 = concatenate(v{1},dim,v{2:end});
                    self = self0;
                end
                return
            end
            if isempty(self)
                self = varargin{1};
                if length(varargin)>1
                self = self.concatenate(dim,varargin{2:end});
                end
                return
            end
            [dim,dimname] = self.getdim(dim);
            for obji = 1:length(varargin)
                selfi = varargin{obji};
                sz0 = ones(1,ndims(self));
                sz0(1:ndims(self.data)) = size(self.data);
                szi = ones(1,ndims(selfi));
                szi(1:ndims(selfi.data)) = size(selfi.data);
                self.data = cat(dim,self.data,selfi.data);
                if iscell(self.dims.(dimname))
                    if iscell(selfi.dims.(dimname))
                        self.dims.(dimname) = [self.dims.(dimname)(:); selfi.dims.(dimname)(:)]';
                    elseif ischar(selfi.dims.(dimname))
                        self.dims.(dimname) = [self.dims.(dimname)(:); selfi.dims.(dimname)]';
                    else
                        error('cannot concatenate %s to cell array',class(selfi.dims.(dimname)));
                    end
                elseif ischar(self.dims.(dimname))
                     if iscell(selfi.dims.(dimname))
                        self.dims.(dimname) = [self.dims.(dimname)(:); selfi.dims.(dimname)(:)]';
                    elseif ischar(selfi.dims.(dimname))
                        self.dims.(dimname) = {self.dims.(dimname),selfi.dims.(dimname)};
                    else
                        error('cannot concatenate %s to char',class(selfi.dims.(dimname)));
                     end
                else
                    self.dims.(dimname) = [self.dims.(dimname)(:);selfi.dims.(dimname)(:)];
                end
                paramNames = fieldnames(self.params);
                for i = 1:length(paramNames)
                    param0 = self.params.(paramNames{i});
                    parami = selfi.params.(paramNames{i});
                    if isnumeric(param0) || islogical(param0)
                        [szp0, szpi] = deal(ones(1,length(sz0)));
                        for dimidx = 1:length(sz0)
                            szp0(dimidx) = size(param0,dimidx);
                            szpi(dimidx) = size(parami,dimidx);
                        end
                        if all(szp0 == sz0 | szp0 == 1) && all(szpi == szi | szpi == 1) && all(szp0([1:dim-1 dim+1:end])==szpi([1:dim-1 dim+1:end]))
                            if all(szp0 == szpi)
                                paramcat = cat(dim,param0,parami);
                                d = abs(diff(paramcat,1,dim));
                                if any(d(:)>1e-6*mean(paramcat(:))) || szp0(dim)>1 || szpi(dim)>1
                                    if any(d(:)>0) && szp0(dim) == 1 && szpi(dim) == 1 && (sz0(dim)>1 || szi(dim)>1)
                                    rpmat0 = ones(1,length(szp0));
                                    rpmat0(dim) = sz0(dim);
                                    rpmati = ones(1,length(szpi));
                                    rpmati(dim) = szi(dim);
                                    param0 = cat(dim,repmat(param0,rpmat0),repmat(parami,rpmati));
                                    else
                                    param0 = paramcat;
                                    end
                                end
                            elseif (szp0(dim) == sz0(dim)) && (szpi(dim) == szi(dim))
                                param0 = cat(dim,param0,parami);
                            end
                        end
                    self.params.(paramNames{i}) = param0;
                    end
                end
            end
        end