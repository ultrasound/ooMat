function [self, selfminus] = directionalfilter(self,dim1,dim2)
 %DIRECTIONALFILTER Directionally-filter data
            %self = directionalfilter(self,dim1,dim2)
            %[selfp selfm] = directionalfilter(self,dim1,dim2)
            %
            %DIRECTIONALFILTER returns a datacontainer object filtered in
            %K-space for motion in the d{dim1}/d{dim2} direction. An
            %optional second output also returns motion in the
            %-d{dim1}/d{dim2} direction. Dimension specifications can
            %include sign conventions, as trailing '+' or '-' for strings,
            %or as the sign of a numeric dimension.
            %
            %INPUTS:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %       dim1: numerator dimension. May be the index of the dimension,
            %           or the string matching the dimension name in
            %           self.dims.names. May have a sign 'x+','x-' or
            %           -2. Default direction is positive.
            %       dim2: denominator dimension.
            %
            %OUTPUTS:
            %   self: filtered datacontainer
            %   selfminus: opposite filtered datacontainer
            %
            
if ~exist('dim1','var')
    dim1 = 2;
end
if ~exist('dim2','var')
    dim2 = 3;
end
[dim1, sgn1] = parsedim(dim1);
[dim2, sgn2] = parsedim(dim2);
[dim1,dimname1] = self.getdim(dim1);
[dim2,dimname2] = self.getdim(dim2);

dimorder = 1:ndims(self);
dimorder([dim1 dim2]) = nan;
dimorder = sort(dimorder);
dimorder = [dim1 dim2 dimorder(1:end-2)];
[srted, sortorder] = sort(dimorder);
self = permute(self,dimorder);
sz0 = ones(1,ndims(self));
sz0(1:ndims(self.data)) = size(self.data);
sz = 2.^(max(0,nextpow2(sz0)));
mask = isnan(self.data);
self.data(mask) = 0;
F = fft(fft(self.data(:,:,:),sz(1),1),sz(2),2);
k1 = linspace(-1,1,sz(1));
k2 = linspace(-1,1,sz(2));
[K1, K2] = ndgrid(k1,k2);

if nargout>1
selfminus = self;
msk = (K1.*K2*sgn1*sgn2>=0);
MSK = repmat(msk,[1 1 size(F,3)]);
F1 = F.*MSK;
data1 = real(ifft(ifft(F1,[],2),[],1));
selfminus.data = reshape(data1(1:sz0(1),1:sz0(2),:),size(self.data));
selfminus.data(mask) = nan;
selfminus = permute(selfminus,sortorder);
selfminus.log = char(selfminus.log,sprintf('Directionally filtered in the %s d%s/d%s direction',char(44-sgn1*sgn2),dimname1,dimname2));
end

msk = (K1.*K2*sgn1*sgn2<=0);
MSK = repmat(msk,[1 1 size(F,3)]);
F1 = F.*MSK;
data1 = real(ifft(ifft(F1,[],2),[],1));
self.data = reshape(data1(1:sz0(1),1:sz0(2),:),size(self.data));
self.data(mask) = nan;
self = permute(self,sortorder);
self.log = char(self.log,sprintf('Directionally filtered in the %s d%s/d%s direction',char(44+sgn1*sgn2),dimname1,dimname2));
end

function [dim, sgn] = parsedim(dim)
if ischar(dim) 
    switch(dim(end))
        case '+';
            sgn = 1;
            dim = dim(1:end-1);
        case '-';
            sgn = -1;
            dim = dim(1:end-1);
        otherwise
            sgn = 1;
    end
else
    sgn = sign(dim);
    dim = abs(dim);
end    
end
