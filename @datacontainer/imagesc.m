function [h cb] = imagesc(self,varargin)
sz = size(self.data);
nonsingleton = find(sz>1);
if length(nonsingleton)~=2
    error('datacontainer must have exactly two non-singleton dimensions')
end
varg = varargin;
narg = length(varg);
specopts = struct('clim','auto','colorbar','on');
if(mod(narg,2)==1)
    args = {varg{1:end-1}, 'clim', varg{end}};
else
    args = varg;
end
[specopts,options] = setopts(specopts,args);
xdim = nonsingleton(2);
xName = self.dims.names{xdim};
xUnit = self.dims.units{xdim};
zdim = nonsingleton(1);
zName = self.dims.names{zdim};
zUnit = self.dims.units{zdim};
if iscell(self.dims.(xName))
    x = 1:length(self.dims.(xName));
else
    x = self.dims.(xName);
end
if iscell(self.dims.(zName))
    z = 1:length(self.dims.(zName));
else
    z = self.dims.(zName);
end
if (any(abs(diff(diff(x)))>0.01*mean(abs(diff(x)))) || any(abs(diff(diff(z)))>0.01*mean(abs(diff(z)))));
    x = reshape(x,[],1);
    x = [x(1)-(x(2)-x(1))/2; (x(1:end-1)+x(2:end))/2; x(end) + (x(end)-x(end-1))/2];
    z = reshape(z,[],1);
    z = [z(1)-(z(2)-z(1))/2; (z(1:end-1)+z(2:end))/2; z(end) + (z(end)-z(end-1))/2];
    h0 = surf(x,z,zeros(size(squeeze(self.data))+[1 1]),squeeze(self.data),'edgecolor','none');
    setprops(h0,options{:});
    set(h0,'facecolor','texture')
    %if exist('clim','var')
    %end
    view([0 90]);
    zlim([-0.1 0.1]);
    axis ij
    axis tight
else
    h0 = imagesc(x,z,squeeze(self.data));
    
    setprops(h0,options{:});
end
if iscell(self.dims.(xName));
    set(h0.Parent,'XTick',x,'XTickLabel',self.dims.(xName));
end
if iscell(self.dims.(zName));
    set(h0.Parent,'YTick',z,'YTickLabel',self.dims.(zName));
end
set(h0,'alphadata',~isnan(get(h0,'cdata')),'alphadatamapping','none')
if ischar(specopts.clim) && strcmp(specopts.clim,'auto')
    specopts.clim = autoclim(self.data(:));
end
set(gca,'clim',specopts.clim);
xlabel(genlabel(self,xdim))
ylabel(genlabel(self,zdim));
h0.UserData.colorbar = [];
if ~isempty(self.units)
    switch lower(specopts.colorbar)
        case 'on'
            h0.UserData.colorbar = colorbar;
            ylabel(h0.UserData.colorbar,self.units);
        case 'off'
        otherwise
            error('unknown colorbar option')
    end
end
if ~isempty(self.info)
    title(self.info)
end
if strcmp(zUnit,xUnit) && ~isempty(zUnit) && abs(log10(diff(x([1 end]))/diff(z([1 end]))))<1
    axis image
end
if nargout >= 1
    h = h0;
end
if nargout == 2
    cb = h.UserData.colorbar;
end
end