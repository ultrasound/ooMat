function histobj = hist(self,dim,edges);
if ~exist('edges','var')
    [~,edges] = histcounts(self.data(:),'BinMethod','fd');
end
histobj = self;
[dim dimName] = getdim(self,dim);
sz = datasize(self);
sz(dim) = length(edges)-1;