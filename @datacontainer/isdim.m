function isadim = isdim(self,dimname)
            if ischar(dimname)
                isadim = any(strcmp(self.dims.names,dimname));
            else
                isadim = any(dimname==[1:ndims(self)]);
            end
        end