function self = integrate(self, dim)
        %INTEGRATE Compute integral in specified dimension
        %self = integrate(self,dim)
        %
        %INPUTS:
        % self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %     dim: active dimension. May be the index of the dimension,
        %       or the string matching the dimension name in
        %       self.dims.names
        %
        %OUTPUTS:
        % self: output datacontainer
        %
        %INTEGRATE take the cumulative sum (cumsum) of self.data
        %in the specified dimension, adjusting the self.units parameter
        %accordingly.
        %
        %See Also: cumsum
            if ~exist('dim','var')
                dim = find(strcmpi(self.dims.names,'t'));
                if isempty(dim)
                    warning('dim:nodim','Could not find default ''t'' dimension. Using 3rd dimension ''%s''',self.dims.names{3});
                    dim = 3;
                else
                    warning('dim:t','No dimension specified. Using ''t'' dimension.');
                end
            end
            [dim, dimname, dimunit] = self.getdim(dim); %#ok<ASGLU>
            log = self.log;
            if self.datasize(dim)<2
                error('cannot integrate fewer than two values');
            end
            x0 = self.dimval(dim);
            x = x0(1)-(x0(2)-x0(1))/2;
            for i = 1:length(x0)
                x(i+1) = x0(i)*2 - x(i);
            end
            y0 = self.setdim(dim,diff(x));
            y0 = y0.times(y0.dimval(dim,'match'));
            y0.data = cumsum(y0.data, dim);
            y = y0.slice(dim,1).times(0).concatenate(dim, y0);
            self = y.setdim(dim, x);
            if isempty(self.units)
                self.units = dimunit;
            else
                slashidx = strfind(self.units,'/');
                if ~isempty(slashidx) && strcmp(self.units(slashidx+1:end),dimunit)
                    if slashidx == 1
                        self.units = '';
                    else
                        self.units = self.units(1:slashidx-1);
                    end
                else
                    self.units = sprintf('%s*%s',self.units,dimunit);
                end
            end
            self.log = char(log,sprintf('integrated in the %s dimension',self.dims.names{dim}));
        end