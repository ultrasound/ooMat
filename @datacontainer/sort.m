function self = sort(self,dim,mode);
if ~exist('mode','var')
    mode = 'ascend';
end
sz = self.datasize;
[dim dimName dimUnit] = self.getdim(dim);
if any(sz([1:dim-1 dim+1:end])>1)
    error('Data can only have one nonsingleton dimension to be sorted');
end
log = self.log;
[~,index] = sort(self.data,dim,mode);
self = slice(self,dim,index);
self.log = char(log,sprintf('sorted data in the %s dimension (%sing)',dimName,mode));