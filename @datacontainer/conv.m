%CONV convolve datacontainer with N-D array
%
% self = conv(self,g,dim,shape)
function self = conv(self,g,dim,shape)
sz = size(g);
if iscell(dim)
    dims = dim;
else
    dims = {dim};
end
if isa(g,'datacontainer')
    dims = g.dims.names;
    g = g.data;
end
if sum(sz>1) > length(dims)
    error('Not enough dimensions specified')
end
if ~exist('shape','var')
    shape = 'same';
end
if length(dims) == 1;
    g = g(:);
end
pidx = 1:self.ndims;
dim = nan(1,length(dims));
for i = 1:length(dims);
    [dim(i) dimName{i}] = self.getdim(dims{i});
    pidx(dim(i)) = -1;
end
pidx = sort(pidx);
pidx(1:length(dim)) = dim;
[~,sortidx] = sort(pidx);
g = permute(g,sortidx);
log = self.log;
data = self.data;
switch shape
    case 'valid'
        for i = 1:self.ndims
            index = find(conv(ones(self.datasize(i),1),ones(size(g,i),1),'valid'));
            self = self.slice(i,index);
        end
    case 'same'
    otherwise
        error('invalid shape specification. Must be ''valid'' or ''same''.')
end
self.data = convn(data,g,shape);
left = @(x,n)x(1:end-n);
if length(dims) == 1
    self.log = char(log,sprintf('Convolved with a %g-sample kernel in the %s dimension',length(g),dimName{1}));
else
    szg = size(g);
    self.log = char(log,sprintf('Convolved with a %s kernel in the [%s] dimensions',left(sprintf('%gx',szg(dim)),1),left(sprintf('%s,',dimName{:}),1)));
end

    