function self = rdivide(self,B)
            if isnumeric(self)
                self = B.ldivide(self);
            elseif isnumeric(B)
                sza = datasize(self);
                for i = 1:length(sza)
                    szb(i) = size(B,i); %#ok<AGROW>
                end
                if any(szb>1 & sza~=szb)
                    error('Size Mismatch')
                end
                if ~all(szb==1)
                B = repmat(B,sza./szb);
                self.data = self.data./B;
                self.log = char(self.log,sprintf('divided by outside data'));
                else
                self.data = self.data/B;
                self.log = char(self.log,sprintf('divided by %g',B));
                end
            elseif isa(B,'datacontainer')
                [self,B] = broadcast(self,B);
                %sza = datasize(self);
                %szb = datasize(B);
                %if ndims(self)~=ndims(B) || any(szb>1 & sza~=szb)
                %    error('Size Mismatch')
                %end
                %B.data = repmat(B.data,sza./szb);
                if strcmp(self.units,B.units)
                    self.units = '';
                else
                    if length(self.units)>(length(B.units)+1) && strcmp(self.units(end-length(B.units):end),[char(183),B.units])
                            self.units = self.units(1:end-length(B.units)-1);
                    else
                            self.units = [self.units '/' B.units];
                    end
                end
                self.data = self.data./B.data;
                self.log = char(self.log,sprintf('Divided by %s',class(B)));
            else
                error('Type mismatch')
            end
        end