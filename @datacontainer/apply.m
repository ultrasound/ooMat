function self = apply(self, func, varargin)
    self.data = func(self.data,varargin{:});
    self.log = char(self.log,'applied anonymous function');
end