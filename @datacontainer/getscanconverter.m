function anonfun = getscanconverter(spec)
switch lower(spec)
    case 'sector'
        anonfun = @(x,y,apex)deal(atan2(x,y-apex),sqrt(x.^2+(y-apex).^2)+apex);
    case 'vector'
        anonfun = @(x,y,apex)deal(atan2(x,y-apex),sqrt(x.^2+(y-apex).^2)+apex./cos(atan2(x,y-apex)));
    otherwise
        error('Unknown scanconversion spec %s in %s',spec,mfilename('fullpath'));
end