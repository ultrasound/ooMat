function self = differentiate(self,dim,spacing)
        %DIFFERENTIATE Compute derivative in specified dimension
        %self = differentiate(self,dim)
        %
        %INPUTS:
        % self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %     dim: active dimension. May be the index of the dimension,
        %       or the string matching the dimension name in
        %       self.dims.names
        % spacing: optional spacing to use - defaults to 1. 
        %
        %OUTPUTS:
        % self: output datacontainer
        %
        %DIFFERENTIATE take the partial derivative (diff) of self.data
        %in the specified dimension, adjusting the self.units parameter
        %accordingly, and shortening self.dims.(dim)
        %
        %See Also: diff
            if ~exist('dim','var')
                dim = find(strcmpi(self.dims.names,'t'));
                if isempty(dim)
                    warning('dim:nodim',sprintf('Could not find default ''t'' dimension. Using 3rd dimension ''%s''',self.dims.names{3}));
                    dim = 3;
                else
                    warning('dim:t',sprintf('No dimension specified. Using ''t'' dimension.'));
                end
            end
            if ~exist('spacing','var')
                spacing = 1;
            end
            [dim, dimname, dimunit] = self.getdim(dim);
            datap = permute(self.data,[dim 1:dim-1 dim+1:ndims(self.data)]);
            szp = size(datap);
            t = reshape(self.dims.(self.dims.names{dim}),[],1);
            T = repmat(t,[1 szp(2:end)]);
            [k, kt0, kt1] = deal(zeros(spacing+1,1));
            k(1) = 1;
            k(end) = -1;
            kt0(end) = 0.5;
            kt1(1) = 0.5;
            t1 = convn(t,kt0,'valid')+convn(t,kt1,'valid');
            self.data = permute(convn(datap,k,'valid')./convn(T,k,'valid'),[2:dim 1 dim+1:ndims(self.data)]);
            self.dims.(self.dims.names{dim}) = t1;
            staridx = strfind(self.units,'*');
            if strcmp(self.units, dimunit)
                self.units = '';
            else
                if ~isempty(staridx) && strcmp(self.units(staridx+1:end),dimunit)
                    self.units = self.units(1:staridx-1);
                else
                    self.units = sprintf('%s/%s',self.units,dimunit);
                end
            end
            self.log = char(self.log,sprintf('differentiated in the %s dimension',dimname));
        end