function self = scanconvert(self,varargin)
            %SCANCONVERT Scan-convert sector/vector datacontainer
            %self = SCANCONVERT(self,varargin)
            %
            %SCANCONVERT converts the data in self from a polar
            %coordinate system to a cartesian coordinate system, and
            %returns another datacontainer object.
            %
            %Inputs:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %  varargin: a list of parameter/value pairs that modify the
            %       options, which are:
            %
            % axial_min: output axial lower bound (mm) axial_max: output
            % axial upper bound (mm)
            %  axialinc: output axial increment (mm)
            %   lat_min: output lateral lower bound (mm) lat_max: output
            %   lateral upper bound (mm)
            %    latinc: output lateral increment (mm)
            %  scanmode: apex interpretation
            %
            %Output:
            %   self: scanconverted datacontainer
            %
            %If the bounds are not specified, they will default to the
            %calculated minimum and maximum bounds of the data, with 0.1 mm
            %spacing. If scanmode is left as 'auto', the program looks for
            %self.dims.geometry to determine if scanmode should be
            %'vector', for phased arrays, or 'sector', for curvilinear
            %arrays. If it does not find the geometry field, it will use
            %'vector'. Scan conversion is done using MEX code.

            if ~isfield(self.dims,'th')
                if isfield(self.dims,'thp') && isfield(self.dims,'dth')
                    self = self.rescale('thp','deg');
                    self = self.rescale('dth','deg');
                    [THP,DTH] = meshgrid(self.dims.thp,self.dims.dth);
                    th = THP(:)+DTH(:);
                    pdim = find(strcmpi(self.dims.names,'thp'));
                    newdata = [];
                    for i = 1:length(self.dims.thp)
                        selfi = slice(self,pdim,i);
                        thi = selfi.dims.thp+selfi.dims.dth;
                        sc = struct('fs',self.params.fs,'min_th',min(thi),'span_th',max(thi)-min(thi),'apex',self.dims.apex,'axialmin',0,'axialmax',max(self.dims.r),'axialinc',0.1,'latmin',max(self.dims.r)*sind(min(th)),'latmax',max(self.dims.r)*sind(max(th)),'latinc',0.1,'scanmode','auto');
                        if nargin>1
                            sc = setfields(sc,varargin);
                        end
                        if strcmpi(sc.scanmode,'auto')
                            if ~isfield(self.dims,'geometry')
                                sc.scanmode = 'vector';
                            else
                                switch lower(self.dims.geometry)
                                    case 'phased'
                                        sc.scanmode = 'sector';
                                    case 'curvilinear'
                                        sc.scanmode = 'vector';
                                    otherwise
                                        sc.scanmode = 'sector';
                                end
                            end
                        end
                        if ~isreal(selfi.data)
                            [idata axial lat] = scan_convert(sc.scanmode,real(selfi.data),deg2rad(sc.min_th),deg2rad(sc.span_th),sc.apex*1e-3,2,sc.fs,NaN,[(sc.axialmin-sc.apex) (sc.axialmax-sc.apex) sc.axialinc sc.latmin sc.latmax sc.latinc]*1e-3);
                            [qdata axial lat] = scan_convert(sc.scanmode,imag(selfi.data),deg2rad(sc.min_th),deg2rad(sc.span_th),sc.apex*1e-3,2,sc.fs,NaN,[(sc.axialmin-sc.apex) (sc.axialmax-sc.apex) sc.axialinc sc.latmin sc.latmax sc.latinc]*1e-3);
                            tmpdata = complex(idata,qdata);
                        else
                            [tmpdata axial lat] = scan_convert(sc.scanmode,selfi.data,deg2rad(sc.min_th),deg2rad(sc.span_th),sc.apex*1e-3,2,sc.fs,NaN,[(sc.axialmin-sc.apex) (sc.axialmax-sc.apex) sc.axialinc sc.latmin sc.latmax sc.latinc]*1e-3);
                        end
                        newdata = cat(pdim,newdata,tmpdata);
                    end
                    self.data = newdata;
                    self.dims = rmfield(self.dims,{'r','dth','apex'});
                    self.dims.names{1} = 'z';
                    self.dims.names{2} = 'x';
                    self.dims.units{1} = 'mm';
                    self.dims.units{2} = 'mm';
                    self.dims.z = axial*1e3;
                    self.dims.x = lat*1e3;
                else
                    warning('sc:input','data is not in sector format');
                end
            else
                if ~isfield(self.dims,'apex')
                    self.dims.apex = 0;
                end
                                
                if length(self.dims.th) == 1 && isfield(self.dims,'thp') && length(self.dims.thp)>1
                     self = self.rescale('thp','deg');
                     thname = 'thp';
                else
                    self = self.rescale('th','deg');
                    thname = 'th';
                end
                
                sc = struct('fsiq',self.params.fs,'min_th',min(self.dims.(thname)),'span_th',max(self.dims.(thname))-min(self.dims.(thname)),'apex',self.dims.apex,'axialmin',0,'axialmax',max(self.dims.r),'axialinc',0.1,'latmin',min((self.dims.r-self.dims.apex)*sind(min(self.dims.(thname)))),'latmax',max((self.dims.r-self.dims.apex)*sind(max(self.dims.(thname)))),'latinc',0.1,'scanmode','auto');
                dim = getdim(self,thname);
                self = permute(self,[1 dim 2:dim-1 dim+1:ndims(self)]);
                
                
                if nargin>1
                    sc = addfields(sc,varargin);
                end
                if strcmpi(sc.scanmode,'auto')
                    if ~isfield(self.dims,'geometry')
                        sc.scanmode = 'vector';
                    else
                        switch lower(self.dims.geometry)
                            case 'phased'
                                sc.scanmode = 'sector';
                            case 'curvilinear'
                                sc.scanmode = 'vector';
                            otherwise
                                sc.scanmode = 'sector';
                        end
                    end
                end
                self.params.scanconversion = sc;
                self.log = char(self.log,datestr(now),sprintf('%s scan-converted',sc.scanmode));
                
                datavec = self.data(:);
                if any(isinf(datavec));
                    datavec(isinf(datavec)) = nan;
                    nmax = nanmax(datavec);
                    nmin = nanmin(datavec);
                    self.data(self.data==inf) = nmax;
                    self.data(self.data==-inf) = nmin;                    
                end
                if ~isreal(self.data)
                    [idata axial lat] = datacontainer.scan_convert(sc.scanmode,real(self.data),deg2rad(sc.min_th),deg2rad(sc.span_th),sc.apex*1e-3,2,sc.fsiq,NaN,[(sc.axialmin-sc.apex) (sc.axialmax-sc.apex) sc.axialinc sc.latmin sc.latmax sc.latinc]*1e-3);
                    [qdata axial lat] = datacontainer.scan_convert(sc.scanmode,imag(self.data),deg2rad(sc.min_th),deg2rad(sc.span_th),sc.apex*1e-3,2,sc.fsiq,NaN,[(sc.axialmin-sc.apex) (sc.axialmax-sc.apex) sc.axialinc sc.latmin sc.latmax sc.latinc]*1e-3);
                    self.data = complex(idata,qdata);
                else
                    [self.data axial lat] = scan_convert(sc.scanmode,self.data,deg2rad(sc.min_th),deg2rad(sc.span_th),sc.apex*1e-3,2,sc.fsiq,NaN,[(sc.axialmin-sc.apex) (sc.axialmax-sc.apex) sc.axialinc sc.latmin sc.latmax sc.latinc]*1e-3);
                end
                
                self.dims = rmfield(self.dims,{'r',thname,'apex'});
                self.dims.names{1} = 'z';
                self.dims.names{2} = 'x';
                self.dims.units{1} = 'mm';
                self.dims.units{2} = 'mm';
                self.dims.z = axial*1e3;
                self.dims.x = lat*1e3;
            end
        end