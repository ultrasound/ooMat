 function self = std(self,dim,n)
            if ~exist('n','var')
                n = 0;
            end
            if iscell(dim)
                for i = 1:length(dim)
                   [dimidx(i) dimname{i}]= self.getdim(dim{i});
                   self.dims.(dimname{i}) = mean(self.dims.(dimname{i}));
                end
                order = 1:ndims(self);
                order(dimidx) = inf;
                [~,pOrder] = sort(order);
                [~,rOrder] = sort(pOrder);
                data = permute(self.data,pOrder);
                sz = ones(1,ndims(self));
                sz(1:ndims(data)) = size(data);
                data = nanstd(reshape(data,[sz(1:end-length(dimidx)) prod(sz(end-length(dimidx)+1:end))]),0,ndims(self)-length(dimidx)+1);
                self.data = permute(data,rOrder);
                self.log = char(self.log,sprintf('Standard Deviation taken in the %s%s dimension',dimname{1},sprintf(',%s',dimname{2:end})));
            else
            [dim,dimname] = self.getdim(dim);
            self.data = nanstd(self.data,n,dim);
            if ~iscell(self.dims.(dimname))
                self.dims.(dimname) = mean(self.dims.(dimname));
            else
                self.dims.(dimname) = {nan};
            end
            self.log = char(self.log,sprintf('Standard Deviation taken in the %s dimension',dimname));
            end
        end