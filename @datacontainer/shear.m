function self = shear(self,xdim,ydim,dydx,szflag)
%SHEAR shear the data along the specified dimensions
%self = SHEAR(self,xdim,ydim,dydx)
%
%
%SHEAR returns a datacontainer object with the same xdim, ydim, but sheared
%such that G(x,y) = F(x,(y-y(1))/dydx). If dydx is a vector, a new dimesion
%d[yDim]d[xDim] will be created.
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%      xdim: x dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names. 
%      ydim: y dimension. 
%      dydx: shearing amount in units of y/x. shear is relative to the
%            first value of y, so the row/column with y = y(1) will be
%            unchanged.
%    szflag: otuput size flag. 'full' or 'same' - defaults to 'same'
%
%OUTPUTS:
%   self: transformed datacontainer
%
[xdim xdimName xdimUnit] = self.getdim(xdim);
[ydim ydimName ydimUnit] = self.getdim(ydim);
log = self.log;
y = self.dims.(ydimName);
if ~exist('szflag','var')
    szflag = 'same';
end
switch lower(szflag)
    case 'same'
        x = self.dims.(xdimName);
    case 'full'
        x0 = self.dims.(xdimName);
        dx = diff(x0(1:2));
        dxi0 = abs(round((y(end)-y(1))/min(abs(dydx))/dx));
        x = [[-dxi0:-1]'*dx + x0(1);x0(:);x0(end)+[1:dxi0]'*dx];
end

shearfun = @(x,y,dydx)deal(x+(y-y(1))/dydx,y);
shearname = sprintf('d%sd%s',ydimName,xdimName);
shearunit = sprintf('%s/%s',ydimUnit,xdimUnit);
for i = 1:length(dydx);
    selfi = plaid2plaid(self,xdim,ydim,shearfun,x,y,dydx(i));
    selfi = setdim(selfi,'y1',[],xdimName,xdimUnit);
    selfi = setdim(selfi,'y2',[],ydimName,ydimUnit);
    if length(dydx)>1
        selfi = setdim(selfi,ndims(selfi)+1,dydx(i),shearname,shearunit);
    end
    if i == 1
        self1 = selfi;
    else
        self1 = concatenate(self1,shearname,selfi);
    end
end
if length(dydx) == 1;
    self1.log = char(log,sprintf('Sheared with %s = %g %s',shearname,dydx,shearunit));
else
    self1.log = char(log,sprintf('Sheared with %s = [%g , %g] %s',shearname,dydx(1),dydx(end),shearunit));
end
self = self1;