function self = ifft(self,dim,N)
%FFT Discrete Inverse Fourier Transform
%self = fft(self,dim,N)
%
%
%FFT returns a datacontainer object of the N-point discrete Fourier
%transform of the data in the dim dimension
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%        dim: Active dimension
%          N: Optional number of output points
%OUTPUTS:
%   self: transformed datacontainer
%
if ~exist('N','var')
    N = [];
end
if iscell(dim) || (~ischar(dim) && length(dim)>1)
    for i = 1:length(dim)
        if iscell(dim)
            dimi = dim{i};
        else
            dimi = dim(i);
        end
        if length(N) == 1
            Ni = N;
        elseif iscell(N)
            Ni = N{i};
        elseif isempty(N)
            Ni = [];
        else
            Ni = N(i);
        end
        self = ifft(self,dimi,Ni);
    end
    return
end
[dim, dimname, dimunit] = getdim(self,dim);

self.data = ifft(ifftshift(self.data,dim),N,dim,'nonsymmetric');
fs = 1/mean(diff(self.dims.(dimname)));
slashidx = strfind(dimunit,'/');
if isempty(slashidx)
    newunit = ['1/' dimunit];
else
    if strcmp(dimunit([1:slashidx-1]),'1')
        newunit = dimunit([slashidx+1:end]);
    else
        newunit = dimunit([slashidx+1:end slashidx 1:slashidx-1]);
    end
end
if length(dimname)>2 && strcmp(dimname(1:2),'k_')
    newname = dimname(3:end);
else
    newname = sprintf('k_%s',dimname);
end
log = self.log;
self = setdim(self,dim,fs/2 *linspace(0,2,size(self.data,dim)),newname,newunit);
self.log = char(log,sprintf('%g-point inverse Fourier transform in %s dimension',size(self.data,dim),dimname));

