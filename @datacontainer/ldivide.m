function self = ldivide(self,B)
            if isnumeric(self)
                self = B.rdivide(self);
            elseif isnumeric(B)
                sza = datasize(self);
                for i = 1:length(sza)
                    szb(i) = size(B,i); %#ok<AGROW>
                end
                if any(szb>1 & sza~=szb)
                    error('Size Mismatch')
                end
                if ~all(szb==1)
                B = repmat(B,sza./szb);
                self.data = self.data.\B;
                self.log = char(self.log,sprintf('divided outside data by data values'));
                else
                self.data = B./self.data;
                self.log = char(self.log,sprintf('%g divided by data values',B));
                end
                if ~isempty(self.units)
                    slashidx = strfind(self.units,'/');
                    if isempty(slashidx)
                        self.units = ['1/' self.units];
                    else
                        self.units = self.units([slashidx+1:end slashidx 1:slashidx-1]);
                    end
                end
            elseif isa(B,'datacontainer')
                [self,B] = broadcast(self,B);
                %sza = datasize(self);
                %szb = datasize(B);
                %if ndims(self)~=ndims(B) || any(szb>1 & sza~=szb)
                %    error('Size Mismatch')
                %end
                %B.data = repmat(B.data,sza./szb);
                if strcmp(self.units,B.units)
                    self.units = '';
                else
                    self.units = [B.units '/' self.units];
                end
                self.data = self.data.\B.data;
                self.log = char(B.log,sprintf('Divided by %s',class(B)));
            else
                error('Type mismatch')
            end
        end