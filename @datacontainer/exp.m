 function self = exp(self)
            self.data = exp(self.data);
            if ~isempty(self.units)
            self.units = '';
            end
            self.log = char(self.log,'e to the data');
 end
