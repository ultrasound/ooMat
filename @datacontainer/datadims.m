function dims = datadims(values,names,units,varargin)
        %DATADIMS create dimensions structure (Static Method)
        %dims = DATADIMS(values,names,units,varargin)
        %
        %DATADIMS returns a struct with names, values, and units, as well
        %as any additional geometry parameters (typically apex). DATADIMS
        %could replace its struct with a specific dimensions object in
        %future releases.
        %
        %INPUTS:
        %   values - cell array of 1 x size(data,i) dimension values
        %   names - cell array of dimension names (strings)
        %   units - cell array of dimension units (strings). Use '' for
        %       unitless dimensions. 
        %   additional parameters can be passed in as parameter-value
        %   pairs. i.e. ...,'apex',3, etc.
        %
        %OUTPUTS:
        %   dims - output dimension structure. Used in datacontainer
        %   constructors.
            if nargin == 0
                dims.names = {};
                dims.units = {};
            else
                if ~exist('names','var')
                    names = cell(1,length(values));
                    for i = 1:length(values)
                        names{i} = char('i'+i-1);
                    end
                end
                if ~exist('units','var')
                    units = cell(1,length(values));
                    for i = 1:length(values)
                        units{i} = '';
                    end
                end
                dims.names = names;
                dims.units = units;
                for i = 1:length(names)
                    dims.(names{i}) = values{i};
                end
                if nargin>3
                    dims = addfields(dims,varargin);
                end
            end
        end