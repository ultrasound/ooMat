function varargout = imagecolor(self,map,varargin);
[im, cb] = imagesc(self,varargin{:});
cData = im.CData;
cLim = im.Parent.CLim;
N = size(map,1);
ind = round((cData-cLim(1))/diff(cLim) * (N-1)) + 1;
rgb = ind2rgb(ind,map);
set(im,'CData',rgb,'CDataMapping','direct');
if ~isempty(cb)
    axcb = subplot('Position',cb.Position);
    cb1 = image(0,linspace(cLim(1),cLim(2),N),permute(map,[1 3 2]));
    set(axcb,'XTick',[],'YDir','Normal','YAxisLocation','right');
    ylabel(axcb,cb.YLabel.String);
    set(cb,'Visible','off');
    cb = cb1;
end
subplot(im.Parent);
if nargout>0
    varargout{1} = im;
end
if nargout>1
    varargout{2} = cb;
end