function [h] = errorbar(self,dim,varargin);
            varg = varargin;
            narg = length(varg);
            specopts = struct('YLim','auto','XDim',[],'linespec','','Center','mean','Bars','std');
            if(mod(narg,2)==1)
                args = {'linespec', varg{1:end}};
            else
                args = varg;
            end
            [specopts,options] = setopts(specopts,args);
            if ~isempty(specopts.XDim)
                self = permute(self,specopts.XDim);
            end
            sz = size(self.data);
            nonsingleton = find(sz>1);            
            if ~exist('dim','var')
                dim = nonsingleton(end);
            end
            [dim, dimName, dimUnit] = self.getdim(dim);

            if length(nonsingleton)>3 || ~any(dim==nonsingleton)
                error('datacontainer must nat more than three non-singleton dimensions')
            end

            zdim = nonsingleton(1);
            if zdim == dim
                zdim = nonsingleton(2);
            end
            zName = self.dims.names{zdim};
            zUnit = self.dims.units{zdim};
            self.data(isinf(self.data)) = nan;
            switch lower(specopts.Center)
                case 'mean'
                    mu = squeeze(nanmean(self.data,dim));
                case 'median'
                    mu = squeeze(nanmedian(self.data,dim));
            end
            switch lower(specopts.Bars)
                case 'std'
                    sig = squeeze(nanstd(self.data,0,dim));
                case 'stde'
                    sig = squeeze(nanstd(self.data,0,dim)./sqrt(sum(~isnan(self.data),dim)));
            end
            z = self.dims.(zName);
            if iscell(z)
                z = 1:numel(z);
            end
            if size(mu,1) == 1
            mu = mu';
            z = z';
            sig = sig';
            end
            jigglefactor = 0.05;
            jiggle = linspace(-1,1,size(mu,2))*mean(diff(z))*jigglefactor;
            jiggle = jiggle-mean(jiggle);
                h0 = errorbar(repmat(z(:),[1 size(mu,2)])+repmat(jiggle,length(z),1),mu,sig);
            
            if nargin>2
                for i = 1:length(h0)
                    setprops(h0(i),options{:});
                end
            end
            if length(nonsingleton)>2;
            i = 1;
            xdim = nonsingleton(i);
            while xdim == zdim || xdim == dim
                i = i+1;
                xdim = nonsingleton(i);
            end
            xName = self.dims.names{xdim};
            xUnit = self.dims.units{xdim};
            for i = 1:length(h0)
                set(h0(i),'DisplayName',genlabel(self,xdim,i));
            end
            lh = legend(h0);
            for i = 1:length(h0)
                h0(i).UserData.Legend = lh;
            end
            end
            xlabel(genlabel(self,zdim))
            ylabel(self.units);
            if nargout == 1
                h = h0;
            end
        end