%IMAGETILE show image frames side-by-side
%h = imagetile(self,dim,tilingspec,varargin);
function [h] = imagetile(self,dim,tilingspec,varargin);
            if nargin<2
                error('not enough input arguments')
            end
            if ~exist('tilingspec','var')
                tilingspec = 'h';
            end
            [dim, dimName, dimUnit] = self.getdim(dim);
            sz = size(self.data);
            nonsingleton = find(sz>1);
            if length(nonsingleton)~=3 || ~any(dim==nonsingleton)
                error('datacontainer must not have exactly three non-singleton dimensions')
            end
            t = self.dimval(dim);
            ntiles = length(self.dims.(dimName));
            axh = gca;
            if isfield(axh.UserData,'tileAxes')
                pos0 = axh.UserData.original_position;
                axh = mergeAxes(axh.UserData.tileAxes);
                axh.Position = pos0;
            end
            pos0 = get(axh,'position');
            delete(axh);

            switch tilingspec
                case {'h','horiz','horizontal'}
                    for i = 1:ntiles
                        sp(i) = subplot('position',[pos0(1)+(i-1)*pos0(3)/ntiles+0.04*pos0(3)/ntiles pos0(2) pos0(3)/ntiles*0.92 pos0(4)]);
                        h0(i) = self.slice(dim,i).imagesc('colorbar','off',varargin{:});
                        title(self.genlabel(dimName,i))
                        if i > 1
                            ylabel('')
                            set(sp(i),'YTickLabel',{});
                        end
                    end
                   	 if ~isempty(self.units)
                        pos = get(gca,'position');
                        cb = colorbar;
                        set(gca,'position',pos);
                        ylabel(cb,self.units);
                    end
                case {'v','vert','vertical'}
                    for i = 1:ntiles
                        sp(i) = subplot('position',[pos0(1) pos0(2)+(ntiles-i)*pos0(4)/ntiles+0.04*pos0(4)/ntiles pos0(3) pos0(4)/ntiles*0.92]);
                        h0(i) = self.slice(dim,i).imagesc('colorbar','off',varargin{:});
                        title(self.genlabel(dimName,i))
                        if i < ntiles
                            xlabel('');
                            set(sp(i),'XTickLabel',{});
                        end
                    end
                    if ~isempty(self.units)
                        pos = get(sp(1),'position');
                        cb = colorbar('peer',sp(1),'northoutside');
                        set(sp(1),'position',pos);
                        title(cb,self.units);
                    end
                otherwise
                    error('unrecognized tiling specification ''%s''.',tilingspec)
            end
            lp = linkprop(sp(:),{'XLim','YLim','CLim','XDir','YDir'});
            for i = 1:ntiles
                sp(i).UserData.tileAxes = sp;
                sp(i).UserData.original_position = pos0;
                sp(i).UserData.link = lp;
            end
            if nargout>0
                h = h0;
            end
        end