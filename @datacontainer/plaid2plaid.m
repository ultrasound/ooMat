function self = plaid2plaid(self,dimx1,dimx2,y2xHandle,y1,y2,varargin)
%PLAID2PLAID resample coordinate system with anonymous function
%self = plaid2plaid(self,dimx1,dimx2,y2xHandle,y1,y2,varargin)
%
%
%PLAID2PLAID returns a datacontainer object with dimensions dimx1,dimx2
%resampled in a new coordinate system, at y1,y2. The term "plaid" refers to
%the fact that the dimensions of both the input and output matrices must be
%specified by vectors.
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%      dimx1: 1st input dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names. 
%      dimx2: 2nd input dimension
%  y2xHandle: Anonymous function handle that has 2 or more inputs and 2
%           outputs. It takes values of y1,y2, and returns x1,x2.
%         y1: vector specifying output dimension 1
%         y2: vector specifying output dimension 2
%   varargin: optional additional arguments passed to y2xHandle
%
%OUTPUTS:
%   self: transformed datacontainer
%
[dimx1, dimname1, dimunit1] = getdim(self,dimx1);
[dimx2, dimname2, dimunit2] = getdim(self,dimx2);
x1 = self.dims.(dimname1);
x2 = self.dims.(dimname2);
dimorder = 1:ndims(self);
dimorder([dimx1 dimx2]) = nan;
dimorder = sort(dimorder);
dimorder = [dimx1 dimx2 dimorder(1:end-2)];
[~, reorder] = sort(dimorder);
datap = permute(self.data,dimorder);
[Y1OUT,Y2OUT] = ndgrid(y1,y2);
[X1OUT,X2OUT] = y2xHandle(Y1OUT,Y2OUT,varargin{:});
msk = X2OUT(:)>=x2(1) & X2OUT(:)<=x2(end) & X1OUT(:)>=x1(1) & X1OUT(:)<=x1(end);
II = interp1(x1,1:length(x1),X1OUT(msk));
JJ = interp1(x2,1:length(x2),X2OUT(msk));
IDXff = sub2ind([length(x1) length(x2)],floor(II),floor(JJ));
IDXfc = sub2ind([length(x1) length(x2)],floor(II),ceil(JJ));
IDXcf = sub2ind([length(x1) length(x2)],ceil(II),floor(JJ));
IDXcc = sub2ind([length(x1) length(x2)],ceil(II),ceil(JJ));
COEFff = (1-mod(II,1)).*(1-mod(JJ,1));
COEFfc = (1-mod(II,1)).*(mod(JJ,1));
COEFcf = (mod(II,1)).*(1-mod(JJ,1));
COEFcc = (mod(II,1)).*(mod(JJ,1));
szp = size(datap);
datap = datap(:,:,:);
IMPOL = nan([size(X2OUT) size(datap,3)]);
for k = 1:size(datap,3);
DATAi = datap(:,:,k);
IMPOLi = nan(size(X2OUT));
IMPOLi(msk) =   DATAi(IDXff).* COEFff + ...
                DATAi(IDXfc).* COEFfc + ...
                DATAi(IDXcf).* COEFcf + ...
                DATAi(IDXcc).* COEFcc;
IMPOL(:,:,k) = IMPOLi; 
end
self.data = permute(reshape(IMPOL,[size(IMPOL,1),size(IMPOL,2),szp(3:end)]),reorder);
self = setdim(self,dimx1,y1,'y1','');
self = setdim(self,dimx2,y2,'y2','');
self.log = char(self.log,sprintf('Converted (%s,%s) to (y1,y2);',dimname1,dimname2));

