 function self = conj(self)
            self.data = conj(self.data);
            self.log = char(self.log,'complex conjugated');
        end