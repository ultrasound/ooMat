 function self = uminus(self)
            self.data = -1*self.data;
            self.log = char(self.log,'Negated');
        end
        