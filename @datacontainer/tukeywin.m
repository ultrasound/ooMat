 function self = tukeywin(self,dim,R)
            if ~exist('R','var')
                R = 0.5;
            end
            [dim,dimname] = self.getdim(dim);
             pDim = [1:ndims(self)];
             pDim(dim) = 0;
             [~, pOrder] = sort(pDim);
             [~, rOrder] = sort(pOrder);
             pdata = permute(self.data,pOrder);
             sz = size(pdata);
             pdata(:,:) = pdata(:,:).*repmat(tukeywin(sz(1),R),[1 prod(sz(2:end))]);
             self.data = permute(pdata,rOrder);
             self.log = char(self.log,sprintf('Tukey windowed (R = %g) in the %s dimension',R,dimname));
        end