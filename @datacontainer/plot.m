function h = plot(self,varargin)
%PLOT plot data
            %H = plot(self,...)
            %
            %PLOT creates a line plot of the data in the datacontainer,
            %using the first nonsingleton dimension as the independent
            %variable.
            %
            %INPUTS:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %   additional parameters can be specified with property-value
            %   pairs, which will be applied to the plot
            %
            %OUTPUTS:
            %   H: handle (array) to the line plots.
            %
            varg = varargin;
            narg = length(varg);
            specopts = struct('YLim','','XDim',[],'ZDim',[],'linespec','','plotstyle','plot');
            if(mod(narg,2)==1)
                args = {'linespec', varg{1:end}};
            else
                args = varg;
            end
            [specopts,options] = setopts(specopts,args);
            if ~isempty(specopts.XDim)
                self = permute(self,specopts.XDim);
            end
            sz = datasize(self);
            nonsingleton = find(sz>1);
%             if self.datasize(1) == 1
%                 nonsingleton = [1 nonsingleton];
%             end
            if length(nonsingleton)>3
                error('datacontainer must not have more than two non-singleton dimensions')
            end
            if ~isempty(specopts.ZDim)
               [zdim, zName, zUnit] = getdim(self,specopts.ZDim);
            else
                [zdim, zName, zUnit] = getdim(self,nonsingleton(1));
            end
            
            y = squeeze(self.data);
%             if self.datasize(1) == 1
%                 y = permute(y,[ndims(y)+1 1:ndims(y)]);
%             end
            lineStyleOrder = get(gca,'LineStyleOrder');
            if length(lineStyleOrder) == 1
            lineStyleOrder = {'-','--','-.',':'};
            end
            %set(gca,'lineStyleOrder',lineStyleOrder);
            if all(isnan(y(:)));
                warning('no Valid Data found!')
                h = [];
                return
            end
            switch lower(specopts.plotstyle)
                case 'plot'
                    pfun = @plot;
                case 'stem'
                    pfun = @stem;
            end
            if isempty(specopts.linespec)
                h0 = pfun(self.dims.(zName),y(:,:));
            else
                h0 = pfun(self.dims.(zName),y(:,:),specopts.linespec);
            end
            if ischar(specopts.YLim) && strcmp(specopts.YLim,'auto')
                specopts.YLim = autoclim(self.data(:));
            end
            switch get(gca,'NextPlot')
                case 'add'
                case 'replace'
                    if ~isempty(specopts.YLim);
                        set(gca,'YLim',specopts.YLim);
                    end
            end
            for i = 1:numel(h0)
                warning off all
                setprops(h0(i),options);
                warning on all
            end
            if ~isempty(specopts.linespec)
                manualColor = ~isempty(regexp(specopts.linespec,'[rgbcmykw]'));
                manualLine = any(strcmp(specopts.linespec(regexp(specopts.linespec,'[-:.]')),{'-','--',':','-.'}));
            else
                manualLine = 0;
                manualColor = 0;
            end
            markerOrder = {'o','s','d','^','v','>','<','p','h','x','+','*','.','none'};        
            switch length(nonsingleton)
                case 2
                    if ~manualColor && size(h0,1)>size(get(gca,'ColorOrder'),1);
                        cmap = colormap;
                    end
                    [xdim, xName, xUnit] = getdim(self,nonsingleton(2));
                    for i = 1:length(h0)
                        set(h0(i),'DisplayName',genlabel(self,xdim,i));
                        if manualColor
                            if manualLine
                                set(h0(i),'Marker',markerOrder{mod(i-1,length(markerOrder))+1});
                            else
                                set(h0(i),'LineStyle',lineStyleOrder{mod(i-1,length(lineStyleOrder))+1});
                            end
                        else
                            if size(h0,1)>size(get(gca,'ColorOrder'),1);
                                set(h0(i),'color',cmap(round((size(cmap,1)-1)*(i-1)/(length(h0)-1))+1,:));
                            end
                        end
                            
                    end
                    if iscell(self.dims.(xName)) || manualColor || size(h0,1)<=size(get(gca,'ColorOrder'),1);
                        lh = legend(h0);
                    else
                        cb = colorbar;
                        x = self.dims.(xName);          
                        xlim = interp1(0:length(x)-1,x,[-0.5 length(x)-0.5],'linear','extrap');
                        set(gca,'clim',xlim);
                        ylabel(cb,genlabel(self,xdim));
                        if length(get(cb,'YTick'))>size(h0,1)
                            set(cb,'YTick',x);
                        end
                    end
                case 3
                    colorOrder = get(gca,'ColorOrder');
                    for i = 1:length(nonsingleton)-1
                            [xdim{i} xName{i}, xUnit{i}] = getdim(self,nonsingleton(i+1));
                    end
                    nx1 = datasize(self,xdim{1});
                    nx2 = datasize(self,xdim{2});
                    h0 = reshape(h0,[nx1 nx2]);
                    if size(h0,1)>size(colorOrder,1)
                        cmap0 = colormap;
                        colorOrder = interp1(linspace(0,1,size(cmap0,1)),cmap0,linspace(0,1,size(h0,1)),'linear');
                    end
            for i = 1:size(h0,1)
                for j = 1:size(h0,2)
                    set(h0(i,j),'DisplayName',sprintf('%s, %s',genlabel(self,xdim{1},i),genlabel(self,xdim{2},j)));
                    if numel(h0)>size(colorOrder,1)
                        if manualColor
                            set(h0(i,j),'LineStyle',lineStyleOrder{mod(i-1,length(lineStyleOrder))+1},'Marker',markerOrder{mod(j-1,length(markerOrder))+1});
                        else
                            set(h0(i,j),'Color',colorOrder(mod(i-1,size(colorOrder,1))+1,:),'LineStyle',lineStyleOrder{mod(j-1,length(lineStyleOrder))+1});
                            if size(h0,1)>size(get(gca,'ColorOrder'),1);
                            set(h0(i,j),'DisplayName',sprintf('%s',genlabel(self,xdim{2},j)));
                            end
                        end
                    end
                end
            end
            if (~iscell(xName) && iscell(self.dims.(xName))) || numel(h0)<=size(get(gca,'ColorOrder'),1)
                lh = legend(h0(:));
            else
                if manualColor || size(h0,1)<=size(get(gca,'ColorOrder'),1);
                    legText = {};
                    for i = 2:size(h0,1)
                        set(h0(i,1),'DisplayName',sprintf('%s',genlabel(self,xdim{1},i)));
                    end
                    for j = 2:size(h0,2)
                        set(h0(1,j),'DisplayName',sprintf('%s',genlabel(self,xdim{2},j)));
                    end
                    lh = legend([h0(:,1)' h0(1,2:end)],legText);
                else
                    lh = legend(h0(1,:));
                    colormap(gca,colorOrder(1:size(h0,1),:));
                    cb = colorbar;
                    x = self.dims.(xName{1});
                    xlim = interp1(0:length(x)-1,x,[-0.5 length(x)-0.5],'linear','extrap');
                    set(gca,'clim',xlim);
                    ylabel(cb,genlabel(self,xdim{1}));
                    if length(get(cb,'YTick'))>size(h0,1)
                        set(cb,'YTick',x);
                    end
                end
            end

            end
            if exist('lh','var')
                for i = 1:numel(h0)
                    h0(i).UserData.Legend = lh;
                end
            end
            xlabel(genlabel(self,zdim))
            ylabel(self.units);
            if nargout == 1
                h = h0;
            end
        end