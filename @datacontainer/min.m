 function [self, locobj] = min(self,dim)
        if ~exist('dim','var')
            [pk index] = min(self.data(:));
            I = cell(1,ndims(self));
            [I{:}] = ind2sub(datasize(self),index);
            for i = 1:length(I);
                self = slice(self,i,I{i});
            end
            return
        end
        if iscell(dim)
            if nargout>1
                error('unsupported multiple min location')
            end
            for i = 1:length(dim);
                self = min(self,dim{i});
            end
        else
            [dim,dimname,dimunits] = self.getdim(dim);
            [self.data, idx]= min(self.data,[],dim);
            if nargout>1
                locobj = self;
                locobj.data(:) = locobj.dims.(dimname)(idx);
                locobj.data(isnan(self.data)) = nan;
                locobj.units = dimunits;
                locobj.dims.(dimname) = nan;
                locobj.log = char(self.log,sprintf('location of min value in %s dimension',dimname));
            end
            self.dims.(dimname) = nan;
            self.log = char(self.log,sprintf('min value taken in the %s dimension',dimname));
        end