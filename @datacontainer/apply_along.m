function self = apply_along(self, dim, func, varargin)
    [dim, dimName, dimUnit] = self.getdim(dim);
    dims = self.dims.names;
    self1 = self.permute(dim);
    self1.data = func(self1.data(:,:),varargin{:});
    self = self1.permute(dims);
    self.log = char(self.log,...
        sprintf('applied anonymous function in %s dimension',dimName));
end