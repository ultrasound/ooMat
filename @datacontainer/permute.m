function self = permute(self,varargin)
            %PERMUTE Permute array dimensions
            %B = permute(A,ORDER) rearranges the dimensions of the data in
            %datacontainer A so that they are in the order specified by the
            %vector ORDER. The dimensions parameter of the datacontainer is
            %adjusted accordingly. Additionally, any parameters of the
            %datacontainer that have a matching (non-singleton) dimension
            %to A's data will be permuted. The array produced has the same
            %values as A but the order of the subscripts needed to access
            %any particular element are rearranged as specified by ORDER.
            %All the elements ORDER must be unique. For datacontainers,
            %order may also be specified as a cell array of dimension
            %names. If numel(ORDER)<ndims(self), missing dimensions are
            %appended to ORDER in ascending order.
            %
            %
            if length(varargin) == 1
                order = varargin{1};
            else
                order = varargin;
            end
            oldOrderStr = sprintf('%s, ',self.dims.names{:});
            if ischar(order)
                order = {order};
            end
            if iscell(order)
                for i = 1:length(order)
                    idx = find(strcmp(order{i},self.dims.names));
                    if isempty(idx)
                        error('No dimension ''%s'' found.',order{i});
                    end
                    tmporder(i) = idx;
                end
                order = tmporder;
            end
            if length(order) < ndims(self)
                defaultorder = 1:ndims(self);
                neworder = defaultorder;
                neworder(order) = [1:length(order)]-length(order);
                [sortorder sortidx] = sort(neworder);
                order = defaultorder(sortidx);
            elseif length(order) > ndims(self);
                for i = (ndims(self)+1):length(order)
                    self = setdim(self,i,0);
                end
            end
            self.dims.names = self.dims.names(order);
            self.dims.units = self.dims.units(order);
            paramnames = fieldnames(self.params);
            permuteflag = zeros(1,length(paramnames));
            for i = 1:length(paramnames)
                for j = 1:length(order)
                    if size(self.params.(paramnames{i}),j)>1 && size(self.params.(paramnames{i}),j) == size(self.data,j)
                        permuteflag(i) = 1;
                    end
                end
                if permuteflag(i)
                    self.params.(paramnames{i}) = permute(self.params.(paramnames{i}),order);
                end
            end
            self.data = permute(self.data,order);
            newOrderStr = sprintf('%s, ',self.dims.names{:});
            self.log = char(self.log,sprintf('Permuted from [%s] to [%s].',oldOrderStr(1:end-2),newOrderStr(1:end-2)));
        end