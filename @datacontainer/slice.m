function self = slice(self,dim,index,varargin)
            %SLICE Slice data along specified dimension
            %self = slice(self,dim,index)
            %
            %SLICE returns a datacontainer object indexed in the dim
            %dimension, also slicing the dimensions and any parameters with
            %equivalent size in the dim dimension.
            %
            %INPUTS:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %       dim: active dimension. May be the index of the dimension,
            %           or the string matching the dimension name in
            %           self.dims.names
            %     index: vector containing the indices to return in the
            %           specified dimension
            %
            %OUTPUTS:
            %   self: sliced datacontainer
            %
            %SLICE searches the parameters of self for parameters whose
            %size(self.params.(param),dim) == size(self.data,dim),
            %and slices those parameters as well. 
           
            if ~exist('dim','var')
                dim = 1;
            end
            [dim dimname dimunit] = self.getdim(dim);
            ndim = ndims(self);
            if any(index>datasize(self,dim));
                error('index out of range');
            end
            index = mod(index-1,datasize(self,dim))+1;
            s.type = '()';
            s.subs = cell(1,ndim);
            s.subs(1:ndim) = {':'};
            s.subs{dim} = index;
            len = size(self.data,dim);
            self.data = subsref(self.data,s);

            self.dims.(dimname) = self.dims.(dimname)(index);
            paramNames = fieldnames(self.params);
            for i = 1:length(paramNames)
                if size(self.params.(paramNames{i}),dim) == len && (size(self.params.(paramNames{i}),dim))>1
                    self.params.(paramNames{i}) = subsref(self.params.(paramNames{i}),s);
                end
            end
            if isempty(index)
                error('Could not slice %s',dimname);
            elseif numel(index)==1
                switch class(self.dims.(dimname))
                    case 'char'
                        val = self.dims.(dimname);
                    case 'cell'
                        val = self.dims.(dimname){1};
                    otherwise
                        val = sprintf('%g %s',self.dims.(dimname),dimunit);
                end
                logstr = sprintf('Sliced to %s sample in %s dimension (%s = %s)',nth(index),dimname,dimname,val);
            else
                switch class(self.dims.(dimname))
                    case 'cell'
                        val1 = self.dims.(dimname){1};
                        val2 = self.dims.(dimname){end};
                    otherwise
                        val1 = sprintf('%g %s',min(self.dims.(dimname)),dimunit);
                        val2 = sprintf('%g %s',max(self.dims.(dimname)),dimunit);
                end
                logstr = sprintf('Sliced to %g samples in the %s dimension (%s <= %s <= %s)',length(index),dimname,val1,dimname,val2);
            end
            self.log = char(self.log,logstr);
            if nargin>3
                if length(varargin)>2
                    self = slice(self,varargin{1},varargin{2},varargin{3:end});
                elseif length(varargin)==2
                    self = slice(self,varargin{1},varargin{2});
                else
                    error('not enough input arguments');
                end
            end
end
        
function sfx = nth(n)
    switch n
        case 1
            sfx = '1st';
        case 2
            sfx = '2nd';
        case 3
            sfx = '3rd';
        otherwise
            sfx = sprintf('%gth',n);
    end
end