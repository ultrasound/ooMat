 function self = angle(self)
            self.data = angle(self.data);
            self.units = 'rad';
            self.log = char(self.log,'Angle of data');
        end