function [shiftobj,ccobj] = normxcorr(self1,dim,self2,ksz_samp,srch_samp,decimationfactor);
%NORMXCORR normalized cross correlation peak search
%[shiftobj,ccobj] = normxcorr(self1,dim,self2,ksz_samp,srch_samp,decimationfactor);
%
%
%NORMXCORR returns datacontainer objects of the lag and peak normalized
%cross-correlation coefficient between two datacontainers in the specified
%dimension.
%
%INPUTS:
%    self1: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%      dim: search dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names. 
%   self2: target datacontainer. One dimension of self1 or self2
%             may not match the other, but then that dimension must be
%             singleton in either self1 or self2.
%
%OUTPUTS:
%   self: transformed datacontainer
%
[dim, dimName, dimUnit] = getdim(self1,dim);
if nargin<4
    error('not enough input arguments')
end
if ~exist('decimationfactor','var')
    decimationfactor = 1;
end
if ndims(self1)~=ndims(self2)
    error('datacontainers must have the same number of dimensions')
end
 pDim = [1:ndims(self1)];
 pDim(dim) = 0;
 sz1 = datasize(self1);
 sz2 = datasize(self2);
 singleton1 = (sz1==1);
 singleton2 = (sz2==1);
 if ~all(sz1==sz2)
     diffdim = find(sz1~=sz2);
     if length(diffdim)>1
         for i = 1:length(diffdim)
             [~,diffName{i}] = getdim(self1,diffdim(i));
         end
         error('Cannot have more than one mismatched dimension ( %s)',sprintf('%s ',diffName{:}))
     end
     if sz1(diffdim)==1 || sz2(diffdim)==1
         pdim(diffdim) = inf;
     else
         [~,diffName] = getdim(self1,diffdim);
         error('One of the mismatched dimensions (%s) must be singleton',diffName);
     end
 end      
 [~, pOrder] = sort(pDim);
 [~, rOrder] = sort(pOrder);
 pdata1 = permute(self1.data,pOrder);
 pdata2 = permute(self2.data,pOrder);
if nargout>1
 [shift,cc] = calc_normxcorr(pdata1,pdata2,ksz_samp,srch_samp,decimationfactor);
else
 [shift] = calc_normxcorr(pdata1,pdata2,ksz_samp,srch_samp,decimationfactor);
end
 shiftobj = self1;
 if decimationfactor>1
     shiftobj.dims.(dimName) = shiftobj.dims.(dimName)([0:size(shift,1)-1]*decimationfactor+1);
 end
 shiftobj.data = permute(shift*median(diff(self1.dims.(dimName))),rOrder);
 shiftobj.units = dimUnit;
 for i = 1:ndims(self1);
     if sz2(i)>sz1(i)
         [~,dimnamei] = getdim(self1,i);
         shiftobj.dims.(dimnamei) = self2.dims.(dimnamei);
     end
 end
 shiftobj.log = char(shiftobj.log,sprintf('%g-point Normalized Cross Correlation taken in %s with %g-point search region',ksz_samp,dimName,srch_samp));
 if nargout>1
 ccobj = shiftobj;
 ccobj.data = permute(cc,rOrder);
 ccobj.units = '';
 end
 