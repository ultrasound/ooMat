function h = quiver(self,nz,nx,s)
            sz = size(self.data);
            nonsingleton = find(sz>1);
            if length(nonsingleton)~=2
                error('datacontainer must not have exactly two non-singleton dimensions')
            end
            xdim = nonsingleton(2);
            xName = self.dims.names{xdim};
            xUnit = self.dims.units{xdim};
            zdim = nonsingleton(1);
            zName = self.dims.names{zdim};
            zUnit = self.dims.units{zdim};
            self = rescale(self,sprintf('%s/%s',zUnit,xUnit));
            %    v = varargin;
            %    if mod(length(v),2) == 1 && isnumeric(v{end}) && length(v{end})==2
            %        v = v(1:end-1);
            %        clim = varargin{end};
            %    else
            %        clim = autoclim(self.data(:),[5 95]);
            %    end
            %if any(abs(diff(diff(self.dims.(xName))))>0.01*mean(abs(diff(self.dims.(xName))))) || any(abs(diff(diff(self.dims.(zName))))>0.01*mean(abs(diff(self.dims.(zName)))));
            %error('must be evenly spaced')
            %    h0 = surf(self.dims.(xName),self.dims.(zName),zeros(size(squeeze(self.data))),squeeze(self.data),'edgecolor','none',v{:});
                %if exist('clim','var')
                %end
            %view([0 90]);
            %zlim([-0.1 0.1]);
            %axis ij
            %axis tight
            %else
                if ~exist('nz','var')
                    nz = 20;
                end
                if ~exist('nx','var')
                    nx = nz;
                end
                if ~exist('s','var')
                    s = 0.5;
                end
                x = self.dims.(xName);
                z = self.dims.(zName);
                j = 1:round(length(x)/nx):length(x);
                i = 1:round(length(z)/nz):length(z);
                [X Z] = meshgrid(x(j),z(i));
                dx = diff(x(i([1 2])))*s;
                V = squeeze(self.data)*dx;
                V = V(i,j);
                U = dx*ones(size(V));
                h0 = quiver(X,Z,U,V,0);
            %end
            %set(gca,'clim',clim);
            xlabel(genlabel(self,xdim))
            ylabel(genlabel(self,zdim));
            %if ~isempty(self.units)
            %    cb = colorbar;
            %    ylabel(cb,self.units);
            %end
            if strcmp(zUnit,xUnit)
                axis image
            end
            if nargout == 1
                h = h0;
            end
        end