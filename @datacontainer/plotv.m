function h = plotv(self,varargin)
            sz = size(self.data);
            nonsingleton = find(sz>1);
            if length(nonsingleton)>2
                error('datacontainer must not have exactly two non-singleton dimensions')
            end

            zdim = nonsingleton(1);
            zName = self.dims.names{zdim};
            zUnit = self.dims.units{zdim};
            h0 = plot(squeeze(self.data),self.dims.(zName),varargin{:});
            if length(nonsingleton)==2;
            xdim = nonsingleton(2);
            xName = self.dims.names{xdim};
            xUnit = self.dims.units{xdim};
            for i = 1:length(h0)
                set(h0(i),'DisplayName',genlabel(self,xdim,i));
            end
            legend(h0)
            end
            ylabel(genlabel(self,zdim))
            xlabel(genlabel(self,0));
            if nargout == 1
                h = h0;
            end
        end