function M = animate(self,dims,varargin)
%ANIMATE show animated data
%M = animate(self,dims,...)
%
%ANIMATE displays the data contained in self, and then loops
%over the dimensions specified in dims. The first two
%non-singleton dimensions NOT listed in dims of self.data
%are used to specify the y- and x-axes. dims specifies which
%indices to loop over and in what order.
%
%INPUTS:
%   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%   dims: dimensions to loop over. Can be single dimension
%       (character or index), or a cell array of
%       characters/indices. If a cell array is specified, the
%       dimensions are looped through in order, inner- to
%       outer-.
%   additional parameters can be specified with property-value
%   pairs, which will be applied to the current axis. An
%   additional parameter,'fps',specifies the framerate)
%
%OUTPUTS:
%   M: movies structure captured using getframe, if an output
%   is requested
%
%Examples:
%arfi object with parameters
%   data: [144x8x8x96 single] with units of �m
%   dims:
% 		r: [1x144 double] with units of mm
% 		th: [1x8 double] with units of deg
% 		phi: [1x8 double] with units of deg
% 		t: [1x96 double] with units of ms
%
%animate(arfi,{'t','phi'}) animates with th as x and r as why,
%first over t, and then over phi
%animate(arfi(60,:,:,:),'t','fps',10) animates with phi as x and th as y, over t at 10 frames per second
%animate(arfi(60,:,:,:),'fps',20) assumes the dimensions to be
%looped over is 't';
%animate(arfi(:,:,:,30)) assumes the looping dimension is 'phi'

if nargin<2
    error('Not enough input arguments')
end
narg = length(varargin);
sz = ones(1,ndims(self));
sz1 = size(self.data);
sz(1:length(sz1)) = sz1;
nonsingleton = find(sz>1);
if ~iscell(dims)
    dims = {dims};
end
for i = 1:length(dims)
    [idim{i},idimname{i},idimunit{i}] = self.getdim(dims{i});
end
dimnames = self.dims.names;

varg = varargin;
midx = find(strcmpi('mode',varg));
mode = '';
if ~isempty(midx);
    mode = varg{midx+1};
    varg = varg([1:midx-1 midx+2:end]);
end
if isempty(mode)
switch length(nonsingleton)
    case 2+length(dims)
        mode = 'image';
    case 1+length(dims)
        mode = 'plot';
    otherwise
        error('Dimensions/Indexing loop mismatch')
end
end


switch mode
    case 'image'
        specopts = struct('fps',10,'clim','auto');
        
        if(mod(narg,2)==1)
            args = {varg{1:end-1}, 'clim', varg{end}};
        else
            args = varg;
        end
        [specopts,options] = setopts(specopts,args);
        zdim = 0;
        xdim = 0;
        isindexed = false(1,length(dimnames));
        for i = 1:length(dimnames)
            isindexed(i) = any(strcmp(dimnames{i},idimname));
            if ~isindexed(i) && sz(i)>1
                if zdim == 0
                    zdim = i;
                elseif xdim == 0
                    xdim = i;
                else
                    error('too many non-singleton dimensions')
                end
            end
        end
        if ischar(specopts.clim) && strcmp(specopts.clim,'auto')
            specopts.clim = autoclim(self.data(:));
        end
        dimorder = [zdim,xdim,idim{:},find(~isindexed & [1:length(dimnames)]~=xdim & [1:length(dimnames)]~=zdim)];
        [xdim, xdimname] = self.getdim(xdim);
        [zdim, zdimname] = self.getdim(zdim);
        N = 1;
        for i = 1:length(idimname)
            N = N*length(self.dims.(idimname{i}));
        end
        [len, divideby, modulo] = deal(ones(1,length(dimorder)));
        for i = 1:length(dimorder)
            len(i) = length(self.dims.(dimnames{i}));
        end
        len([xdim zdim]) = 1;
        for i = 3:length(dimorder)
            divideby(i) = len(dimorder(i-1))*divideby(i-1);
            modulo(i) = len(dimorder(i));
        end
    case 'plot'
        specopts = struct('fps',10,'ylim','auto','linespec','');
        if(mod(narg,2)==1)
            args = {'linespec', varg{1:end}};
        else
            args = varg;
        end
        [specopts,options] = setopts(specopts,args);
        xdim = 0;
        isindexed = false(1,length(dimnames));
        for i = 1:length(dimnames)
            isindexed(i) = any(strcmp(dimnames{i},idimname));
            if ~isindexed(i) && sz(i)>1
                if xdim == 0
                    xdim = i;
                else
                    %error('too many non-singleton dimensions')
                end
            end
        end
        if ischar(specopts.ylim) && strcmp(specopts.ylim,'auto')
            specopts.ylim = autoclim(self.data(:));
        end
        dimorder = [xdim,idim{:},find(~isindexed & [1:length(dimnames)]~=xdim)];
        [xdim, xdimname] = self.getdim(xdim);
        N = 1;
        for i = 1:length(idimname)
            N = N*length(self.dims.(idimname{i}));
        end
        [len, divideby, modulo] = deal(ones(1,length(dimorder)));
        for i = 1:length(dimorder)
            len(i) = length(self.dims.(dimnames{i}));
        end
        len([xdim]) = 1;
        for i = 2:length(dimorder)
            divideby(i) = len(dimorder(i-1))*divideby(i-1);
            modulo(i) = len(dimorder(i));
        end
end
dataslice = self;
for i = 1:length(idimname)
    dataslice = slice(dataslice,idimname{i},1);
end

switch mode
    case 'image'
        h = imagesc(dataslice,specopts.clim);
        axh = ancestor(h,'axes');
        figh = ancestor(h,'figure');
        figure(figh);shg;
        set(figh,'CurrentCharacter','a');
        setprops(h,options);
%        fnames = fieldnames(options);
%        for i = 2:length(fieldnames(options));
%            set(axh,fnames{i},options.(fnames{i}));
%        end
        cdata = reshape(permute(self.data,dimorder),length(self.dims.(zdimname)),length(self.dims.(xdimname)),N);
    case 'plot'
        if isempty(specopts.linespec)
            h = plot(dataslice);
        else
            h = plot(dataslice,specopts.linespec);
        end
        axis tight
        ylim(specopts.ylim);
        for i = 1:length(h(:))
        setprops(h(i),options);
        end
        axh = ancestor(h(1),'axes');
        figh = ancestor(axh,'figure');
        figure(figh);shg;
        set(figh,'CurrentCharacter','a');
        pdata = reshape(permute(self.data,dimorder),length(self.dims.(xdimname)),N,[]);
end
tic

% Find the biggest format width and number of decimal points to print a
% nice title.
maxDigitsAbove = 0; maxDigitsBelow = 0;
for i = 1:N
    toc0 = toc;
    ttl = '';
    switch mode
        case 'image'
            for j = length(dimorder):-1:3
                idx = mod(floor((i-1)/divideby(j)),modulo(j))+1;
                ttlNumString = sprintf('%g',self.dims.(dimnames{dimorder(j)})(idx));
                periodIndex = strfind(ttlNumString, '.');
                if isempty(periodIndex)
                    above = length(ttlNumString);
                    if above>maxDigitsAbove, maxDigitsAbove=above; end
                else
                    above = length(ttlNumString(1:periodIndex-1));
                    if above>maxDigitsAbove, maxDigitsAbove=above; end
                    below = length(ttlNumString(periodIndex+1:end));
                    if below>maxDigitsBelow, maxDigitsBelow=below; end
                end
            end
        case 'plot'
            for j = length(dimorder):-1:2
                idx = mod(floor((i-1)/divideby(j)),modulo(j))+1;
                ttlNumString = sprintf('%g',self.dims.(dimnames{dimorder(j)})(idx));
                periodIndex = strfind(ttlNumString, '.');
                if isempty(periodIndex)
                    above = length(ttlNumString);
                    if above>maxDigitsAbove, maxDigitsAbove=above; end
                else
                    above = length(ttlNumString(1:periodIndex-1));
                    if above>maxDigitsAbove, maxDigitsAbove=above; end
                    below = length(ttlNumString(periodIndex+1:end));
                    if below>maxDigitsBelow, maxDigitsBelow=below; end
                end
            end
    end
end
formatSpecifier = ['%s = % ',num2str(maxDigitsAbove+(maxDigitsBelow>0)+maxDigitsBelow),'.',num2str(maxDigitsBelow),'f %s'];

for i = 1:N
    if ~ishandle(h)
        return
    end
    toc0 = toc;
    ttl = '';
    switch mode
        case 'image'
            for j = length(dimorder):-1:3
                idx = mod(floor((i-1)/divideby(j)),modulo(j))+1;
                if iscell(self.dims.(dimnames{dimorder(j)}))
                ttl = char(ttl,sprintf('%s = %s',dimnames{dimorder(j)},self.dims.(dimnames{dimorder(j)}){idx}));
                else
                ttl = char(ttl,sprintf(formatSpecifier,dimnames{dimorder(j)},self.dims.(dimnames{dimorder(j)})(idx),self.dims.units{dimorder(j)}));
                end
            end
            title(ttl);
            set(h,'cdata',cdata(:,:,i),'alphadata',~isnan(cdata(:,:,i)));
        case 'plot'
            for j = length(dimorder):-1:2
                idx = mod(floor((i-1)/divideby(j)),modulo(j))+1;
                ttl = char(ttl,sprintf(formatSpecifier,dimnames{dimorder(j)},self.dims.(dimnames{dimorder(j)})(idx),self.dims.units{dimorder(j)}));
            end
            title(ttl);
            for j = 1:numel(h)
            set(h(j),'ydata',pdata(:,i,j));
            end
    end
    drawnow;
    if nargout>0
        M(i) = getframe(figh);
    end
    while(toc-toc0)<(1/specopts.fps)
        pause(0.01/specopts.fps);
    end
    k = get(figh,'CurrentCharacter');
    if (double(k) == 27)
        fprintf('animation cancelled\n')
        break
    end
end


end