function stats = anova(self,dim,varargin)
opts = struct('alpha',0.05,'display','off');
opts = setopts(opts,varargin{:});
f = fieldnames(opts);
vin = [f'; cell(1,length(f))];
for i = 1:length(f);
    vin{2,i} = opts.(f{i});
end
sz = datasize(self);
if sum(sz>1)~=2
    error('dataconatiner must have 2 nonsingleton dimensions');
end
[dim,dimName,dimUnit] = getdim(self,dim);
g = dimval(self,dim);
if iscell(g);
    groupNames = g;
else
    gc = mat2cell(g(:),ones(numel(g),1),1);
    groupNames = cellfun(@(x)sprintf('%s = %g %s',dimName,x,dimUnit),gc,'uniformoutput',false);
end
data = self.permute(dim).data(:,:)';
[P,table,stats] = anova1(data,groupNames,'off');
[comp,means,h] = multcompare(stats,vin{:});
stats.table = table;
stats.h = h;
stats.means = means;
stats.comp = comp;
ng = numel(g);
if ~isempty(dimUnit)
    dimNameUnit = sprintf('%s (%s)',dimName,dimUnit);
else
    dimNameUnit = dimName;
end
% <<<<<<< HEAD
% c = cell(ng+2,ng+2);
% =======
% c = cell(ng+2,ng+4);
% >>>>>>> 42afed712be8d4ed342cde35d25c728cd861a3b1
% c(:) = {''};
% c{1,1} = '\multicolumn{4}{c|}{}';
% c{1,2} = sprintf('\\multicolumn{%g}{c|}{%s}',ng,dimNameUnit);
% c(1,3:end) = {[]};
% <<<<<<< HEAD
% 
% =======
% c{2,1} = '\multicolumn{2}{c|}{}';
% for i = 1:ng
%     c{2,3+i} = g{i};
% end
% c{3,1} = sprintf('\\multirow{%g}{*}{\\rotatebox[origin=c]{90}{%s}}',ng-1,dimNameUnit);
% c(2,ng+4:end) = {[]};
% for k = 1:size(stats.comp,1);
%     row = stats.comp(k,:);
%     if row(6)>opts.alpha
%         c{2+row(1),4+row(2)} = sprintf('NS (P = %0.3f)',row(6));
%     else
%         c{2+row(1),4+row(2)} = sprintf('%0.2g (%0.2g, %0.2g) (P = %0.3f)',row(4),row(3),row(5),row(6));
%     end
% end
% c{2,2} = '$\mu$';
% c{2,3} = '$\sigma$';
% for i = 1:ng;
%     c{2+i,2} = g{i};
%     c{2+i,3} = sprintf('%0.3g',stats.means(i,1));
%     c{2+i,4} = sprintf('%0.3g',std(data(:,i)));
% end
% >>>>>>> 42afed712be8d4ed342cde35d25c728cd861a3b1
% latex = genLatexTable(c,...
%     'label',sprintf('ANOVA_%s',strrep(self.info,' ','_')),...
%     'caption',sprintf('ANOVA for %s',self.info));
% index = strfind(latex,'\hline');
% latex = [latex(1:index(1)-1) sprintf('\\cline{%g-%g}',5,size(c,2)) latex(index(1)+6:end)];
% index = strfind(latex,'\hline');
% latex = [latex(1:index(1)-1) sprintf('\\cline{%g-%g}',3,size(c,2)) latex(index(1)+6:end)];
% for i = 1:ng-1;
%     index = strfind(latex,'\hline');
%     latex = [latex(1:index(2)-1) sprintf('\\cline{%g-%g}',2,size(c,2)) latex(index(2)+6:end)];
% end
% stats.latex = latex;
end

