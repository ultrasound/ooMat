function H = xsurf(self,dims,cutidx,varargin)
%XSURF slice volume
%H = xsurf(self,dims,cutidx...)
%
%TODO
narg = length(varargin);
defaults = struct('cutby','index',...
    'facealpha',1,...
    'alphaprctile',[0 75],...
    'clim','auto',...
    'alpharange','auto',...
    'alphalim',[0 1],...
    'edges','full');
sz = ones(1,ndims(self));
sz1 = size(self.data);
sz(1:length(sz1)) = sz1;
nonsingleton = find(sz>1);
args = varargin;
options = setfields(defaults,args);
if length(nonsingleton)~=3
    error('Dimensions/Indexing loop mismatch')
end
if length(dims)~=3
    error('Must specify 3 dimensions')
end
if ~iscell(cutidx)
    cutidx = {cutidx(1) cutidx(2) cutidx(3)};
end
dimnames = self.dims.names;
for i = 1:length(dims)
    [dim(i),dimname{i},dimunit{i}] = self.getdim(dims{i});
end
isindexed = false(1,ndims(self));
isindexed(dim) = true;
dimorder = [dim(:)',find(~isindexed)];

%self = permute(self,dimorder);

holdstate = get(gca,'NextPlot');
if ischar(options.alpharange) && strcmp(options.alpharange,'auto')
    if ischar(options.facealpha)
        switch options.facealpha
            case 'abs';
                alpharange = prctile(abs(self.data(:)),options.alphaprctile);
            case 'direct'
                alpharange = prctile((self.data(:)),options.alphaprctile);
        end
        if length(alpharange) == 1
            alpharange = [0 alpharange];
        end
    end
else
    alpharange = options.alpharange;
end
NextPlot = get(gca,'NextPlot');
ii = 0;
for j = 1:3
    for i = 1:length(cutidx{j})
        switch lower(options.cutby)
            case 'index'
            dataslice = permute(slice(self,dim(j),cutidx{j}(i)),dim);
            case 'value'
            dataslice = permute(slicex(self,dim(j),cutidx{j}(i)),dim); 
        end
        if datasize(dataslice,j)>1
            error('can''t use multiple slicex bounds in surfx');
        end
        x = dataslice.dims.(dimname{1});x = x(:);
        switch options.edges
            case 'full'
                frac = 0.5;
            case 'tight'
                frac = 0;
            otherwise
                error('unknown edge option %s',options.edges);
        end
        if j ~= 1
             x = [x(1)-(x(2)-x(1))*frac; (x(1:end-1)+x(2:end))/2; x(end) + (x(end)-x(end-1))*frac];
        end
        y = dataslice.dims.(dimname{2});y = y(:);
        if j ~= 2
             y = [y(1)-(y(2)-y(1))*frac; (y(1:end-1)+y(2:end))/2; y(end) + (y(end)-y(end-1))*frac];
        end
        z = dataslice.dims.(dimname{3});z = z(:);
        if j ~= 3
             z = [z(1)-(z(2)-z(1))*frac; (z(1:end-1)+z(2:end))/2; z(end) + (z(end)-z(end-1))*frac];
        end
        [X,Y,Z] = ndgrid(x,y,z);
        cdata = squeeze(dataslice.data);
        H{j}(i) = surf(squeeze(X),squeeze(Y),squeeze(Z),cdata,'edgecolor','none');
        set(H{j}(i),'faceColor','texture')
        %set([H{j}(i)],'alphadatamapping','none');
        alphalim = options.alphalim;
        if ischar(options.facealpha)
            switch options.facealpha
                case 'abs'
                    set(H{j}(i),'alphadata',~isnan(squeeze(dataslice.data)).*((max(0,min(1,squeeze((abs(dataslice.data))-alpharange(1))/diff(alpharange)))*diff(alphalim))+alphalim(1)),'facealpha','texture','alphadatamapping','none');
                case 'direct'
                    set(H{j}(i),'alphadata',~isnan(squeeze(dataslice.data)).*((max(0,min(1,squeeze(((dataslice.data))-alpharange(1))/diff(alpharange)))*diff(alphalim))+alphalim(1)),'facealpha','texture','alphadatamapping','none');
                otherwise
                    error('unknown facealpha %s',options.facealpha);
            end
            
        else
            
            set(H{j}(i),'facealpha',options.facealpha);
        end
       if ii == 0
            set(gca,'NextPlot','add');
        ii = 1;
        end 
        
    end
    if ii == 0
        set(gca,'NextPlot','add');
        ii = 1;
    end
end
h = [H{:}];
h = h(~isempty(h));
if strcmp(dimunit{1},dimunit{2}) && strcmp(dimunit{2},dimunit{3})
    axis image
else
    axis tight
end
xlabel(genlabel(self,dim(1)))
ylabel(genlabel(self,dim(2)))
zlabel(genlabel(self,dim(3)))
cb = colorbar;
ylabel(cb,self.units)
axh = ancestor(h(1),'axes');
set(axh,'ZDir','Reverse','ydir','Reverse')
set(axh,'NextPlot',holdstate);
figh = ancestor(h(1),'figure');
if ischar(options.clim) && strcmp(options.clim,'auto')
    options.clim = autoclim(self.data(:));
end
caxis(options.clim);
figure(figh);shg;
fnames = fieldnames(options);


for i = 1:length(fieldnames(options));
    if isprop(axh,fnames{i});
        set(axh,fnames{i},options.(fnames{i}));
    end
end

end