function self = splicedim(self,innerdim,outerdim)
[idim,idimname,idimunit] = self.getdim(innerdim);
[odim,odimname,odimunit] = self.getdim(outerdim);
if ~strcmpi(idimunit,odimunit)
    warning('dim:mismatch','Dimensions of %s (%s) and %s (%s) do not match. Attempting to rescale %s to %s',idimname,idimunit,odimname,odimunit,odimname,idimunit)
    self = rescale(self,odimname,idimunit);
    odimunit = idimunit;
end
order = [find([1:ndims(self)]~=idim & [1:ndims(self)]~=odim) idim odim];
[~,reorder] = sort(order(1:end-1));
self = permute(self,order);
sz = ones(1,ndims(self));
sz(1:ndims(self.data)) = size(self.data);
self.data = reshape(self.data,[sz(1:end-2) sz(end-1)*sz(end)]);
[X,Y] = meshgrid(self.dims.(odimname),self.dims.(idimname));
self.dims.(idimname) = X(:)+Y(:);
self.dims = rmfield(self.dims,odimname);
self.dims.names = self.dims.names(1:end-1);
self.dims.units = self.dims.units(1:end-1);
self = permute(self,reorder);
self.log = char(self.log,sprintf('Spliced dimension %s into %s, removing %s',odimname,idimname,odimname));
