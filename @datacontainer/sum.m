 function self = sum(self,dim)
            if iscell(dim)
                for i = 1:length(dim)
                    self = sum(self,dim{i});
                end
                return
            end
            [dim,dimname] = self.getdim(dim);
            self.data = nansum(self.data,dim);
            self.dims.(dimname) = nan;
            self.log = char(self.log,sprintf('Summed in the %s dimension',dimname));
        end
        