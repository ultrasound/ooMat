function self = medfilt(self,dims,K)
if ~iscell(dims)
    if ischar(dims)
        dims = {dims};
    else
        dims = mat2cell(dims,1,ones(1,length(dims)));
    end
end
if numel(K) == 1
    K = repmat(K,1,length(dims));
end
nd = ndims(self);
L0 = datasize(self);
Y = self.data;
L = L0;
for i = 1:length(dims);
    [dim(i),dimName{i}] = self.getdim(dims{i});
    szi = L;
    szi(dim(i)) = (K(i)-1)/2;
    Y = cat(dim(i),nan(szi),Y,nan(szi));
    L = size(Y);
end
nl = prod(L);
K1 = ones(1,nd);
K1(dim) = K;
nk = prod(K1);
if nk*nl>1e9
fprintf('Allocating %0.0f-element matrix (%s)... ',nk*nl,class(Y));
end
Y_big = zeros([nk,L],class(Y));
info = whos('Y_big');
if nk*nl>1e9
fprintf('%0.5g bytes\n',info.bytes);
end
shift_scale = ones(1,nd);
shift_scale(1:length(L)) = cumprod([1 L(1:end-1)]);
for i = 1:length(K1);
    I{i} = shift_scale(i)*(-(K1(i)-1)/2:(K1(i)-1)/2);
end
shift_i = cell(length(I),1);
[shift_i{:}] = ndgrid(I{:});
shift_mat = cell2mat(cellfun(@(x)x(:)',shift_i,'UniformOutput',false));
shift = sum(shift_mat,1);
index = (0:nl-1);
for i = 1:length(shift);
    Y_big(i,:) = Y(mod(index+shift(i),nl)+1);
end
Ymed = shiftdim(nanmedian(Y_big,1),1);
subs = struct;
subs.type = '()';
for i = 1:nd;
    subs.subs{i} = ((K1(i)-1)/2)+(1:L0(i));
end
self.data = subsref(Ymed,subs);
left = @(s,n)s(1:end-n);
self.log = char(self.log,sprintf('[%s] median filter in {%s} dimension(s)',left(sprintf('%gx',K),1),left(sprintf('%s, ',dimName{:}),2)));

