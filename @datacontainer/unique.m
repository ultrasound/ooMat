function self = unique(self,dim);

[dim dimName dimUnit] = self.getdim(dim);
log = self.log;
x = self.dims.(dimName);
[xsort index] = unique(x(:));
self = slice(self,dim,index);
self.log = char(log,sprintf('sorted %s dimensions to unique values',dimName));