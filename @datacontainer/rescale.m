function self = rescale(self,dim,tgtunit,varargin)
        %RESCALE Convert units on data or dimension
        %self = rescale(self,dim,tgtunit)
        %self = rescale(self,tgtunit)
        %
        %INPUTS:
        %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %   dim: active dimension. May be the index of the dimension,
        %      or the string matching the dimension name in
        %      self.dims.names. If only 2 inputs are provided, or dim is
        %      set to either 0 or 'data', the conversion will happen on
        %      self.data
        %   tgtunit: output unit specification
        %
        %OUTPUTS:
        %   self: output datacontainer
        %
        %RESCALE uses <a
        %href="matlab:helpPopup('getunitconversion')">getunitconversion</a> to calculate the conversion factor 
        %for the requested data. The units field of either self or
        %self.dims is used to determine the source unit. See the help
        %function for getunitconversion for details on the conversion.
        %
        %See Also: getunitconversion
        
            if ~exist('tgtunit','var')
                tgtunit = dim;
                dim = 0;
            end
            
            if iscell(dim)
                for i = 1:length(dim)
                    if iscell(tgtunit)
                        self = self.rescale(dim{i},tgtunit{i},varargin{:});
                    else
                        self = self.rescale(dim{i},tgtunit,varargin{:});
                    end
                end
                return
            end
            
            if (ischar(dim) && strcmpi(dim,'data')) || all(dim==0) 
                dimunit = self.units;
                if isempty(dimunit)
                    self.units = tgtunit;
                    self.log = char(self.log,sprintf('Assigned units of %s to %s',tgtunit,'data'));
                else
                    scl = getunitconversion(dimunit,tgtunit,varargin{:});
                    self.units = tgtunit;
                    self.data = scl*self.data;
                    self.log = char(self.log,sprintf('Rescaled data from units of %s to %s',dimunit,tgtunit));
                end
            else
                %tgtunit = varargin{1};
                [dim,dimname,dimunit] = self.getdim(dim);
                if isempty(dimunit)
                    self.dims.units{dim} = tgtunit;
                    self.log = char(self.log,sprintf('Assigned units of %s to %s',tgtunit,dimname));
                else
                    scl = getunitconversion(dimunit,tgtunit,varargin{:});
                    self.dims.units{dim} = tgtunit;
                    self.dims.(dimname) = self.dims.(dimname)*scl;
                    self.log = char(self.log,sprintf('Rescaled %s from units of %s to %s',dimname,dimunit,tgtunit));
                end
            end
            
        end