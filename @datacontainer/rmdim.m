function self = rmdim(self,dim);
if iscell(dim)
    for i = 1:length(dim)
        self = rmdim(self,dim{i});
    end
else
[dim,dimName] = getdim(self,dim);
if size(self,dim)>1
    error('cannot remove non-singleton dimension %s',dimName);
end
dimOrder = 1:ndims(self);
dimOrder(dim) = inf;
[~,sOrder] = sort(dimOrder);
if dim~=ndims(self);
self = permute(self,sOrder);
end
self.dims = rmfield(self.dims,dimName);
self.dims.names = self.dims.names(1:end-1);
self.dims.units = self.dims.units(1:end-1);
self.log = char(self.log,sprintf('removed trailing singleton dimension %s',dimName));
end

