function self = cart2pol(self,xdim,ydim,th_rad,r,x0,y0)
%CART2POL resample on polar grid
%self = cart2pol(self,xdim,ydim,th_rad,r,x0,y0)
%
%
%CART2POL returns a datacontainer object with dimensions xdim,ydim
%resampled in polar coordinates, at th,r, using an origin of x0, y0.
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%       xdim: x-dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names. 
%       ydim: y-dimension
%     th_rad: vector specifying theta angle in radians
%          r: vector specifyin radial position in units matching xdim
%       [x0]: x-origin for transform (defaults to 0);
%       [y0]: y-origin for transform (defaults to 0);
%
%OUTPUTS:
%   self: transformed datacontainer
%
[xdim, xdimname, xdimunit] = getdim(self,xdim);
[ydim, ydimname, ydimunit] = getdim(self,ydim);
if ~strcmp(xdimunit,ydimunit);
    warning('Dimension mismatch. Attepmting to scale %s to %s...',ydimname,xdimunit)
    self = rescale(self,ydimname,xdimnuit);
end
x = self.dims.(xdimname);
y = self.dims.(ydimname);
if ~exist('x0','var')
    x0 = 0;
end
if ~exist('y0','var')
    y0 = 0;
end
pol2xy = @(th,r,x0,y0)deal(r.*cos(th)+x0,r.*sin(th)+y0);
self = plaid2plaid(self,xdim,ydim,pol2xy,th_rad,r,x0,y0);
self = setdim(self,xdim,th_rad,'th','rad');
self = setdim(self,ydim,r,'r',xdimunit);
self.log = char(self.log,sprintf('Converted (%s,%s) to (th,r); origin = (%g,%g)',xdimname,ydimname,x0,y0));

