function self = setunit(self,unit)
    self.units = unit;
    self.log = char(self.log,sprintf('set units to %s',unit));
    