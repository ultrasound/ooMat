 function [self, locobj] = subsamplepeak(self,dim)
            [dim,dimname,dimunits] = self.getdim(dim);
            [self.data, pkloc] = subsamplepeak(self.dims.(dimname),self.data,dim);
            if nargout>1
                locobj = self;
                locobj.data = pkloc;
                locobj.data(isnan(self.data)) = nan;
                locobj.units = dimunits;
                locobj.dims.(dimname) = nan;
                locobj.log = char(self.log,sprintf('subsample location of peak value in %s dimension',dimname));
            end
            self.dims.(dimname) = nan;
            self.log = char(self.log,sprintf('subsample peak value taken in the %s dimension',dimname));
        end