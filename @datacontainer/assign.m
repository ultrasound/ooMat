function self = assign(self,dim,index,varargin)
%ASSIGN Assign data along specified dimension
%self = assign(self,dim,index,value)
%self = assign(self,dim1,index1,dim2,index2,...etc...,value)
%self = assign(self,dimCell,indexCell,value)
%
%ASSIGN returns a datacontainer object with new values inserted
%
%INPUTS:
%   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%       dim: active dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names
%     index: vector containing the indices to return in the
%           specified dimension(s)
%     value: variable containing the values to set in the
%           specified dimension(s)
%
%OUTPUTS:
%   self: full datacontainer

% force inputs to cell arrays
if ~iscell(dim)
    dim = {dim};
end
if ~iscell(index)
    index = {index};
end

if nargin < 4
    error('not enough input arguments')
else
    if length(varargin) == 1
        value = varargin{1};
    elseif mod(length(varargin),2) == 1 %odd number of variable args in
        %parse the odd and even arguments
        for i = 1:2:length(varargin)-1
            dim{length(dim)+1} = varargin{i};
            index{length(index)+1} = varargin{i+1};
        end
        value = varargin{end};
    else
        error('not right number of input arguments');
    end
end

ndim = ndims(self);

s.type = '()';
s.subs = cell(1,ndim);
s.subs(1:ndim) = {':'};

for i = 1:length(dim)
    [curdim, dimname, dimunit] = self.getdim(dim{i});

    if any(index{i}>datasize(self,curdim))
        error('index out of range');
    end
    curindex = mod(index{i}-1,datasize(self,curdim))+1;

    s.subs{curdim} = curindex;
end

% If the value is a datacontainer, assign the data
% TODO: implement broadcasting via datacontainer\broadcast
if isobject(value),  value = value.data; end

self.data = subsasgn(self.data,s,value);

% Get first dim and values for the log
[curdim, dimname, dimunit] = self.getdim(dim{1});
if isempty(index{1})
    error('Could not assign in %s',dimname);
elseif numel(index{1})==1
    switch class(self.dims.(dimname))
        case 'char'
            val = self.dims.(dimname);
        case 'cell'
            val = self.dims.(dimname){1};
        otherwise
            val = sprintf('%g %s',self.dims.(dimname)(index{1}),dimunit);
    end
    logstr = sprintf('Assigned %s sample in %s dimension (%s = %s)',nth(index{1}),dimname,dimname,val);
else
    switch class(self.dims.(dimname))
        case 'cell'
            val1 = self.dims.(dimname){1};
            val2 = self.dims.(dimname){end};
        otherwise
            val1 = sprintf('%g %s',min(self.dims.(dimname)),dimunit);
            val2 = sprintf('%g %s',max(self.dims.(dimname)),dimunit);
    end
    logstr = sprintf('Assigned %g samples in the %s dimension (%s <= %s <= %s)',length(index),dimname,val1,dimname,val2);
end
self.log = char(self.log,logstr);

end