 function self = normalize(self,range)
            if ~exist('range','var')
                range = prctile(self.data(:),[0 100]);
            end
            if isnan(range(1))
                range(1) = prctile(self.data(:),1);
            end
            if isnan(range(2))
                range(2) = prctile(self.data(~isnan(self.data)),99);
            end
            self.data = min(1,max(0,(self.data-range(1))/diff(range),'includenan'),'includenan');
            self.log = char(self.log,sprintf('normalized to %g - %g %s',range(1),range(2),self.units));           
            self.units = '';
        end
        