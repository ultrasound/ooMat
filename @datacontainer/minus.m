function self = minus(self,B)
            if isnumeric(self)
                self = B.times(-1).plus(self);
            elseif isnumeric(B)
                sza = datasize(self);
                for i = 1:length(sza)
                    szb(i) = size(B,i);
                end
                if any(szb>1 & sza~=szb)
                    error('Size Mismatch')
                end
                if ~all(szb==1)
                B = repmat(B,sza./szb);
                self.data = self.data - B;
                self.log = char(self.log,sprintf('subtracted outside data'));
                else
                self.data = self.data - B;
                self.log = char(self.log,sprintf('subtracted %g %s from data values',B,self.units));
                end
            elseif isa(B,'datacontainer')
                [self,B] = broadcast(self,B);
                if ~strcmp(self.units,B.units)
                    error('Unit mismatch')
                end
                %sza = datasize(self);
                %szb = datasize(B);
                %if ndims(self)~=ndims(B) || any(szb>1 & sza~=szb)
                %    error('Size Mismatch')
                %end
                %B.data = repmat(B.data,sza./szb);
                self.data = self.data-B.data;
                self.log = char(self.log,sprintf('Subtracted %s',class(B)));
            else
                error('Type mismatch')
            end
        end