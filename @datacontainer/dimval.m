function x = dimval(self,dim, shape)
[dim, dimName] = getdim(self,dim);
x = self.dims.(dimName);
if ~exist('shape','var')
    shape = 'same';
end
switch shape
    case 'same'
        x = x;
    case 'match'
        pmute = [2:dim 1 dim+1:self.ndims];
        x = permute(x(:),pmute);
    case 'row'
        x = x(:).';
    case 'column'
        x = x(:);
end