 function iseq = eq(self1,self2)
            if ~(isa(self1,'datacontainer') && isa(self2,'datacontainer'))
                error('both objects must be datacontainers');
            end
            iseq = (pm_hash('crc',getByteStreamFromArray(self1))==pm_hash('crc',getByteStreamFromArray(self2)));
        end