function self = splitdim(self,innerdim,odimname,outer_n,outer_x0);
log = self.log;
[idim,idimname,idimunit] = self.getdim(innerdim);
x = reshape(self.dimval(idim),[],outer_n);
if ~exist('outer_init','var');
    outer_x0 = 0;
end
xi = x(:,1) - outer_x0;
xo = x(1,:)' - xi(1);
nd = self.ndims;
sz = self.datasize;
self.data = permute(reshape(self.data,[sz(1:idim-1) length(xi) length(xo) sz(idim+1:end)]),[1:idim idim+2:nd+1 idim+1]);
self = self.setdim(nd+1,xo,odimname,idimunit);
self = self.setdim(idim,xi);
self.log = char(log,sprintf('Split dimension %s into %s (%g samples) and %s (%g samples) with %s(1) = %g',idimname,idimname,numel(xi),odimname,numel(xo),odimname,outer_x0));
