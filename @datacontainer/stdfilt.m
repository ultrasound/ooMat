function self = stdfilt(self,dim,k)
        %GRADIENT Compute gradient in specified dimension
        %self = GRADIENT(self,dim)
        %[self1 self2...] = GRADIENT(self,{dim1,dim2...})
        %
        %INPUTS:
        % self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %     dim: active dimension. May be the index of the dimension,
        %       or the string matching the dimension name in
        %       self.dims.names
        %
        %OUTPUTS:
        % self: output datacontainer
        %
        %Gradient take the partial derivative (diff) of self.data
        %in the specified dimension, adjusting the self.units parameter
        %accordingly, and shortening self.dims.(dim)
        %
        %See Also: diff
            if ~exist('dim','var')
               error('not enough input arguments')
            end
            if ~exist('k','var')
                k = 3;
            end
            log = self.log;
            if ~iscell(dim)
                dim = {dim};
            end
            olddims = self.dims.names;
            for i = 1:length(dim)
            [dimIdx(i),dimName{i},dimUnit{i}] = self.getdim(dim{i});    
            end
            isindexed = zeros(1,ndims(self));
            isindexed(dimIdx) = 1;
            if (numel(k) == 1) && length(dim) > 1
                k = repmat(k,1,length(dim));
            end
            if numel(k)>length(dim)
                error('too many dimensions in the neighborhood')
            end
            if length(dim)>2
                error('too many dimensions (max 2)')
            end
            self = permute(self,dim);
            nhood = ones([k(:)' 1]);
            if numel(k) == 1;
                data = reshape(self.data,size(self.data,1),1,[]);
            else
                data = reshape(self.data,size(self.data,1),size(self.data,2),[]);
            end
            for i = 1:size(data,3);
                data(:,:,i) = stdfilt(data(:,:,i),nhood);
            end
            self.data(:) = data;
            self = permute(self,olddims);
            self.log = char(log,sprintf('[%s]-point std filt in the [%s] dimension',sprintf('%g ',k),sprintf('%s ',dimName{:})));
            
end
