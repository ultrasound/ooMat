 function self = abs(self)
            self.data = abs(self.data);
            self.log = char(self.log,'Absolute value of data');
        end