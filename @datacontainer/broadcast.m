%BROADCAST Repmat two datacontainers to match in size
function [A,B] = broadcast(A,B)
    na = A.ndims;
    nb = B.ndims;
    n = max(na,nb);
    for i = 1:n
        if i>na
            A = A.setdim(i,NaN,B.dims.names{i},B.dims.units{i});
        end
        if i>nb
            B = B.setdim(i,NaN,A.dims.names{i},A.dims.units{i});
        end
    end
    sza = A.datasize([1:n]);
    szb = B.datasize([1:n]);
    left = @(x,n)x(1:end-n);
    if any(sza~=1 & szb~=1 & sza~=szb)
        error('Could not broadcast [%s] onto [%s]',left(sprintf('%gx',sza),1),left(sprintf('%gx',szb),1)); 
    end
    repA = ceil(szb./sza);
    A.data = repmat(A.data,repA);
    repA_dims = find(repA>1);
    for i = repA_dims
        A = A.setdim(i,B.dimval(i));
    end
    repB = ceil(sza./szb);
    B.data = repmat(B.data,repB);
    repB_dims = find(repB>1);
    for i = repB_dims
        B = B.setdim(i,A.dimval(i));
    end
    if ~isempty(repA_dims)
        A.log = char(A.log,sprintf('Broadcast from  [%s] to [%s]',left(sprintf('%gx',sza),1),left(sprintf('%gx',A.datasize),1))); 
    end
    if ~isempty(repB_dims)
        B.log = char(B.log,sprintf('Broadcast from  [%s] to [%s]',left(sprintf('%gx',szb),1),left(sprintf('%gx',B.datasize),1))); 
    end
end
        
    
    
    