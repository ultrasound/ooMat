function dataobj = iqr(dataobj,dim);
% if ~exist('dim','var')
%     dim = 1;
% end
% [dim dimname dimunit] = getdim(dataobj,dim);
% iqrobj = slice(dataobj,dim,1);
% iqrobj.data = iqr(dataobj.data,dim);
% iqrobj.dims.(dimname) = 0;
% iqrobj.log = char(dataobj.log,sprintf('interquartile range in the %s dimension',dimname));

if ~exist('n','var')
    n = 0;
end
if iscell(dim)
    for i = 1:length(dim)
        [dimidx(i) dimname{i}]= dataobj.getdim(dim{i});
        dataobj.dims.(dimname{i}) = mean(dataobj.dims.(dimname{i}));
    end
    order = 1:ndims(dataobj);
    order(dimidx) = inf;
    [~,pOrder] = sort(order);
    [~,rOrder] = sort(pOrder);
    data = permute(dataobj.data,pOrder);
    sz = size(data);
    data = iqr(reshape(data,[sz(1:end-length(dimidx)) prod(sz(end-length(dimidx)+1:end))]),ndims(dataobj)-length(dimidx)+1);
    dataobj.data = permute(data,rOrder);
    dataobj.log = char(dataobj.log,sprintf('IQR taken in the %s%s dimension',dimname{1},sprintf(',%s',dimname{2:end})));
else
    [dim,dimname] = dataobj.getdim(dim);
    dataobj.data = iqr(dataobj.data,dim);
    if ~iscell(dataobj.dims.(dimname))
        dataobj.dims.(dimname) = mean(dataobj.dims.(dimname));
    else
        dataobj.dims.(dimname) = {nan};
    end
    dataobj.log = char(dataobj.log,sprintf('IQR taken in the %s dimension',dimname));
end
end

