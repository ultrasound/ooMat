function self = real(self)
%REAL Return real part of complex signal
%self = real(self)
%
%
%REAL returns a datacontainer object containing the real part of the data
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%OUTPUTS:
%   self: real-valued datacontainer
%
self.data = real(self.data);
self.log = char(self.log,'Took real part of values');

