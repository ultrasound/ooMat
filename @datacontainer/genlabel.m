   function str = genlabel(self,dim,index)
        [dim,dimName,dimUnit] = self.getdim(dim);
        if ~exist('index','var')
            if isempty(dimUnit)
                str = sprintf('%s',dimName);
            else
                str = sprintf('%s (%s)',dimName,dimUnit);
            end
        else
            if iscell(self.dims.(dimName))
                x = self.dims.(dimName){index};
                if ischar(x)
                    xstr = x;
                elseif isnumeric(x)
                    xstr = sprintf('%0.3g',x);
                end
                str = sprintf('%s = %s', dimName, xstr);
            else
                if isempty(dimUnit)
                    unitstr = '';
                else
                    unitstr = sprintf(' %s',dimUnit);
                end
                val = sprintf('%0.3g%s',self.dims.(dimName)(index),unitstr);
                str = sprintf('%s = %s',dimName,val);
            end
            
        end
   end