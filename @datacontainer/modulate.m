function self = modulate(self,dim,f,phi)
    [dim, dimName, dimUnit] = getdim(self,dim);
    if ~exist('phi','var')
        phi = 0;
    end
    log = self.log;
    modfun = exp(1j*(2*pi*f*self.dims.(dimName)+phi));
    porder = 1:ndims(self);
    porder(dim) = 1;
    porder(1) = dim;
    modfun = permute(modfun(:),porder);
    self = self*modfun;
    self.log = char(log,sprintf('modulated by %g 1/(%s) in the %s dimension',f,dimUnit,dimName));
end