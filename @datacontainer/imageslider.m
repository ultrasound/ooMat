function h = imageslider(self, dim, varargin)
    %IMAGESLIDER show data with interactive slider
    %h = imageslider(self, dim, varargin)
    %h = imageslider(self, dim, paramName1, paramVal1, paramName2,
    %                paramVal2, ... , clim)
    %
    %IMAGESLIDER returns a figure handle with a horizontal slider at the
    %bottom of the image. Sliding the slider selects from slices in the
    %specified dimension. For data with more than 3 nonsingleton
    %dimensions, right clicking an axis or the slider allows for section of
    %a different dimension for that axis or control.
    %
    %INPUTS:
    %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
    %       dim: active dimension. May be the index of the dimension,
    %           or the string matching the dimension name in
    %           self.dims.names
    %       clim: after the parameter-value pairs, an additional 1x2 matrix
    %           can be specified to set the bounds of the caxis
    %     
    %PARAMETERS:
    %      XDim: x-dimension index or name
    %      YDim: y-dimension index or name
    %   initval: initial nearest value to select from on caxis
    %       fps: maximum frame rate (frames per second) with which to animate
    %
    %OUTPUTS:
    %         h: image handle
    %

if nargin<2
    error('not enough input arguments')
    end
    [dim, dimName, dimUnit] = self.getdim(dim);
    h= imageslider_ui(self,'z',dimName,varargin{:});
    h.imshow();
end