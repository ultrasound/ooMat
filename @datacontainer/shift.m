function self = shift(self,dim,dx)
log = self.log;
[dim dimName dimUnit] = self.getdim(dim);
if isa(dx,'datacontainer')
    dx = dx.permute(self.dims.names).rescale(dimUnit).data;
end
x = self.dimval(dim);
G = self.fft(dim);
realflag = isreal(self.data);
omega = 2*pi*G.dimval(dim);
omega = reshape(omega,[ones(1,dim-1) length(omega) 1]);
sz0 = size(omega);
osz = ones(1,self.ndims);
osz(1:length(sz0)) = sz0;
dsz = G.datasize;
if numel(dx) > 1 && false
    Omega = repmat(omega,dsz./osz);
    dxsz = size(dx);
    xsz = ones(1,self.ndims);
    xsz(1:length(dxsz)) = dxsz;
    if any((dsz ~= 1) & (xsz ~= 1) & (dsz ~= xsz)) || any(xsz>dsz)
        left = @(x,n)x(1:end-n);
        error('broadcast dimension error: [%s] vs [%s]',left(sprintf('%gx',dsz),1),left(sprintf('%gx',xsz),1));
    end
    DX = repmat(dx,dsz./xsz);
    G = G*exp(1j*Omega.*DX);
else
    Omega = omega;
    G.data = bsxfun(@times,G.data,exp(1j*Omega*dx));
end
self = G.ifft(dim);
if realflag
    self = self.real;
end
self = self.setdim(dim,x);
self.log = char(log,sprintf('shifted in the %s dimension',dimName));