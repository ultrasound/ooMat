function s = struct(self)
            p = properties(self);
            s = struct;
            for i = 1:length(p)
                s.(p{i}) = self.(p{i});
            end
            s.class = class(self);
        end