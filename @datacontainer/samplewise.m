function self = samplewise(self,fun,varargin)
self.data = fun(self.data,varargin{:});
funstr = evalc('disp(fun)');
funstr = deblank(funstr(5:end));
self.log = char(self.log,sprintf('Applied function %s to the data',funstr));
end