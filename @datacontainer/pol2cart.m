function self = pol2cart(self,thdim,rdim,x,y,x0,y0)
%POL2CART resample on cartesian grid
%self = pol2cart(self,thdim,rdim,x,y,x0,y0)
%
%
%POL2CART returns a datacontainer object with dimensions thdim,rdim
%resampled in cartesian coordinates, at x,y, using an origin of x0, y0.
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%       thdim:theta-dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names. 
%       rdim: r-dimension
%          x: vector specifying x-position
%          y: vector specifying y-position
%       [x0]: x-origin for transform (defaults to 0);
%       [y0]: y-origin for transform (defaults to 0);
%
%OUTPUTS:
%   self: transformed datacontainer
%
self = rescale(self,thdim,'rad');
[thdim, thdimname] = getdim(self,thdim);
[rdim, rdimname, rdimunit] = getdim(self,rdim);
th = self.dims.(thdimname);
r = self.dims.(rdimname);
if ~exist('x0','var')
    x0 = 0;
end
if ~exist('y0','var')
    y0 = 0;
end
xy2pol = @(x,y,x0,y0)deal(atan2(y-y0,x-x0),sqrt((x-x0).^2+(y-y0).^2));
self = setdim(slice(self,thdimname,[length(th) 1:length(th) 1]),thdimname,[th(end)-2*pi; th(:); th(1)+2*pi]);
self = plaid2plaid(self,thdim,rdim,xy2pol,x,y,x0,y0);
% 
% dimorder = 1:ndims(self);
% dimorder([thdim rdim]) = nan;
% dimorder = sort(dimorder);
% dimorder = [thdim rdim dimorder(1:end-2)];
% [~, reorder] = sort(dimorder);
% datap = permute(self.data,dimorder);
% [XX, YY] = ndgrid(x,y);
% [TTH, RR] = cart2pol(XX-x0,YY-y0);
% msk = RR(:)>=r(1) & RR(:)<=r(end);
% dr = (r(2)-r(1));
% dth = (th(2)-th(1));
% II = interp1([th-2*pi th th+2*pi],(-length(th)+1):2*length(th),TTH(msk));
% JJ = interp1(r,1:length(r),RR(msk));
% %%II = (TTH-th(1))/dth + 1;
% %%JJ = (RR-r(1))/dr + 1;
% IDXff = sub2ind([length(th) length(r)],mod(floor(II-1),length(th))+1,floor(JJ));
% IDXfc = sub2ind([length(th) length(r)],mod(floor(II-1),length(th))+1,ceil(JJ));
% IDXcf = sub2ind([length(th) length(r)],mod(ceil(II-1),length(th))+1,floor(JJ));
% IDXcc = sub2ind([length(th) length(r)],mod(ceil(II-1),length(th))+1,ceil(JJ));
% COEFff = (1-mod(II,1)).*(1-mod(JJ,1));
% COEFfc = (1-mod(II,1)).*(mod(JJ,1));
% COEFcf = (mod(II,1)).*(1-mod(JJ,1));
% COEFcc = (mod(II,1)).*(mod(JJ,1));
% szp = size(datap);
% datap = datap(:,:,:);
% IMCART = nan([size(YY) size(datap,3)]);
% for tidx = 1:size(datap,3);
% DATAi = datap(:,:,tidx);
% IMCARTi = nan(size(YY));
% IMCARTi(msk) =   DATAi(IDXff).* COEFff + ...
%                 DATAi(IDXfc).* COEFfc + ...
%                 DATAi(IDXcf).* COEFcf + ...
%                 DATAi(IDXcc).* COEFcc;
% IMCART(:,:,tidx) = IMCARTi; 
% end
% self.data = permute(reshape(IMCART,[size(IMCART,1),size(IMCART,2),szp(3:end)]),reorder);
self = setdim(self,thdim,x,'x',rdimunit);
self = setdim(self,rdim,y,'y',rdimunit);
self.log = char(self.log,sprintf('Converted (%s,%s) to (x,y); origin = (%g,%g)',thdimname,rdimname,x0,y0));

