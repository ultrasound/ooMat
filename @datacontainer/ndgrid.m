function grid = ndgrid(self)
grid = struct;
A = cell(1,ndims(self));
for i = 1:ndims(self)
    X{i} = self.dims.(self.dims.names{i});
    if iscell(X{i})
        X{i} = (1:numel(X{i}))';
    end
end
[A{:}] = ndgrid(X{:});
for i = 1:ndims(self)
    grid.(self.dims.names{i}) = A{i};
end