function self = polyval(polyobj,x)
        %POLYVAL evaulate polynomial fit at values
        % 
        %POLYVAL uses the 'poly_power' dimension, set during a call to
        %datacontainer/polyfit, to retrieve the polynomial coefficients of
        %the fit, and evaulates the fit at the values specified in x. The
        %returned object replaces the poly_power dimensions with the
        %x-dimension, which has been intermediately stored in
        %params.poly_dim_name.
        %
        %Inputs:
        %  polyobj: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %        x: values at which to evaulate the fit, in units matching polyobj.dims.poly_power 
        %
        %Outputs:
        %  self: evaulated object
        %
        %
        % See Also: datacontainer/polyfit
        
    reorder = polyobj.dims.names;
    [dim] = getdim(polyobj,'poly_power');
    order = datasize(polyobj,'poly_power')-1;
    reorder{dim} = polyobj.params.poly_dim_name;
    polyobj = permute(polyobj,'poly_power');
    data = polyval_multi(x,polyobj.data);
    self = polyobj;
    self.data = data;
    self = setdim(self,1,x,self.params.poly_dim_name);
    self.params = rmfield(self.params,'poly_dim_name');
    self = permute(self,reorder);
    self.log = char(polyobj.log,sprintf('Evaulated %s order polynomial fit in %s at %g values',nth(order),polyobj.params.poly_dim_name,length(x)));
end    