function [imH, sldH] = imagesliders(self,dim,varargin);
            if nargin<2
                error('not enough input arguments')
            end
            if ~iscell(dim)
                dim = {dim};
            end
            ndims = length(dim);
            sz = size(self.data);
            nonsingleton = find(sz>1);
            singleton = find(sz==1);
            if length(nonsingleton) ~= ndims+2
                error('size mismatch')
            end
            dataslice = self;
            for i = 1:length(dim)
                dataslice = slice(dataslice,dim{i},1);
            end
            imH = imagesc(dataslice,varargin{:});               
            drawnow
            fig0 = ancestor(imH,'Figure');
            pos0 = get(fig0,'Position');
            fig1 = figure(300+fig0.Number);
            sliderH = 20;
            vMargin = 18;
            leftW = vMargin*4;
            rightW = vMargin*4;
            barH = 30;
            pos1 = [pos0(1) pos0(2)-((sliderH+vMargin)*ndims+barH) pos0(3) (sliderH+vMargin)*ndims];
            set(fig1,'Position',pos1);
            switch fig0.NumberTitle
                case 'off'
                    if ~isempty(fig0.Name)
                        ttl = sprintf('Slider Controls for %s',fig0.Name);
                    else
                        ttl = sprintf('Slider Controls');
                    end
                case 'on'
                    if ~isempty(fig0.Name)
                        ttl = sprintf('Slider Controls for Figure %g: %s',fig0.Number,fig0.Name);
                    else
                        ttl = sprintf('Slider Controls for Figure %g',fig0.Number);
                    end
            end
            set(fig1,'ToolBar','none','MenuBar','none','NumberTitle','off','Name',ttl,'Units','Pixels')
            figure(fig1);
            for i = 1:ndims
                [dimIndex dimName dimUnit] = getdim(self,dim{i});
                %spos = round([0.15*pos1(3),((i-1)*(1.25/(ndims+1))+(0.25/(ndims+1)))*pos1(4),0.75*pos1(3),sliderH*pos1(4)]);
                spos = [leftW (i-1)*(vMargin+sliderH) pos1(3)-(leftW+rightW) sliderH]; 
                sliders(i) = uicontrol('Style', 'slider',...
                        'Min',1,'Max',datasize(self,dim{i}),'Value',1,...
                        'Units','normalized',...
                        'Position', spos./pos1([3 4 3 4]),...
                        'Callback',@cDataUpdate,...
                        'SliderStep',[1/(datasize(self,dim{i})-1) max(1/(datasize(self,dim{i})-1),0.1)],...
                        'Tag','ImageSlider');
                ltxt = uicontrol('Style','text',...
                        'Position',[0 spos(2) leftW sliderH],...
                        'String',sprintf('%g',self.dims.(dimName)(1)),...
                        'HorizontalAlignment','Right','FontSize',sliderH*0.6);
                rtxt = uicontrol('Style','text',...
                        'Position',[pos1(3)-rightW spos(2) rightW sliderH],...
                        'String',sprintf('%g',self.dims.(dimName)(end)),...
                        'HorizontalAlignment','Left','FontSize',sliderH*0.6);
                ttltxt = uicontrol('Style','text',...
                        'Position',[0 (i-1)*(vMargin+sliderH)+sliderH pos1(3) vMargin*0.9],...
                        'String',sprintf('%s',genlabel(self,dim{i})),...
                        'HorizontalAlignment','Center','FontSize',vMargin*0.6);
                sliders(i).UserData.dim = dimIndex;
                set(sliders(i),'Callback',@sliderUpdate)
            end
            fig1.UserData.SourceObject = self;
            fig1.UserData.SourceImage = imH;
            fig1.UserData.dims = dim;
            fig1.UserData.sliders = sliders;
end


function sliderUpdate(hObject,eventdata)
idx = round(hObject.Value);
hObject.Value = idx;
figH = ancestor(hObject,'figure');
dataslice = figH.UserData.SourceObject;
for i = 1:length(figH.UserData.dims)
    dataslice = slice(dataslice,figH.UserData.dims{i},figH.UserData.sliders(i).Value);
end
set(figH.UserData.SourceImage,'cdata',squeeze(dataslice.data));
set(figH.UserData.SourceImage,'alphadata',~isnan(squeeze(dataslice.data)));
% switch class(hObject.UserData.t)
%     case 'cell'
%     val = hObject.UserData.t{idx};
%     case 'char'
%         val = hObject.UserData.t;
%     otherwise
%         val = sprintf('%0.3g %s',hObject.UserData.t(idx),hObject.UserData.dimUnit);
% end
% title(ancestor(hObject.UserData.imHandle,'axes'),sprintf('%s = %s',hObject.UserData.dimName,val));
end