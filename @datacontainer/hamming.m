        function self = hamming(self,dim)
            [dim,dimname] = self.getdim(dim);
             pDim = [1:ndims(self)];
             pDim(dim) = 0;
             [~, pOrder] = sort(pDim);
             [~, rOrder] = sort(pOrder);
             pdata = permute(self.data,pOrder);
             sz = size(pdata);
             pdata(:,:) = pdata(:,:).*repmat(hamming(sz(1)),[1 prod(sz(2:end))]);
             self.data = permute(pdata,rOrder);
             self.log = char(self.log,sprintf('Hamming window in the %s dimension',dimname));
        end