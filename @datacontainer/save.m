function save(self,filename,overwrite,verbose)
if ~exist('overwrite','var')
    overwrite = 0;
end
if ~exist('verbose','var')
    verbose = true;
end
[path,fname,ext] = fileparts(filename);
if isempty(path)
    path = pwd;
end
fullfilename = fullfile(path,[fname '.mat']);
if exist(fullfilename,'file') && ~overwrite
    error('no overwrite')
end
self = struct(self);
if verbose
    fprintf('saving %s...',fullfilename);
end
save(fullfilename,'-struct','self','-v7.3');
if verbose
    d = dir(fullfilename);
    fprintf('done (%sB)\n',estring(d.bytes,3,'.'));
end
    
    