function radonobj = radon_sum(self,xdim,ydim,x0,dydx)
        %RADON_SUM Find the summed projection along specified vectors
        %
        %INPUTS:
        %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %   xdim: 1st active dimension. May be the index of the dimension,
        %      or the string matching the dimension name in
        %      self.dims.names. 
        %   ydim: 2nd active dimension.
        %     x0: vector of xdim values to start at
        %     y0: vector of slopes to test
        %OUTPUTS:
        %   self: output datacontainer. xdim will be replaced by x0, and
        %   ydim will be replaced by y0
        
        [xdim, xdimName, xdimUnit] = self.getdim(xdim);
        [ydim, ydimName, ydimUnit] = self.getdim(ydim);
        dimorder = 1:ndims(self);
        dimorder(xdim) = -1;
        dimorder(ydim) = 0;
        [dimsort, sortorder] = sort(dimorder);
        self = permute(self,sortorder);
        [X0, DYDX] = ndgrid(x0,dydx);
        sz = size(self.data);
        sz(1) = length(x0);
        sz(2) = length(dydx);
        x = self.dims.(xdimName);
        y = self.dims.(ydimName);
        rsum = nan(sz);
        sz = [length(y) sz];
        presum = nan(sz);
        for xi = 1:length(x0);
            for dyi = 1:length(dydx);
                idxx = interp1(x,1:length(x),x0(xi)+y./dydx(dyi));
                idx1 = floor(idxx);
                idx2 = idx1+1;
                frac = mod(idxx,1);
                for yi = 1:length(y)
                    if isnan(idxx(yi))
                       presum(yi,xi,dyi,:) = nan;
                    else
                        presum(yi,xi,dyi,:) = (1-frac(yi)) * self.data(idx1(yi),yi,:) + (frac(yi)) * self.data(idx2(yi),yi,:);
                    end
                end
                rsum(xi,dyi,:) = nansum(presum(:,xi,dyi,:),1);
            end
        end
        radonobj = self;
        