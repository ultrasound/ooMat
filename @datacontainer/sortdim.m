function self = sortdim(self,dim,mode,remove_duplicates);
if ~exist('mode','var')
    mode = 'ascend';
end
if ~exist('remove_duplicates','var')
    remove_duplicates = false;
end

[dim dimName dimUnit] = self.getdim(dim);
log = self.log;
x = self.dims.(dimName);
xsort = x;
[xsort(:) index] = sort(x(:),1,mode);
self = slice(self,dim,index);
if remove_duplicates
    [x_mono, index] = unique(xsort(:));
    self = self.slice(dim,index);
end
self.log = char(log,sprintf('sorted the %s dimension (%sing)',dimName,mode));