function self = mean(self, dim)
%MEAN take the mean along the specified dimension
%self = slice(self, dim)
%
%MEAN returns a datacontainer object, averaged along a particular dimension
% and skipping nans
%
%INPUTS:
%   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%       dim: active dimension. May be the index of the dimension,
%           or the string matching the dimension name in
%           self.dims.names
%
%OUTPUTS:
%   self: averaged datacontainer
%
    if iscell(dim)
        for i = 1:length(dim)
            self = mean(self,dim{i});
        end
        return
    end
    [dim,dimname] = self.getdim(dim);
    if ~license('test','statistics_toolbox')
        warning('Need to install the Statistics Toolbox')
    end
    self.data = nanmean(self.data,dim);
    self.dims.(dimname) = mean(self.dims.(dimname));
    self.log = char(self.log,sprintf('mean value taken in the %s dimension',dimname));
end
