        function self = logcompress(self)
            self.data = db(self.data);
            if ~isempty(self.units)
                self.units = [self.units '(dB)'];
            else
                self.units = 'dB';
            end
            self.log = char(self.log,'log compressed');
        end