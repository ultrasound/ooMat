function self = setparam(self,paramname,value,dim)
if exist('dim','var')
    [dim, dimName] = self.getdim(dim);
    if sum(size(value)>1)>1
        error('cannot assign parameter to dimension %s',dimName);
    end
    value = permute(value(:),[2:dim 1 dim+1]);
end
self.params.(paramname) = value;
self.log = char(self.log,sprintf('set parameter %s',paramname));

    