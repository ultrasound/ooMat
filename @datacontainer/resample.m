function self = resample(self,dim,k,method,extrap)
            %RESAMPLE Interpolate along specified dimension 
            %self = RESAMPLE(self,dim,x,[method])
            %
            %RESAMPLE resamples self.data in the specified dimension at
            % a fixed upsampling rate k. If a method is specificed, INTERP
            % will use interp1 with the specified method. Two additional
            % methods can be used 'sinc' and 'polyphase'. These methods
            % will use interpft and resample, respectively. Any
            % self.params which share the same size as self.data IN
            % THE SAME DIMENSION will also be resampled.
            %
            %INPUTS:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %  dim: active dimension. May be the index of the dimension,
            %      or the string matching the dimension name in
            %      self.dims.names
            %  k (scalar): resampling factor for the specified dimesnion.
            %  [method] interp1 interpolation method. 'linear' if blank.
            %
            % OUTPUTS:
            % self: updated datacontainer object
            %
            % See Also: datacontainer\interp interp1, signal\resample, interpft
            [dim, dimname] = self.getdim(dim);
            if ~exist('extrap','var')
                extrap = true;
            end
            if ~exist('method','var')
                method = 'linear';
            end
            x0 = self.dims.(dimname);
            if k == 1;
                return
            end
            self = self.interp(dim,{k},method,extrap);