function [h] = boxplot(self,dim,varargin);
            specopts = struct('FaceColor','none','EdgeColor','auto');
            [specopts,options] = setopts(specopts,varargin{:});
            sz = size(self.data);
            nonsingleton = find(sz>1);            
            if ~exist('dim','var')
                dim = nonsingleton(end);
            end
            if (length(dim)+1)<length(nonsingleton)
                error('must specify %g groups for this datacontainer',length(nonsingleton)-1);
            end
            if ~iscell(dim)
                dim = self.getdim(dim);
                dimCell = mat2cell(dim(:),ones(length(dim),1),1);
            else
                dimCell = dim;
            end
            dim = [];
            for i = 1:length(dimCell)
            [dim(i), dimName{i}, dimUnit{i}] = self.getdim(dimCell{i});
            end
            i = 1;
            distdim = nonsingleton(i);
            while any(distdim == dim)
                i = i+1;
                distdim = nonsingleton(i);
            end
            [distdim,distName, distUnit] = self.getdim(distdim);
            self = permute(self,[distdim dim]);
            sz = datasize(self);
            for i = 1:length(dim)
                x = self.dims.(dimName{i});
                if isempty(dimUnit{i})
                    unitstr = '';
                else
                    unitstr = [' ' dimUnit{i}];
                end
                %if i < length(dim)
                %    unitstr = [unitstr ','];
                %end
                if ~iscell(x)
                    x = mat2cell(x(:),ones(length(x),1),1);
                    x = cellfun(@(c)sprintf('%s = %g%s',dimName{i},c,unitstr),x,'uniformoutput',false);
                else
                    x = cellfun(@(c)sprintf('%s%s',c,unitstr),x,'uniformoutput',false);
                end
                    rsz = ones(1,length(sz));
                    rsz(i+1) = length(x);
                    sz1 = sz;sz1(1) = 1;
                    g = reshape(x,rsz);
                    g = repmat(g,sz1./rsz);
                G{i} = g(:)';
            end
            %if nargin>2
                h0 = boxplot(self.data(:,:),G,options{:});
                ax = ancestor(h0(1),'axes');
                g = get(h0(1),'Parent');
                k = get(ax,'Children');
                k = k(2:end);
                if ~ischar(specopts.FaceColor)
                    colors = specopts.FaceColor;
                else
                switch specopts.FaceColor
                    case 'none'
                        colors = [];
                    case 'auto'
                        if size(h0,2)<=ax.ColorOrder
                            colors = get(ax,'ColorOrder');
                        else
                            map = colormap;
                            colors = interp1((0:size(map,1)-1)/(size(map,1)-1),map,linspace(0,1,size(h0,2)));
                        end
                        
                    case 'axis'
                        colors = get(ax,'ColorOrder');
                    case 'cmap'
                        map = colormap;
                        colors = interp1((0:size(map,1)-1)/(size(map,1)-1),map,linspace(0,1,size(h0,2)));
                    otherwise
                        colors = specopts.FaceColor;
                end
                end
                
                if ~isempty(colors)
                    %hld = get(ax,'NextPlot');
                    %hold on
                    p = [];
                    for j = 1:size(h0,2)
                        p(j) = patch(get(h0(5,j),'XData'),get(h0(5,j),'YData'),'k','FaceColor',colors(mod(j-1,size(colors,1))+1,:));
                    end
                    %set(ax,'Children',[g;p(:);k(:)])
                    k = g.Children;
                    set(p(:),'Parent',g)
                    set(g,'Children',[k(:);p(:)]);
                    %set(ax,'NextPlot',hld);
                end
                switch specopts.EdgeColor
                    case 'auto'
                    otherwise
                        set(h0(6,:),'Color',specopts.EdgeColor);
                        set(h0(5,:),'Color',specopts.EdgeColor); 
                end
            %else
            %    h0 = boxplot(self.data(:,:),G);
            %end
            ylabel(self.units);
            if nargout == 1
                h = g;
            end
        end