function varargout = gradient(self,dim,k)
        %GRADIENT Compute gradient in specified dimension
        %self = GRADIENT(self,dim)
        %[self1 self2...] = GRADIENT(self,{dim1,dim2...})
        %
        %INPUTS:
        % self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %     dim: active dimension. May be the index of the dimension,
        %       or the string matching the dimension name in
        %       self.dims.names
        %
        %OUTPUTS:
        % self: output datacontainer
        %
        %Gradient take the partial derivative (diff) of self.data
        %in the specified dimension, adjusting the self.units parameter
        %accordingly, and shortening self.dims.(dim)
        %
        %See Also: diff
            if ~exist('dim','var')
               error('not enough input arguments')
            end
            if ~exist('k','var')
                k = 3;
            end
            if ~iscell(dim)
                dim = {dim};
            end
            for i = 1:length(dim)
            [dim{i}, sgn{i}] = parsedim(dim{i});
            [dimIdx(i),dimName{i},dimUnit{i}] = self.getdim(dim{i});    
            end
            isindexed = zeros(1,ndims(self));
            isindexed(dimIdx) = 1;
            
            for i = 1:length(dim)
            selfi = self;
            [dimidx,dimname,dimunit] = self.getdim(dim{i});
            pOrder = [dimIdx([i:end 1:i-1]) find(~isindexed)];
            [sOrder rOrder] = sort(pOrder);
            datap = permute(self.data,pOrder);
            K = zeros(k*ones(1,length(dim)));
            K(1,:) = -1;
            K(end,:) = 1;
            szp = size(datap);
            datap = sgn{i}*convn(datap,K,'same')/((k*(k-1)*mean(diff(selfi.dims.(dimname)))));
            datap([1 end],:) = nan;
            selfi.data = permute(datap,rOrder);
            
            %self.dims.(self.dims.names{dimidx}) = 0.5*(self.dims.(self.dims.names{dimidx})(1:end-1) + self.dims.(self.dims.names{dimidx})(2:end));
            selfi.units = sprintf('%s/%s',selfi.units,dimunit);
            selfi.log = char(selfi.log,sprintf('%g-point gradient in the %s dimension',k,selfi.dims.names{dimidx}));
            varargout{i} = selfi;
            end
end
        

function [dim, sgn] = parsedim(dim)
if ischar(dim) 
    switch(dim(end))
        case '+';
            sgn = 1;
            dim = dim(1:end-1);
        case '-';
            sgn = -1;
            dim = dim(1:end-1);
        otherwise
            sgn = 1;
    end
else
    sgn = sign(dim);
    dim = abs(dim);
end    
end
