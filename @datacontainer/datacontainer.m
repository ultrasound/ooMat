%

%    Copyright 2015 Peter Hollender
%
%    Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.

classdef datacontainer
    properties
        data = [];
        units = '';
        info = '';
        dims = struct();
        params = struct();
        log = '';
    end
    properties (Hidden = true)
        maxResamplePQsum = 1000;
    end
    methods (Static)
        dims = datadims(values,names,units,varargin);
        anonfun = getscanconverter(spec);
    end
    methods
        %Data Referencing
        self = slice(self,dim,index,varargin)
        self = slicex(self,dim,range,varargin);
        self = permute(self,varargin)
        self = concatenate(self,dim,varargin)
        s = struct(self)
        save(self,filename,overwrite)
        
        %Assignment
        self = assign(self,dim,index,value,varargin)
        
        %Data Dimensions
        [dimidx, dimname, dimunit] = getdim(self,dim_name_or_index);
        [self] = setdim(self,dim,dimval,dimname,dimunit)
        self = setunit(self,unit)
        sz = datasize(self,dim)
        isadim = isdim(self,dimname)
        n = ndims(self)
        self = rescale(self,dim,tgtunit,varargin)
        self = rmdim(self,dim)
        self = sortdim(self,dim,mode,remove_duplicates)
        self = sort(self,dim,mode)
        self = unique(self,dim)
        self = splitdim(self,innerdim,odimname,outer_n,outer_x0);
        self = splicedim(self,innerdim,outerdim)
        grid = ndgrid(self)
        x = dimval(self,dim, shape)
        self = setparam(self,paramname,value,dim)
        [A,B] = broadcast(A,B)
        
        %Data Display
        h = plot(self,varargin)
        h = plotv(self,varargin)
        str = genlabel(self,dim,index)
        [h, cb] = imagesc(self,varargin)
        M = animate(self,dim,varargin);
        [h, sublabel] = imagetile(self,dim,tilingspec,varargin);
        h = quiver(self,varargin)
        H = xsurf(self,dims,cutidx,varargin)
        h = surfc(self,varargin)
        h = imageslider(self,dim,varargin);
        h = boxplot(self,dim,varargin);
        varargout = imagecolor(self,map,varargin)
        
        %Scan-Conversion
        self = scanconvert(self,varargin)
        self = cart2pol(self,xdim,ydim,th_rad,r,x0,y0)
        self = pol2cart(self,thdim,rdim,x,y,x0,y0)
        self = plaid2plaid(self,dim1,dim2,xformhandle,y1,y2,varargin)
        self = radon_sum(self,xdim,ydim,x0,dydx)
        self = shear(self,xdim,ydim,dydx,szflag)
        
        %Data Processing
        self = interp(self,dim,x,method,extrap)
        self = resample(self,dim,k,method,extrap)
        [self, locobj] = subsamplepeak(self,dim)
        self = integrate(self, dim)
        self = differentiate(self,dim,spacing)
        self = partial(self,dim,order)
        self = modulate(self,dim,f,phi)
        self = filter(self,dim,cutoff,order,type)
        self = conv(self,g,dim,shape)
        [self, selfminus] = directionalfilter(self,dim1,dim2)
        varargout = gradient(self,dim,k)
        self = fft(self,dim,N)
        self = ifft(self,dim,N)
        self = normalize(self,range)
        self = hamming(self,dim)
        self = tukeywin(self,dim,R)
        self = logcompress(self)
        [shiftobj,ccobj] = normxcorr(self1,dim,self2,ksz_samp,srch_samp,decimationfactor);
        [polyobj, r2obj] = polyfit(self,dim,order)
        self = polyval(polyobj,x)
        stats = anova(self,dim);
        self = stdfilt(self,dim,k)
        self = shift(self,dim,dx)
        self = apply(self,func,varargin)
        self = apply_along(self,dim,func,varargin)
            
        %Basic Functions
        self = sum(self,dim)
        [self, locobj] = min(self,dim)
        [self, locobj] = max(self,dim)
        self = mean(self,dim)
        self = median(self,dim)
        self = std(self,dim,n)
        iqrobj = iqr(self,dim);
        self = plus(self,B)
        self = minus(self,B)
        self = times(self,B)
        self = rdivide(self,B)
        self = ldivide(self,B)
        self = sqrt(self)
        self = power(self,B)
        self = abs(self)
        self = angle(self)
        self = real(self)
        self = imag(self)
        self = conj(self)
        self = samplewise(self,fun,varargin)
        iseq = eq(self1,self2)
        noteq = ne(self1,self2)
        is_empty = isempty(self)
        
        function sz_bytes = bytes(self)
            mirror = self; %#ok<NASGU>
            s = whos('mirror');
            sz_bytes = s.bytes;
        end
        
        %datacontainer constructor
        function self = datacontainer(data,dims,varargin)
        %DATACONTAINER Construct new datacontainer
        %obj = DATACONTAINER(data,dims,varargin)
        %obj = DATACONTAINER(datastruct)
        %
        %DATACONTAINER creates a datacontainer from a dataset, dimensions,
        %and parameters. When called on a single argument, the argument
        %must be a complete datacontainer object, or else a structure with 
        %the equivalent fields for dimensions and parameters.
        %
        %INPUTS
        %   data: N-D numeric data
        %   dims: structure containing dimension information. Can be
        %         created from <a href="matlab:helpPopup('datacontainer/datadims')">datacontainer/datadims</a>, or directly,
        %         as a structure with the following fields:
        %           names: 1 x N cell array of the names of the dimensions
        %           units: 1 x N cell array of the units for the dimensions
        %      (names{i}): one field for each dimension, containing the
        %         values of that dimension as a column
        %           additional fields can be provided, such as 'apex' or
        %           'geometry', that may be used for certain specialized
        %           functions.
        %
        %       Additional parameters will be included as property-value
        %       pairs in iqobj.params.
        %
        %See Also: datacontainer/datadims

            self.data = [];
            if nargin == 1 && isa(data,'datacontainer')
                self = datacontainer(data.data,data.dims,data.params);
                self.units = data.units;
                self.params = data.params;
                self.log = data.log;
            elseif nargin == 1 && isa(data,'struct')
                self = datacontainer(data.data,data.dims);
                pnames = properties(self);
                for i = 1:length(pnames)
                if isfield(data,pnames{i})
                    self.(pnames{i}) = data.(pnames{i});
                end
                end
            else
                if exist('data','var')
                    self.data = data;
                end
                if exist('dims','var') && ~isempty(dims)
                    if isa(dims,'struct')
                        self.dims = dims;
                    elseif isa(dims,'cell')
                        self.dims = datacontainer.datadims(dims{:});
                    else
                        error('cannot build dims')
                    end
                else
                    ndim = ndims(self.data);
                    [values, dimunits, dimnames] = deal(cell(1,ndim));
                    for i = 1:ndim
                        values{i} = 1:size(self.data,i);
                        dimunits{i} = '';
                        dimnames{i} = char('i'+(i-1));
                    end
                    self.dims = datacontainer.datadims(values,dimnames,dimunits);
                end
                if nargin > 2
                    if mod(length(varargin),2)
                        self.units = varargin{1};
                        self.params = addfields(self.params,varargin(2:end));
                    else
                        self.params = addfields(self.params,varargin);
                    end
                end
                ndim = ndims(self.data);
                % check if array is actually a row or column vector
                if ndim == 2 && any(size(self.data) == 1), ndim = 1; end
                for i = 1:ndim
                    [dim, dimname] = self.getdim(i);
                    ldim = length(self.dims.(dimname));
                    dsz = self.datasize(dim);
                    if  dsz ~= ldim 
                        error('Length of %s (%g) does not match %s data dimension size (%g)',dimname, ldim, nth(dim), dsz);
                    end
                end
            end
            
        end
    end
    methods (Hidden)
        
        %Utilities - Redundantly-named functions
        disp(self);
        self = uminus(self)
        self = uplus(self)
        %varargout = subsref(self,varargin)
        %varargout = subsasgn(self,s,varargin)
        out=end(self,k,n)
        self = mtimes(self,B)
        self = mrdivide(self,B)
        self = mldivide(self,B)
        self = mpower(self,B)
        self = db(self)
        
    end
end
