        function self = filter(self,dim,cutoff,order,type)
        %FILTER Butterworth filters data in the specified dimension
        %self = filter(self,dim,cutoff,order,type)
        %
        %INPUTS:
        % self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %     dim: active dimension. May be the index of the dimension,
        %      or the string matching the dimension name in
        %      self.dims.names.
        %  cutoff: cutoff frequency (1 or 2-element vector).
        %   order: filter order (defaults to 3) negative values time
        %   reverse the filter
        %    type: defaults to 'low' or 'bandpass' based on length of
        %    cutoff
        %OUTPUTS:
        % self: output datacontainer
        %
        %FILTER uses <a href="matlab:helpPopup(fullfile(matlabroot,'toolbox','signal','signal','butter'))">butter</a> to build the filter based on the sampling
        %frequency determined from self.dims. For ultrasound data, note
        %that you must specify this in m^-1, not hz. 
        %
        % See Also: butter
            
            if ~exist('order','var')
                order = 3;
            end
            if iscell(order)
                for i = 1:length(order)
                    if exist('type','var');
                        self = filter(self,dim,cutoff,order{i},type);
                    else
                        self = filter(self,dim,cutoff,order{i});
                    end
                end
                return
            end
            [dim, dimname, dimunit] = self.getdim(dim);
            t = self.dims.(self.dims.names{dim});
            fs = 1/median(diff(t));
            if any(abs(diff(diff(t)))/mean(diff(t))>1e-2)
                warning('%s is not uniformly sampled. Assumed fs of %g may produce unexpected results',self.dims.names{dim},fs)
            end
            Wn = cutoff/(fs/2);
            if exist('type','var')
                [B,A] = butter(abs(order),Wn,type);
                switch type
                    case 'high'
                        fstr = sprintf('%g %s high-pass',Wn,sprintf('1/%s',dimunit));
                    case 'low'
                        fstr = sprintf('%g %s low-pass',Wn,sprintf('1/%s',dimunit));
                    case 'stop'
                        fstr = sprintf('%g-%g %s stopband',Wn(1),Wn(2),sprintf('1/%s',dimunit));
                    case 'bandpass'
                        fstr = sprintf('%g-%g %s bandpass',Wn(1),Wn(2),sprintf('1/%s',dimunit));
                end
            else
                if Wn(1) <= 0
                    Wn = Wn(2);
                end
                if length(Wn) == 1 && Wn>1
                    return
                end
                if length(Wn) == 2 && Wn(2)>1
                    [B,A] = butter(abs(order),Wn(1),'high');
                    fstr = sprintf('%g %s high-pass',Wn(1),sprintf('1/%s',dimunit));
                else
                    [B,A] = butter(abs(order),Wn);
                    if length(Wn) > 1
                        fstr = sprintf('%g-%g %s bandpass',Wn(1),Wn(2),sprintf('1/%s',dimunit));
                    else
                        fstr = sprintf('%g %s low-pass',Wn,sprintf('1/%s',dimunit));
                    end
                end
            end
            msk = isnan(self.data);
            self.data(msk) = 0;
            if order<0
                self.data = flipdim(filter(B,A,flipdim(self.data,dim),[],dim),dim);
            else
                self.data = filter(B,A,self.data,[],dim);
            end
            self.data(msk) = nan;
            self.log = char(self.log,sprintf('filtered in %s dimension with %s filter',dimname,fstr));
        end