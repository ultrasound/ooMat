 function self = sqrt(self)
            self.data = sqrt(self.data);
            if length(self.units)>2 && strcmp(self.units(end-1:end),'^2')
                self.units = self.units(1:end-2);
            else
                self.units = sprintf('\\sqrt{%s}',self.units);
            end
            self.log = char(self.log,'square root of values');
        end
        