function self = imag(self)
%REAL Return imaginary part of complex signal
%self = imag(self)
%
%
%IMAG returns a datacontainer object containing the imaginary part of the data
%
%INPUTS:
%    self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
%OUTPUTS:
%   self: imaginary-valued datacontainer
%
self.data = imag(self.data);
self.log = char(self.log,'Took imaginary part of values');

