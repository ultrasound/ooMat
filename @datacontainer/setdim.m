function self = setdim(self,dim,dimval,dimname,dimunit)
            %SETDIM Manully assign new values to a dimension
            %self = setdim(self,dim,dimval,(dimname),(dimunit))
            %self =
            %setdim(self,{dim1,dim2,...},{dimval1,dimval2,...},{dimname1,dimname2,...},{dimunit1,dimunit2,...})
            %
            %SETDIM returns a datacontainer object with vew values assigned
            %to the specified dimension. SETDIM can be used to append
            %dimensions.
            %
            %INPUTS:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %       dim: active dimension. May be the index of the dimension,
            %           or the string matching the dimension name in
            %           self.dims.names. May be a cell array.
            %    dimval: new values to be assigned to dimension. Can be
            %       assigned as an empty array [], in which case only the
            %       dimension name (and unit) will be modified. Must be a
            %       cell array if dim is provided as a cell array.
            %   dimname: optional new name for the dimension. If omitted,
            %       the name will be unchanged, or assigned as "iN" for the
            %       Nth dimension if appending dimensions.
            %   dimunit: optional unit assignment for dim
            %
            %
            %OUTPUTS:
            %   self: modified datacontainer
            %
if iscell(dim)
    for i = 1:length(dim)
        argin{1} = dim{i};
        if iscell(dimval) && (numel(dimval) == numel(dim))
            argin{2} = dimval{i};
        else
            argin{2} = dimval;
        end
        if exist('dimname','var') 
            if iscell(dimname)
                argin{3} = dimname{i};
            else
                error('not enough dimension names')
            end
            if exist('dimunit','var')
                if iscell(dimunit)
                    argin{4} = dimunit{i};
                else
                    argin{4} = dimunit;
                end
            end
        end
        self = setdim(self,argin{:});
    end
    return
end
if ischar(dim)
    [dim] = self.getdim(dim);
end
if dim>ndims(self)
    for i = ndims(self)+1:dim-1
        self = setdim(self,i,0);
    end
end
if dim == 0
    dim = ndims(self)+1;
end
if exist('dimname','var') && any(strcmp(dimname,{'names','units'}))
    error('%s is not a valid dimension name (reserved)',dimname)
end
if exist('dimname','var') && any(strcmp(dimname,self.dims.names([1:dim-1 dim+1:end])))
    error('Dimension %s already exists!',dimname)
end
if dim > ndims(self)
    if dim > ndims(self)+1
        warning('dim:toofar','data object only has %g dimensions. Filling in singleton dimension(s) %g:%g with default names',ndims(self),ndims(self)+1,dim-1)
        for dimi = ndims(self)+1:dim-1
            self.dims.names{dimi} = sprintf('i%g',dimi);
            self.dims.units{dimi} = '';
            self.dims.(self.dims.names{dimi}) = 1:size(self.data,dimi);
        end
    end
        if ~exist('dimunit','var')
            dimunit = '';
        end
        if ~exist('dimname','var')
            dimname = sprintf('i%g',dim);
        end
        if length(dimval) ~= size(self.data,dim)
                error('New %s dimension has length of %g, but the size of the data in the %s dimension is %g',dimname,length(dimval),rankstr(dim),size(self.data,dim))
                
        end
        self.dims.names{dim} = dimname;
        self.dims.units{dim} = dimunit;
        self.dims.(self.dims.names{dim}) = dimval;
        self.log = char(self.log,sprintf('Added %s dimension %s (%s)',rankstr(dim),dimname,dimunit));

    
else
    [dim, dimname0, dimunit0] = self.getdim(dim);
    if ~exist('dimunit','var')
        dimunit = dimunit0;
    end
    if ~exist('dimname','var')
        dimname = dimname0;
    end
    
    self.dims.units{dim} = dimunit;
    if isempty(dimval)
        dimval = self.dims.(dimname0);
    else
        if ~ischar(dimval) && length(dimval) ~= size(self.data,dim)
        error('New %s dimension has length of %g, but the size of the data in the %s dimension is %g',dimname,length(dimval),dimname0,size(self.data,dim))
        end
    end
    if ~strcmpi(dimname,dimname0)
        self.dims = rmfield(self.dims,dimname0);
        self.dims.names{dim} = dimname;
    end
    self.dims.(dimname) = dimval;
    self.log = char(self.log,sprintf('Changed dimension %s (%s) -> %s (%s)',dimname0,dimunit0,dimname,dimunit));
end
end

function str = rankstr(num);
switch num
    case 1
        str = '1st';
    case 2
        str = '2nd';
    case 3
        str = '3rd';
    otherwise
        str = sprintf('%gth',num);
end
end

