function self = partial(self,dim,order)
        %PARTIAL Compute partial derivative in specified dimension
        %self = differentiate(self,dim,order)
        %
        %INPUTS:
        % self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %     dim: active dimension. May be the index of the dimension,
        %       or the string matching the dimension name in
        %       self.dims.names
        % spacing: optional order to use (may be non-integer) 
        %
        %OUTPUTS:
        % self: output datacontainer
        %
        %PARTIAL take the partial derivative of self.data using a
        %filtered Fourier Transform method in the specified dimension
        %
        %See Also: datacontainer/differentiate datacontainer/fft
            if ~exist('order','var')
                order = 1;
            end
            [dim,dimName] = self.getdim(dim);
            dimval = self.dims.(dimName);
            log = self.log;
            ftobj = fft(self,dim);
            omega = ftobj.dims.(ftobj.dims.names{dim});
            omega = reshape(omega,[ones(1,dim-1) length(omega)]);
            self = real(ifft(ftobj*(1i*2*pi*omega).^order,dim));
            if order == 1
                self.units = sprintf('%s/%s',self.units,self.dims.units{dim});
            else
                self.units = sprintf('%s/(%s^{%g})',self.units,self.dims.units{dim},order);
            end
            self.dims.(dimName) = dimval;
            self.log = char(log,sprintf('%s-order partial derivative in the %s dimension',nth(order),dimName));
        end