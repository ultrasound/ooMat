function self = interp(self,dim,x,method,extrap)
            %INTERP Interpolate along specified dimension 
            %self = INTERP(self,dim,x,[method])
            %
            %INTERP resamples self.data in the specified dimension at
            %either a fixed upsampling rate x, or at arbitrary positions
            %x. If a method is specificed, INTERP will use interp1 with
            %the specified method. Two additional methods can be used,
            %'sinc' and 'polyphase'. These methods will use interpft and
            %resample, respectively. Any self.params which share the
            %same size as self.data IN THE SAME DIMENSION will
            %also be resampled.
            %
            %INPUTS:
            %   self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
            %  dim: active dimension. May be the index of the dimension,
            %      or the string matching the dimension name in
            %      self.dims.names
            %  x cell{scalar}: resampling factor for the specified dimesnion.
            %  x (vector): target dimension vector
            %  x 'uniform': uniform sampling from
            %       x(1):x(2)-x(1):x(end)
            %  [method] interp1 interpolation method. 'linear' if blank.
            %
            % OUTPUTS:
            % self: updated datacontainer object
            %
            % See Also: interp1, signal\resample, interpft
            [dim, dimname] = self.getdim(dim);
            if ~exist('extrap','var')
                extrap = true;
            end
            x0 = self.dims.(dimname);
            if ischar(x) && strcmpi(x,'uniform')
                x = x0(1):median(diff(x0)):x0(end);
                if ~exist('method','var')
                    method = 'linearf';
                end
            end
            if iscell(x)
                if numel(x)>1
                    error('must specify single resampling rate');
                end
                x1 = interp1((0:(length(x0)-1))',x0,(0:(1/x{1}):length(x0)-(1/x{1}))','linear','extrap');
            else
                x1 = x;
            end
            if ~exist('method','var')
                method = 'linear';
            end
            uniformSpacing0 = max(abs(diff(x0)))<1.01*min(abs(diff(x0)));
            uniformSpacing1 = max(abs(diff(x1)))<1.01*min(abs(diff(x1)));
            sz0 = size(self.data);
            switch lower(method)
                case {'sinc','interpft'}
                    if ~uniformSpacing0
                        error('Input dimension must be uniformly sampled for sinc interpolation')
                    end
                    if ~uniformSpacing1
                        error('Output dimension must be uniformly sampled for sinc interpolation')
                    end
                    
                    if length(x1)<length(x0)
                        error('Sinc interpolation cannot be used to downsample')
                    end
                    fs0 = 1/mean(diff(x0));
                    fs1 = 1/mean(diff(x1));
                    self.data = interpft(self.data,length(x1),dim);
                    self.log = char(self.log,sprintf('sinc interpolated %s from %g to %g %s^-1',dimname,fs0,fs1,self.dims.units{dim}));
                case {'resample','polyphase'}
                    if ~iscell(x)
                        error('A fixed resample rate must be used for polyphase resampling')
                    end
                    [N,D] = rat(x{1});
                    if N+D > self.maxResamplePQsum
                        error('Requested resample rate (%g -> %g) uses a rational resampling rate of P/Q = %g/%g, which exceeds the limit of P+Q < %g specified for this %s object. You may set the maxResamplePQsum property to a higher value, or use a different target frequency.',fs0,fs,N,D,self.maxResamplePQsum,class(self));
                    end
                    data = permute(self.data,[dim 1:dim-1 dim+1:length(sz0)]);
                    data = resample(double(data(:,:)),N,D);
                    data = reshape(data,[length(x1) sz0([1:dim-1 dim+1:end])]);
                    self.data = reshape(permute(data,[2:dim 1 dim+1:length(sz0)]),[sz0(1:dim-1) length(x1) sz0(dim+1:end)]);
                    self.log = char(self.log,sprintf('resampled %s at a rate of %g/%g',dimname,N,D));
                otherwise
                    data = permute(self.data,[dim 1:dim-1 dim+1:length(sz0)]);
                    switch method
                        case 'linearf'
                            data0 = data(:,:);
                            data0(isnan(data0)) = 0;
                            idx = interp1(x0,[1:length(x0)],x1,'linear','extrap');
                            if any(idx<1 | idx>length(x0))
                                error('no extrapolation support')
                            end
                            idx0 = floor(idx);
                            frac = mod(idx,1);
                            idx1 = min(idx0+1,length(x0));
                            F = repmat(frac(:),[1 size(data0,2)]);
                            data = (1-F).*data0(idx0,:) + F.*data0(idx1,:);
                        otherwise
                            if extrap
                                data = interp1(x0(:),data(:,:),x1(:),method,'extrap');
                            else
                                data = interp1(x0(:),data(:,:),x1(:),method);
                            end
                    end
                    
                    data = reshape(data,[length(x1) sz0([1:dim-1 dim+1:end])]);
                    self.data = reshape(permute(data,[2:dim 1 dim+1:length(sz0)]),[sz0(1:dim-1) length(x1) sz0(dim+1:end)]);
                    self.log = char(self.log,sprintf('%s-interpolated %s from %g to %g samples',method,dimname,length(x0),length(x1)));
            end
            self.dims.(dimname) = x1;
            paramNames = fieldnames(self.params);
            for i = 1:length(paramNames)
                if size(self.params.(paramNames{i}),dim) == sz0(dim);
                    szd0 = size(self.params.(paramNames{i}));
                    dim0 = permute(self.params.(paramNames{i}),[dim 1:dim-1 dim+1:length(sz0)]);
                    dim0 = interp1(x0,dim0(:,:),x1,'linear','extrap');
                    dim0 = reshape(dim0,[length(x1) szd0([1:dim-1 dim+1:end])]);
                    self.params.(paramNames{i}) = reshape(permute(dim0,[2:dim 1 dim+1:length(sz0)]),[szd0(1:dim-1) length(x1) szd0(dim+1:end)]);
                end
            end
        end