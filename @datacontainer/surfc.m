function h = surfc(self,varargin)
            sz = size(self.data);
            nonsingleton = find(sz>1);
            if length(nonsingleton)~=2
                error('datacontainer must not have exactly two non-singleton dimensions')
            end
            xdim = nonsingleton(2);
            xName = self.dims.names{xdim};
            xUnit = self.dims.units{xdim};
            zdim = nonsingleton(1);
            zName = self.dims.names{zdim};
            zUnit = self.dims.units{zdim};
                v = varargin;
                if mod(length(v),2) == 1 && isnumeric(v{end}) && length(v{end})==2
                    v = v(1:end-1);
                    clim = varargin{end};
                else
                    clim = autoclim(self.data(:),[5 95]);
                end
                [X Y] = meshgrid(self.dims.(xName),self.dims.(zName));
                h0 = surfc(X,Y,squeeze(self.data),squeeze(self.data),varargin{:});
            set(gca,'clim',clim);
            xlabel(genlabel(self,xdim))
            ylabel(genlabel(self,zdim));
            if ~isempty(self.units)
                cb = colorbar;
                ylabel(cb,self.units);
            end
            if strcmp(zUnit,xUnit)
                PBAR = get(gca,'PlotBoxAspectRatio');
                set(gca,'PlotBoxAspectRatio',[(PBAR(1)+PBAR(2))/2+[0 0] PBAR(3)]);
                %axis image
            end
            set(gca,'zlim',clim);
            if nargout == 1
                h = h0;
            end
        end