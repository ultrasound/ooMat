function [polyobj r2obj] = polyfit(self,dim,order)
        %POLYFIT Fit the data to a polynomial in the specified dimension
        %polyobj = POLYFIT(self,dim,order)
        %
        %POLYFIT fits the data in the datacontainer to a polynomial of the
        % specified order along the requested dimension. The result is
        % returned as a <a
        % href="matlab:helpPopup('datacontainer')">datacontainer</a>
        %object which replaces the specified dimension with the polynomial
        %coefficients for the OLS polynomial fit in that dimension. 
        %
        %Inputs:
        %  self: <a href="matlab:helpPopup('datacontainer')">datacontainer</a> object
        %      dim: dimension along which to concatenate. May be the index
        %       of the dimension, or the string matching the dimension name 
        %       in self.dims.names
        %  order: order of the polynomial fit.
        %           0: constant
        %           1: linear [default]
        %           2,3,etc: quadratic, cubic, etc.
        %Outputs:
        %  polyobj: polynomial coefficient object
        %    r2obj: goodness of fit object
        %
        %
        % See Also: datacontainer/polyval
        
    [dim dimName] = getdim(self,dim);
    if ~exist('order','var')
        order = 1;
    end
    dimorder = self.dims.names;
    dimorder{dim} = 'poly_power';
    polyobj = permute(self,dimName);
    polyobj.params.poly_dim_name = dimName;
    sz = datasize(polyobj);
    sz(1) = order+1;
    coeffs = zeros(sz);
    coeffs(:,:) = polyfit_multi(self.dims.(dimName),polyobj.data(:,:),order);
    polyobj.data = coeffs;
    polyobj = setdim(polyobj,dimName,0:order,'poly_power');
    polyobj = permute(polyobj,dimorder);
    polyobj.log = char(self.log,sprintf('Calculated %s order polynomial fit from %s dimension',nth(order),dimName));
    if nargout>1
        ybar = mean(self,dim);
        yhat = polyval(polyobj,self.dims.(dimName));
        SSR = sum((yhat-ybar)^2,dim);
        TSS = sum((self-ybar)^2,dim);
        r2obj = (SSR/TSS);
        r2obj.log = char(self.log,sprintf('Goodness of fit for %s order polynomial fit from %s dimension',nth(order),dimName));
    end

    