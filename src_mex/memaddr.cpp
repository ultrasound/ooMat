#include "mex.h"

/* Input arguments */
#define A        prhs[0]

/* Output arguments */
#define PTRs        plhs[0]

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   double* ptr = (double*)mxGetData(A);
   //mexPrintf("Pointer Address = %x\n\n", &ptr[0]);
   char buffer [16];
   int numchar = sprintf(buffer,"%x",&ptr[0]);
   //mexPrintf("%d characters\n",numchar);
   PTRs = mxCreateString(buffer);
   //int i;
   //for (i=[0];i<numchar;i++)
	//PTRs[i] = buffer[i];
}