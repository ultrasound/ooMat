#include "mex.h"
#include "matrix.h"
#include <math.h>

/* Input arguments */
#define X0          prhs[0]
#define Y0          prhs[1]
#define DIM    prhs[2]

/* Output arguments */
#define YMAX			plhs[0]
#define XMAX        plhs[1]


template<typename xType,typename yType>
void subsamplepeak(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Check number of input arguments */
    if (nrhs < 2)
        mexErrMsgTxt("Not enough input arguments.");
	if (nrhs > 3)
		mexErrMsgTxt("Too many input arguments.");
	if (nlhs > 2)
		mexErrMsgTxt("Too many output arguments.");
    /* Get active dimension */
	int dim;
	if (nrhs<3)
        dim = 0;
    else
        dim = (int)mxGetScalar(DIM)-1;
		
	/* Declare indexing variables */
	mwSize idx,vecidx,xidx;
	
	/* Get input matrix sizes */
    const mwSize ny = mxGetNumberOfElements(Y0);
    const mwSize nx = mxGetNumberOfElements(X0);
	const mwSize nDimX = mxGetNumberOfDimensions(X0);
	const mwSize nDimY = mxGetNumberOfDimensions(Y0);
	const mwSize *Ysz = mxGetDimensions(Y0);
	const mwSize *Xsz = mxGetDimensions(X0);
	
	/* Check input matrix sizes */
	if (Ysz[dim]!=nx){
		mexPrintf("size(Y,%d) = %d, but length(X) = %d\n",dim+1,Ysz[dim],nx);
		mexErrMsgTxt("Size mismatch");
		}
	
	/* Set up output size */
	mwSize *outsz = (mwSize*)malloc(sizeof(mwSize)*nDimY);
	for(idx=(0);idx<(nDimY);idx++)
		outsz[idx] = Ysz[idx];
    outsz[dim] = 1;
	
	/* Declare looping increments, sizes */
	mwSize vecinc,vecmod,xinc,nvec;
	nvec = (mwSize)ny/nx;
	xinc = 1;
	for(idx=(1);idx<(dim+1);idx++)
		xinc*=Ysz[idx-1];
	vecinc = 1;
	for(idx=(0);idx<dim+1;idx++)
		vecinc*=Ysz[idx];
	vecmod = 1;
	for(idx=(1);idx<dim+1;idx++)
		vecmod*=Ysz[idx-1];
	
	//mexPrintf("xinc = %d\n",xinc);
	//mexPrintf("vecinc = %d\n",vecinc);
	//mexPrintf("vecmod = %d\n",vecmod);
	
		/* Declare I/O pointers */
		xType *xmax, *x;
		yType *ymax, *y;
		/* Declare variables */
		double ssa,ssb,ssc,ssd;
		double ssx[3];
		double ssy[3];
		xType xpeak;
		yType ypeak;
		mwSize xpeaki,ypeaki;
		if (nlhs > 1){
			XMAX = mxCreateNumericArray(nDimY,outsz,mxGetClassID(X0),mxREAL);
			xmax  = (xType*)mxGetData(XMAX);
		}
		YMAX = mxCreateNumericArray(nDimY,outsz,mxGetClassID(Y0),mxREAL);
		ymax  = (yType*)mxGetData(YMAX);
		
		x       = (xType*)mxGetData(X0);
		y       = (yType*)mxGetData(Y0);
		
	
	/* Loop through target matrix */
	for(vecidx=(0);vecidx<(nvec);vecidx++)
	{
		//mexPrintf("vecidx = %d: [",vecidx);
		for(xidx=(0);xidx<(nx);xidx++)
		{
			idx = (vecidx/vecmod)*(vecinc)+(vecidx%vecmod)+xidx*xinc;
			//mexPrintf("%d ",idx);
			if(xidx==0){
				/* Initialize running averages */
				ypeak = y[idx];
				xpeak = x[xidx];
				xpeaki = 0;
				ypeaki = (vecidx/vecmod)*(vecinc)+(vecidx%vecmod);
			}
			else {
				if (y[idx]>ypeak) {
					ypeak = y[idx];
					ypeaki = idx;
					xpeaki = xidx;
					xpeak = x[xpeaki];
				}				
			}
		}
		//mexPrintf("] xpeaki = %d, ypeaki = %d",xpeaki,ypeaki);
		
		if ((xpeaki==0) || (xpeaki==nx-1)){	
		//mexPrintf("Exact\n");
			ymax[vecidx] = (xType)ypeak;
			if (nlhs > 1)
				xmax[vecidx] = (xType)xpeak;
		}
		else{
		    //mexPrintf("Interp\n");
			/*quadratic subsample peak estimation*/
			ssx[0] =  (double)x[xpeaki-1];
			ssx[1] =  (double)x[xpeaki];
			ssx[2] =  (double)x[xpeaki+1];
			ssy[0] =  (double)y[ypeaki-xinc];
			ssy[1] =  (double)y[ypeaki];
			ssy[2] =  (double)y[ypeaki+xinc];
			ssd = (ssx[0] - ssx[1]) * (ssx[0] - ssx[2]) * (ssx[1] - ssx[2]);
			ssa = (ssx[2] * (ssy[1] - ssy[0]) + ssx[1] * (ssy[0] - ssy[2]) + ssx[0] * (ssy[2] - ssy[1])) / ssd;
			ssb = (ssx[2]*ssx[2] * (ssy[0] - ssy[1]) + ssx[1]*ssx[1] * (ssy[2] - ssy[0]) + ssx[0]*ssx[0] * (ssy[1] - ssy[2])) / ssd;
			ssc = (ssx[1] * ssx[2] * (ssx[1] - ssx[2]) * ssy[0] + ssx[2] * ssx[0] * (ssx[2] - ssx[0]) * ssy[1] + ssx[0] * ssx[1] * (ssx[0] - ssx[1]) * ssy[2]) / ssd;
			ymax[vecidx] = (yType)(ssc-((ssb*ssb)/(4*ssa)));
			if (nlhs>1)
				xmax[vecidx] = (xType)(ssb/(-2*ssa));
		}
	}
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    /* Check Input arguments */
	if (nrhs < 2) {
        mexErrMsgTxt("Not enough input arguments.");
    }
	if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) 
		subsamplepeak<double,double>(nlhs,plhs,nrhs,prhs);
    else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) 
		subsamplepeak<float,float >(nlhs,plhs,nrhs,prhs);
	else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) 
		subsamplepeak<float,double >(nlhs,plhs,nrhs,prhs);
	else if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) 
		subsamplepeak<double,float >(nlhs,plhs,nrhs,prhs);
	else
		mexErrMsgTxt("X and Y must be single or double.");	
	return;
}
