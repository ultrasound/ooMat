classdef imageslider_ui < handle
    properties
        x = '';
        y = '';
        z = '';
    end
    properties (Hidden=true)
        xdim = [];
        ydim = [];
        zdim = [];
        image = []
        axis = [];
        xctrl = [];
        yctrl = [];
        title = [];
        zctrl = [];
        playf = [];
        playb = [];
        zcontext = [];
        ycontext = [];
        xcontext = [];
        indices = {};
        shape = []        
        obj = [];
        names = {};
        databuffer = [];
        specopts = [];
    end
    methods
        function self = imageslider_ui(obj,varargin)
            self.specopts = struct('clim','auto','initval',-inf,'fps',30,'XDim','','YDim','','x','','y','','z','');
            if(mod(nargin,2)==0)
                args = {varargin{1:end-1}, 'clim', varargin{end}};
            else
                args = varargin;
            end
            [self.specopts, options] = setopts(self.specopts,args);
            if isempty(self.specopts.XDim) && ~isempty(self.specopts.x)
                self.specopts.XDim = self.specopts.x;
            end
            if isempty(self.specopts.YDim) && ~isempty(self.specopts.y)
                self.specopts.YDim = self.specopts.y;
            end
            self.shape = obj.datasize;
            nonsingleton = find(self.shape>1);
            self.names = obj.dims.names;
            ndims = obj.ndims;
            singleton = find(self.shape==1);
            valid = ones(1, ndims);
            valid(singleton) = 0;
            if ~isempty(self.specopts.x)
                [self.xdim, self.x] = obj.getdim(self.specopts.XDim);
                valid(self.xdim) = 0;
            end
            if ~isempty(self.specopts.y)
                [self.ydim, self.y] = obj.getdim(self.specopts.YDim);
                valid(self.ydim) = 0;
            end
            if ~isempty(self.specopts.z)
                [self.zdim, self.z] = obj.getdim(self.specopts.z);
                valid(self.zdim) = 0;
            end

            validnames = obj.dims.names(find(valid));
            if isempty(self.specopts.x)
                [self.xdim, self.x] = obj.getdim(validnames{1});
                valid(self.xdim) = 0;
                validnames = validnames(2:end);
            end
            if isempty(self.specopts.y)
                [self.ydim, self.y] = obj.getdim(validnames{1});
                valid(self.ydim) = 0;
                validnames = validnames(2:end);
            end
            if isempty(self.specopts.z)
                [self.zdim, self.z] = obj.getdim(validnames{1});
                valid(self.zdim) = 0;
                validnames = validnames(2:end);
            end
            self.indices = cell(1, ndims);
            for i = 1:obj.ndims
                self.indices{i} = 1;
            end
            self.obj = obj;
        end
        
        function cdata = get_cdata(self)
            cdata = self.databuffer(:,:,self.indices{self.zdim});
        end
        
        function update_databuffer(self)
            idx = self.indices;
            idx{self.xdim} = 1:self.shape(self.xdim);
            idx{self.ydim} = 1:self.shape(self.ydim);
            idx{self.zdim} = 1:self.shape(self.zdim);
            dims = [self.names; idx];
            self.databuffer = squeeze(self.obj.slice(dims{:}).permute({self.y,self.x,self.z}).data);
        end
        
        function imshow(self)
            xval = self.obj.dims.(self.x);
            yval = self.obj.dims.(self.y);
            self.update_databuffer()
            cdata = self.get_cdata();
            self.image = imagesc(xval,yval,cdata);
            self.xctrl = xlabel(self.obj.genlabel(self.x));
            self.yctrl = ylabel(self.obj.genlabel(self.y));
            self.title = title(self.get_title);
            self.axis = gca;
            if ~strcmp(class(self.specopts.clim),'char')
                caxis(self.specopts.clim)
            end
            pos0 = plotboxpos(self.axis);
            figH = ancestor(self.axis,'figure');
            fsz = figH.Position;
            buttonsz = [max(pos0(3)*0.05,20/fsz(3)) max(20/fsz(4),0.05*pos0(4))];
            n = self.shape(self.zdim);
            self.zctrl = uicontrol('Style', 'slider',...
                'Min',1,'Max',n,'Value',self.indices{self.zdim},...
                'Units','normalized',...
                'Position', ([pos0(1)+buttonsz(1)*2 pos0(2) pos0(3)-buttonsz(1)*2 buttonsz(2)]),...
                'Callback',@cDataUpdate,...
                'SliderStep',[1/(n-1), max(1/(n-1),0.1)],...
                'Tag','ImageSlider',...
                'Tooltipstring',self.z);
            self.zctrl.UserData.obj = self;
            
            self.playf = uicontrol('Style','pushbutton',...
                'Units','normalized',...
                'Position',[pos0(1)+buttonsz(1) pos0(2) buttonsz],...
                'String','>',...
                'Callback',@animate,...
                'Tag','PlayButton');
            self.playf.UserData.obj = self;
            self.playf.UserData.fps = self.specopts.fps;
            self.playf.UserData.is_playing = 0;
            self.playf.UserData.inc = 1;
            self.playf.UserData.playstr = '>';
            self.playf.UserData.offset = 1;

            self.playb = uicontrol('Style','pushbutton',...
                'Units','normalized',...
                'Position',[pos0(1) pos0(2) buttonsz],...
                'String','<',...
                'Callback',@animate,...
                'Tag','PlayButton');
            self.playb.UserData.obj = self;
            self.playb.UserData.fps = self.specopts.fps;
            self.playb.UserData.is_playing = 0;
            self.playb.UserData.inc = -1;
            self.playb.UserData.playstr = '<';
            self.playb.UserData.offset = 0;

            self.playb.UserData.otherplay = self.playf;
            self.playf.UserData.otherplay = self.playb;
            self.update_context_menus()
            set(ancestor(self.axis,'figure'),'ResizeFcn',@updateSliderPositions);
            set(self.image,'DeleteFcn',{@deleteControls,{self.zctrl,self.playf,self.playb}})
        end    
        
        function update_context_menus(self)
            dimnames = {'x','y','z'};
            callback = struct('x',@switchXdim,'y',@switchYdim,'z',@switchZdim);
            for dimname = dimnames
                dim = dimname{1};
                context = sprintf('%scontext',dim);
                ctrl = sprintf('%sctrl',dim);
                dimnum = sprintf('%sdim',dim);
                self.(context) = uicontextmenu('UserData',struct('obj',self));
                self.(context).UserData.obj = self;
                self.(ctrl).UIContextMenu = self.(context);
                for d = self.get_unused_axes(dim)
                    menuitem = uimenu(self.(context),'Label',self.names{d},'Tag',self.names{d},'Callback',callback.(dim));
                    if d == self.(dimnum)
                        menuitem.Label = ['*' menuitem.Label];
                    end
                end
            end
        end
        
        function dims = get_unused_axes(self, candidate)
            valid = self.shape > 1;
            if ~exist('candidate','var')
                candidate = '';
            end
            valid(self.xdim) = strcmp(candidate,'x');
            valid(self.ydim) = strcmp(candidate,'y');
            valid(self.zdim) = strcmp(candidate,'z');
            dims = find(valid);
        end
            
        
        function set_index(self, dim, index)
            [dim] = self.obj.getdim(dim);
            if index<1
                index = self.shape(dim)+index;
            end
            self.indices{dim} = index;
            if ~any(dim==[self.xdim, self.ydim, self.zdim])
                self.update_databuffer();
            end
        end
        
        function update_cdata(self, index)
            if exist('index','var')
                self.set_index(self.z, index);
            end
            self.image.CData = self.get_cdata;
            self.title.String = self.get_title();
        end
        
        function ttl = get_title(self)         
            ttl = '';
            for dim = self.get_unused_axes('z')
                ttl = [ttl self.obj.genlabel(dim, self.indices{dim}) ', '];
            end
            ttl = ttl(1:end-2);
        end
        
        function set_zdim(self, dim)
            [self.zdim, self.z] = self.obj.getdim(dim);
            n = self.shape(self.zdim);
            set(self.zctrl,'Max',self.shape(self.zdim),'Value',self.indices{self.zdim},...
                'Tooltipstring',self.z,'SliderStep',[1/(n-1), max(1/(n-1),0.1)])
            self.update_databuffer();
            self.update_context_menus()
            self.update_cdata();
        end
        
        function set_xdim(self, dim)
            [self.xdim, self.x] = self.obj.getdim(dim);
            xval = self.obj.dims.(self.x);
            n = length(xval);
            if iscell(xval)
                set(self.image,'XData',1:n)
                set(self.axis,'XLim',[0.5, n+0.5],'XTick',[1:n],'XTickLabel',xval)
            else
                dx = mean(diff(xval));
                set(self.image,'XData',xval)
                set(self.axis,'XLim',[min(xval)-0.5*dx, max(xval)+0.5*dx],'XTickMode','auto','XTickLabelMode','auto');
            end
            if length(self.axis.XTick) > length(xval)
                    self.axis.XTick = xval;
            end
            self.xctrl.String = self.obj.genlabel(self.x);
            self.update_databuffer();
            self.update_context_menus()
            self.update_cdata();
        end
        
        function set_ydim(self, dim)
            [self.ydim, self.y] = self.obj.getdim(dim);
            yval = self.obj.dims.(self.y);
            n = length(yval);
            if iscell(yval)
                set(self.image,'YData',1:n)
                set(self.axis,'YLim',[0.5, n+0.5],'YTick',[1:n],'YTickLabel',yval)
            else
                dy = mean(diff(yval));
                set(self.image,'YData',yval)
                set(self.axis,'YLim',[min(yval)-0.5*dy, max(yval)+0.5*dy],'YTickMode','auto','YTickLabelMode','auto');
                if length(self.axis.YTick) > length(yval)
                    self.axis.YTick = yval;
                end
            end
            self.yctrl.String = self.obj.genlabel(self.y);
            self.update_databuffer();
            self.update_context_menus()
            self.update_cdata();
        end
        
    end
end

function cDataUpdate(hObject, eventdata)
    idx = round(hObject.Value);
    hObject.Value = idx;
    hObject.UserData.obj.update_cdata(idx);
end

function switchZdim(hObject, eventdata)
    hObject.Parent.UserData.obj.set_zdim(hObject.Tag)
end

function switchXdim(hObject, eventdata)
    hObject.Parent.UserData.obj.set_xdim(hObject.Tag)
end

function switchYdim(hObject, eventdata)
    hObject.Parent.UserData.obj.set_ydim(hObject.Tag)
end


function deleteControls(hObject,eventdata,handles);
    for i = 1:length(handles)
        if ishandle(handles{i})
            delete(handles{i})
        end
    end
end

function updateSliderPositions(hObject,eventdata);
    drawnow;
    sliders = findobj(eventdata.Source,'Tag','ImageSlider');
    for i = 1:length(sliders)
        im = sliders(i).UserData.obj.image;
        ax = sliders(i).UserData.obj.axis;
        fsz = hObject.Position;
        pos = plotboxpos(ax);
        buttonsz = [max(pos(3)*0.05,20/fsz(3)) max(20/fsz(4),0.05*pos(4))];
        set(sliders(i),'Position',[pos(1)+buttonsz(1)*2 pos(2) max(pos(3)-buttonsz(1)*2,1e-3) max(buttonsz(2),1e-3)]);
    end
    buttons = findobj(eventdata.Source,'Tag','PlayButton');
    for i = 1:length(buttons)
        ax = buttons(i).UserData.obj.axis;
        fsz = hObject.Position;
        pos = plotboxpos(ax);
        buttonsz = [max(pos(3)*0.05,20/fsz(3)) max(20/fsz(4),0.05*pos(4))];
        set(buttons(i),'Position',[pos(1)+buttonsz(1)*buttons(i).UserData.offset pos(2) max(buttonsz,1e-3)]);
    end
end


function animate(hObject,eventdata)
    hObject.UserData.is_playing = ~hObject.UserData.is_playing;
    if ~hObject.UserData.is_playing
        hObject.String = hObject.UserData.playstr;
        hObject.UserData.otherplay.Enable = 'on';
    else
        hObject.String = '||';
        hObject.UserData.otherplay.Enable = 'off';
        obj = hObject.UserData.obj;
        n = size(obj.databuffer,3);
        sld = obj.zctrl;
        z0 = sld.Value;
        i = z0;
        while i>=1 && i<=n && hObject.UserData.is_playing
            t = tic;
            i = sld.Value+hObject.UserData.inc;
            sld.Value = max(1,min(n,i));
            cDataUpdate(sld,eventdata);
            pause(0.001);
            while((toc(t))<(1/hObject.UserData.fps))
                pause(0.001);
            end
        end
        if i>n && hObject.UserData.inc == 1
            sld.Value = z0;
            cDataUpdate(sld,eventdata);
        end
        if i<1 && hObject.UserData.inc == -1
            sld.Value = z0;
            cDataUpdate(sld,eventdata);
        end
        hObject.UserData.is_playing = 0;
        hObject.String = hObject.UserData.playstr;
        hObject.UserData.otherplay.Enable = 'on';
    end
end
