%SUBSAMPLEPEAK Quadratic subsample peak estimation
%[yMax,xMax] = subsamplepeak(x,y,dim)
%SUBSAMPLE peak uses quadratic interpolation about the peak value of y
%using neighboring values to return the estimated peak value, as well as
%the interpolated location of the peak.
%
%INPUTS:
%     x: vector specifying the dimension of y
%     y: data matrix
%   dim: integer specifying in which dimension of y to operate
%
%   length(x) must be equal to size(y,dim)
%
%OUTPUTS:
% yMax: Interpolated peak values. Peaks at the edges of y are reported
%       directly.
% xMax: Interpolated peak locations.

function varargout = subsamplepeak(varargin)
[pth,me,ext] = fileparts(mfilename);
warning('path:nomex','mex file %s.%s not found. Using %s_matlab.m. Consider compiling %s/src_mex/%s.cpp',me,mexext,me,pth,me);
[varargout{:}] = subsamplepeak_matlab(varargin{:});
