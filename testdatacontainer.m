% unit test script
% https://www.mathworks.com/help/matlab/matlab_prog/write-script-based-unit-tests.html
%{
result = runtests('testdatacontainer');
rt = table(result)
%}

% preconditions
t = [1, 2, 3];
x = [1, 2];
d = [5, 20, 10; 6, 21, 11];
o = datacontainer(d,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));

%% Test 1: rdivide
obj_inv = 1/o;
inverse  = [1, 1, 1; 1, 1, 1]./d;
o_inv = datacontainer(inverse,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_inv.data,o_inv.data))

%% Test 2: ldivide
obj_div = 5.\o;
divide = 5.\d;
o_div = datacontainer(divide,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_div.data,o_div.data))

%% Test 3: minus
obj_difference = 1-o;
difference = 1-d;
o_difference = datacontainer(difference,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_difference.data,o_difference.data))

%% Test 4: plus
obj_add = 1+o;
my_add = 1+d;
o_add = datacontainer(my_add,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_add.data,o_add.data))

%% Test 5: power: %g raised to the data-th power
obj_pow = 2.^o;
my_pow = 2.^d;
o_pow = datacontainer(my_pow,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_pow.data,o_pow.data))

%% Test 6: power: data raised to the %s power
obj_pow = o.^2;
my_pow = d.^2;
o_pow = datacontainer(my_pow,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_pow.data,o_pow.data))

%% Test 7: power: data raised to the data power
obj_pow = o.^o;
my_pow = d.^d;
o_pow = datacontainer(my_pow,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_pow.data,o_pow.data))

%% Test 8: times
obj_times = 2.*o;
my_times = 2.*d;
o_times = datacontainer(my_times,datacontainer.datadims({x,t},{'x','t'},{'m','s'}));
assert(isequal(obj_times.data,o_times.data))

%% Test: integrate then differentiate
o_int = o.integrate('t');
o2 = o_int.differentiate('t');
assert(isequal(o2.data,o.data))

%% Test: Differentiate then integrate
o_diff = o.differentiate('t');
o2 = o_diff.integrate('t');
o2_plus_c = o2 + d(:,1)*ones(1,size(d,2));
assert(isequal(o2_plus_c.data,o.data))

%% Test: Char array and string units for getunitconversion 1
assert(isequal(getunitconversion('micron','meter'),getunitconversion("micron","meter")))

%% Test: Char array and string units for getunitconversion 2
assert(getunitconversion('deg', 'rad') - getunitconversion("deg", 'rad') < 2*eps )

%% Test: assign values in one position of one dimension
o1 = o.assign(1,1,50);
d1 = d;
d1(1,:) = 50;
assert(isequal(o1.data,d1))

%% Test: assign values in multiple positions of one dimension
o1 = o.assign(2,1:2,50);
d1 = d;
d1(:,1:2) = 50;
assert(isequal(o1.data,d1))

%% Test: assign values in multiple positions in multiple dimensions
o1 = o.assign(1,1:2,2,2:3,[1,2;3,4]);
d1 = d;
d1(:,2:3) = [1,2;3,4];
assert(isequal(o1.data,d1))

%% Test: assign cell inputs with indices
o1 = o.assign({1,2},{1,3},60);
d1 = d;
d1(1,3) = 60;
assert(isequal(o1.data,d1))

%% Test: assign cell inputs with dim names
o1 = o.assign({'t','x'},{3,1},80);
d1 = d;
d1(1,3) = 80;
assert(isequal(o1.data,d1))

%% Test: assign one datacontainer to another
t2 = [2, 3];
d2 = [1, 2; 3, 4];
o2 = datacontainer(d2,datacontainer.datadims({x,t2},{'x','t'},{'m','s'}));
o1 = o.assign(1,1:2,2,2:3,o2);
d1 = d;
d1(:,2:3) = d2;
assert(isequal(o1.data,d1))
